# frozen_string_literal: true

class CasePdf < Prawn::Document
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::NumberHelper

  def initialize(the_case)
    img = Rails.root.join('app', 'assets', 'images', 'pdf_background.jpg')
    super(top_margin: 70, right_margin: 0, left_margin: 0)
    @case = the_case

    font_families.update('OpenSans' =>
      {
        normal: Rails.root.join('app', 'assets', 'fonts', 'OpenSans-Regular.ttf'),
        italic: Rails.root.join('app', 'assets', 'fonts', 'OpenSans-Italic.ttf'),
        bold: Rails.root.join('app', 'assets', 'fonts', 'OpenSans-Bold.ttf'),
        bold_italic: Rails.root.join('app', 'assets', 'fonts', 'OpenSans-BoldItalic.ttf')
      })
    font 'OpenSans'
    font_size 10

    image(img, width: bounds.width, at: [0, 750])

    move_down(100)
    address_block
    move_down(20)
    date_and_place
    move_down(50)
    subject
    move_down(60)
    letter_top
    move_down(40)
    deployments
    move_down(40)
    letter_bottom
  end

  def address_block
    text_box @case.user_full_name,
             at: [75, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true
    move_down(16)
    text_box @case.user_address_street,
             at: [75, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true
    move_down(16)
    text_box "#{@case.user_address_zip} #{@case.user_address_city}",
             at: [75, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true
    move_down(16)
  end

  def date_and_place
    text_box "Tuttlingen, den #{I18n.l(@case.finished_on)}",
             at: [380, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true
  end

  def subject
    text_box "Abrechnung #{@case.identifier}",
             at: [75, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true,
             style: :bold
  end

  def letter_top
    text_box "Sehr geehrte/r #{@case.user_full_name},",
             at: [75, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true
    move_down(16)
    text_box 'für die folgenden Einsätze für die Stiftung Dianiño '\
             "unter der Fallnummer #{@case.identifier} überweisen wir "\
             "#{number_to_currency(sum_for_all(@case))} auf das Konto #{@case.user_bank_account_iban}.",
             at: [75, cursor], width: 425, height: 10,
             overflow: :expand, inline_format: true
  end

  def deployments
    text_box 'Einsatz',
             at: [75, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true
    text_box 'Einsatzpauschale inkl. Fahrtkosten',
             at: [275, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true
    table_horizontal_rule
    @case.case_deployments.each_with_index do |cd, i|
      text_box I18n.l(cd.started_on, format: :date),
               at: [75, cursor], width: 200, height: 10,
               overflow: :expand, inline_format: true
      text_box number_to_currency(sum_for_deployment_case(cd)),
               at: [275, cursor], width: 200, height: 10,
               overflow: :expand, inline_format: true
      move_down(16)

      start_new_page if i == 18
    end
    table_horizontal_rule
    text_box 'Summe',
             at: [75, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true,
             style: :bold
    text_box number_to_currency(sum_for_all(@case)),
             at: [275, cursor], width: 200, height: 10,
             overflow: :expand, inline_format: true,
             style: :bold
  end

  def letter_bottom
    text_box 'Wir weisen darauf hin, dass du für die Versteuerung '\
             'der gezahlten Beträge selbst verantwortlich bist.',
             at: [75, cursor], width: 425, height: 10,
             overflow: :expand, inline_format: true
    move_down(40)
    text_box 'Vielen Dank für Deinen Einsatz',
             at: [75, cursor], width: 425, height: 10,
             overflow: :expand, inline_format: true
    move_down(60)
    text_box 'Ingrid Binder, Vorsitzende der Stiftung Dianiño',
             at: [75, cursor], width: 425, height: 10,
             overflow: :expand, inline_format: true
  end

  private

    # TODO: Merge with case_helper
    def sum_for_deployments(the_case)
      allowance = if the_case.per_diem_allowance&.positive?
                    the_case.per_diem_allowance
                  else
                    Rails.application.secrets.per_diem_allowance
                  end
      the_case.case_deployments.count * allowance
    end

    # TODO: Merge with case_helper
    def sum_for_distance(the_case)
      allowance = if the_case.commuting_allowance&.positive?
                    the_case.commuting_allowance
                  else
                    Rails.application.secrets.commuting_allowance
                  end
      the_case.case_deployments.sum(:distance) * allowance
    end

    # TODO: Merge with case_helper
    def sum_for_all(the_case)
      sum_for_deployments(the_case) + sum_for_distance(the_case)
    end

    # TODO: Merge with case_helper
    def sum_for_deployment_case(case_deployment)
      the_case = case_deployment.case
      per_diem_allowance = if the_case.per_diem_allowance&.positive?
                             the_case.per_diem_allowance
                           else
                             Rails.application.secrets.per_diem_allowance
                           end
      commuting_allowance = if the_case.commuting_allowance&.positive?
                              the_case.commuting_allowance
                            else
                              Rails.application.secrets.commuting_allowance
                            end

      per_diem = per_diem_allowance * 1.00
      commuting = commuting_allowance * case_deployment.distance

      per_diem + commuting
    end

    def table_horizontal_rule
      stroke do
        move_down(12)
        line_width 1
        horizontal_line(75, 500)
        move_down(6)
      end
    end
end
