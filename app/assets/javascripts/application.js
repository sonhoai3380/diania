// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require popper
//= require rails-ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require wice_grid
//= require bootstrap-datepicker/core
//= require bootstrap-datepicker/locales/bootstrap-datepicker.de.js
//= require cocoon

//= require snippets/caseForm
//= require snippets/editCaseForm
//= require snippets/nannySearch
//= require cable

//= require vendors/sticky
//= require vendors/scripts.bundle
//= require vendors/iframeResizer.contentWindow

//= require_self

// <!-- begin::Global Config(global config for global JS sciprts) -->
var KTAppOptions = {
    "colors": {
        "state": {
            "brand": "#005fc4",
            "metal": "#c4c5d6",
            "light": "#ffffff",
            "accent": "#00c5dc",
            "primary": "#4A85C5",
            "success": "#34bfa3",
            "info": "#36a3f7",
            "warning": "#ffb822",
            "danger": "#fd3995",
            "focus": "#9816f4"
        },
        "base": {
            "label": [
                "#c5cbe3",
                "#a1a8c3",
                "#3d4465",
                "#3e4466"
            ],
            "shape": [
                "#f0f3ff",
                "#d9dffa",
                "#afb4d4",
                "#4A85C5"
            ]
        }
    }
};
// <!-- end::Global Config -->

// START: Confirm leaving page if form has unsaved changes
var msg, unsaved;

msg = "Changes you made may not be saved.";

unsaved = false;

$(document).on('change', 'form[role="check-modified"]:not([data-remote]) :input', function() {
  return unsaved = true;
});

$(document).on('turbolinks:load', function() {
  return unsaved = false;
});

$(document).on('submit', 'form[role="check-modified"]', function() {
  unsaved = false;
});

$(window).bind('beforeunload', function() {
  if (unsaved) {
    return msg;
  }
});

$(document).on('turbolinks:before-visit', function(event) {
  if (unsaved && !confirm(msg)) {
    return event.preventDefault();
  }
});
// END: Confirm leaving page if form has unsaved changes
