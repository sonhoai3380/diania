function totalDeploymentsCount() {
  return $('.nested-fields').length;
}

function recalculateTotalDeploymentsNumber() {
  var totalDeploymentsBlock = $('.total-deployments-count');
  totalDeploymentsBlock.html(totalDeploymentsCount());

  return false;
}

function recalculateTotalDeploymentsDistance() {
  var totalPoints = 0;
  var totalDeploymentsDistanceBlock = $('.total-deployments-distance');
  $('.nested-fields .deployment-duration').each(function () {
    totalPoints = parseFloat($(this).val()) + totalPoints;
  });
  totalDeploymentsDistanceBlock.html(totalPoints);

  return false;
}

function setDeploymentHeader(insertedItem){
  var headerElement = insertedItem.find('.deployment-header');
  var newHeaderText = headerElement.html().split(' ')[0] + totalDeploymentsCount();
  headerElement.html(newHeaderText);

  return false;
}

function initCaseForm() {
  $("form :input").change(function() {
    $('.end_and_send_case').addClass('disabled');
    $('.end_and_send_case').attr("disabled", true);
    $('.form_has_changed').show();
  });
}

jQuery(function () {
  'use strict';

  $(document).ready(function() {
    jQuery('.datepicker').datepicker( { format: "dd.mm.yyyy", language: "de" } );
    if($('#case_child_attributes_nationality_de').is(':checked')) { jQuery('.case_child_nationality_other').hide(); }
    if($('#case_child_attributes_nationality_other').is(':checked')) { jQuery('.case_child_nationality_other').show(); }
    if(!$('#case_child_attributes_diseases_chronic_illness').is(':checked')) { jQuery('.case_child_diseases_chronic_illness_type').hide(); }
    initCaseForm();
  });

  $(document).on('cocoon:after-insert', function (event, insertedItem) {
    jQuery('.datepicker').datepicker( { format: "dd.mm.yyyy", language: "de" } );
    recalculateTotalDeploymentsNumber();
    setDeploymentHeader(insertedItem);
  });

  $(document).on('cocoon:after-remove', function (event, removedItem) {
    recalculateTotalDeploymentsNumber();
    recalculateTotalDeploymentsDistance();
    removedItem[0].style = 'display: none !important;';
  });

  $(document).on('keyup keypress blur change', '.deployment-duration', function(){
    recalculateTotalDeploymentsDistance();
  });

  $(document).on('click', '.save-case-button', function(){
    $('form.edit_case').submit();

    return false;
  });

  $(document).on('click', '.add_fields', function(){
    initCaseForm();

    return false;
  });

  $(document).on('change', '#case_child_attributes_nationality_other', function (event) {
    jQuery('.case_child_nationality_other').show();
  });

  $(document).on('change', '#case_child_attributes_nationality_de', function (event) {
    $("input:text[name='case[child_attributes][nationality_other]']").val(null);
    jQuery('.case_child_nationality_other').hide();
  });

  $(document).on('click', '#case_child_attributes_diseases_chronic_illness', function (event) {
    $("input:text[name='case[child_attributes][diseases][chronic_illness_type]']").val(null);
    jQuery('.case_child_diseases_chronic_illness_type').toggle();
  });
});
