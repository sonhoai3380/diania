function form_field_toggler(parent_selector, child_selector, comparison_value) {
  $(parent_selector).on('change', function () {
    if(this.value == comparison_value) {
      jQuery(child_selector).hide();
      jQuery(child_selector).children().hide();
    } else {
      jQuery(child_selector).show();
      jQuery(child_selector).children().show();
      check_language_selection();
    }
  });
}

function check_box_toggler(parent_selector, child_selector, comparison_value) {
  $(parent_selector).on('change', function () {
    if(this.checked == true) {
      jQuery(child_selector).show();
    } else {
      jQuery(child_selector).hide();
    }
  });
}

function check_language_selection() {
  if($('#case_child_language_other').is(':checked')) {
    $('.child_language_is_other').show();
    $('.child_language_is_other').children().show();
  } else if($('#case_child_language_other').length > 0) {
    $('.child_language_is_other').hide();
    $('.child_language_is_other').children().hide();
  }

  if($('#case_child_attributes_language_other').is(':checked')) {
    $('.child_language_is_other').show();
    $('.child_language_is_other').children().show();
  } else if($('#case_child_attributes_language_other').length > 0) {
    $('.child_language_is_other').hide();
    $('.child_language_is_other').children().hide();
  }
}

$(function () {
  'use strict';

  form_field_toggler('#case_requestor_function', '.requestor_is_consultant', 'doctor');
  form_field_toggler('.german-speaker', '.child_language_is_german', 'true');
  check_box_toggler('#user_other_language', '.other_language', 1);

  check_language_selection();

  if($('#case_requestor_function').val() == 'consultant') {
    $('.requestor_is_consultant').toggle();
  }

  if($('#case_german_speaking_false').is(':checked')) {
    $('.child_language_is_german').show();
  }
  if($('#case_child_attributes_german_speaking_false').is(':checked')) {
    $('.child_language_is_german').show();
  }

  if($('#user_other_language').is(':checked')) {
    $('.other_language').show();
  }

  $('.select-language').on('change', function () {
    check_language_selection();
  });
});
