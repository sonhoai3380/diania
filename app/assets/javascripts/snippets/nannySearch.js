function initMap(childLocation, nannies, case_id) {
  var childCoords = new google.maps.LatLng(childLocation[0], childLocation[1]);
  var mapOptions = {
    center: childCoords,
    zoom: 11
  };

  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

  var childMarker = new google.maps.Marker({
    position: childCoords,
    map: map
  });

  nannies.forEach(function(nanny) {
    loc = new google.maps.LatLng(nanny.address.location_coordinates[0], nanny.address.location_coordinates[1]);
    var marker = new google.maps.Marker({
      position: loc,
      map: map,
      title: nanny.address.full_name,
      icon: {
        url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
      }
    });

    marker.addListener('click', function() {
      var content = '<a href="/cases/' + case_id + '/show_nanny/' + nanny.id +
        '" class="btn btn-primary" style="color:#fff">' +
        nanny.address.full_name + '</a>';

      new google.maps.InfoWindow({
        content: content
      }).open(map, this);
    })
  })
}
