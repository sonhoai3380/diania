# frozen_string_literal: true

class ConfirmationsController < Devise::ConfirmationsController
  private

    def after_confirmation_path_for(_resource_name, resource)
      token = Devise.token_generator.generate(User, :reset_password_token)
      resource.reset_password_token = token.last
      resource.reset_password_sent_at = Time.now.utc
      resource.save
      edit_password_url(resource, reset_password_token: token.first)
    end
end
