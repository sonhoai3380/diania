# frozen_string_literal: true

class SearchNanniesController < ApplicationController
  before_action :current_case, only: :index

  def index
    authorize :search_nanny, :index?

    @nannies = User
               .nearby_nannies_by_coordinates(current_case.child_location_coordinates)
               .without_refusals(@case.id)
    @nannies_grid = initialize_grid(@nannies)
  end

  private

    def current_case
      @case = Case.find(params[:case_id])
      authorize @case
    end
end
