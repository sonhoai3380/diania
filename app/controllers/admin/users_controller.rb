# frozen_string_literal: true

module Admin
  class UsersController < ApplicationController
    after_action :verify_authorized

    before_action :set_user, except: %i[index new create destroy_all]

    def index
      @users_grid = initialize_grid(
        policy_scope(User), include: %i[address], name: 'users',
                            enable_export_to_csv: true, csv_file_name: 'users', csv_field_separator: ';',
                            order: 'users.id', order_direction: 'desc',
                            custom_order: {
                              'users.language' => 'lower(users.language)',
                              'users.email' => 'lower(users.email)',
                              'addresses.last_name' => 'lower(addresses.last_name)',
                              'addresses.first_name' => 'lower(addresses.first_name)'
                            }
      )
      export_grid_if_requested('users' => 'users_grid')
      authorize User
    end

    def new
      @form = UserCreationForm.new
      @nr = SecureRandom.alphanumeric
      @form.prebuild_care_ages
      @form.prebuild_help_service_assignments
      authorize User
    end

    def create
      @form = UserCreationForm.new(create_user_params)
      if @form.save
        redirect_to admin_users_path, notice: 'Benutzer wurde erfolgreich erstellt'
      else
        render :new, alert: 'Benutzer kann nicht erstellt werden'
      end
      authorize User
    end

    def show; end

    def edit
      @care_ages_users = CareAge.all.map do |care_age|
        CareAgesUser.find_or_initialize_by(care_age: care_age, user: @user)
      end
      @help_service_assignments = HelpService.where('key != ?', :used_on_fairs).map do |help_service|
        HelpServiceAssignment.find_or_initialize_by(help_service: help_service, assignee: @user)
      end
    end

    def update
      clear_if_valid(@user, update_user_params)
      if @user.update(update_user_params)
        redirect_to edit_admin_user_path(@user), notice: I18n.t('admin.update.update_success')
      else
        redirect_to edit_admin_user_path(@user), alert: I18n.t('admin.update.update_error')
      end
    end

    def destroy_all
      authorize User

      return if params&.dig(:users, :selected).nil?

      User.where(id: params[:users][:selected]).destroy_all
      redirect_to admin_users_path, notice: 'Die Benutzer wurden gelöscht.'
    end

    def destroy
      authorize @user
      if @user.destroy
        redirect_to(admin_users_path, notice: 'Der Benutzer wurde gelöscht.')
      else
        redirect_to admin_user_path(@user), alert: 'Der Benutzer kann nicht gelöscht werden.'
      end
    end

    def resend_confirmation_instructions
      @user.send_confirmation_instructions
      @user.update(confirmed_at: nil)
      redirect_to(admin_users_path, notice: 'Die Einladung wurde erneut versendet.')
    end

    private

      def clear_if_valid(user, params)
        user.assign_attributes(params)
        return if user.invalid?

        user.skip_reconfirmation!
        user.care_ages_users.clear
        user.help_service_assignments.clear
      end

      def set_user
        @user = User.find(params[:id])
        authorize @user
      end

      def create_user_params
        params.require(:user).permit(
          :address_salutation, :address_first_name, :address_last_name, :address_phone_business, :address_phone_private,
          :address_street, :address_zip, :address_city, :address_email_business, :address_phone_mobile,
          :user_email, :user_role, :user_password, :user_password_confirmation, :user_language, :user_language_other,
          :user_info_birthday, :user_info_occupation, :care_ages, :help_services,
          :bank_account_account_holder, :bank_account_iban, :bank_account_bic,
          user_info_reference_to_diabetes: [],
          help_service_assignments_attributes: %i[help_service_id assignee_type assignee_id],
          care_ages_users_attributes: %i[care_age_id user_id]
        )
      end

      def update_user_params
        parameters = params.require(:user).permit(
          :email, :role, :password, :password_confirmation, :language, :language_other,
          user_info_attributes: [:id, :birthday, :occupation, reference_to_diabetes: []],
          address_attributes: %i[id street zip city phone_private phone_mobile phone_business email_business
                                 salutation first_name last_name ],
          bank_account_attributes: %i[id account_holder iban bic],
          care_ages_users_attributes: %i[_delete care_age_id user_id],
          help_service_assignments_attributes: %i[help_service_id assignee_type assignee_id]
        )
        parameters[:care_ages_users_attributes]&.reject! { |_k, v| v['user_id'] == '0' }
        parameters[:help_service_assignments_attributes]&.reject! { |_k, v| v['assignee_id'] == '0' }
        parameters
      end
  end
end
