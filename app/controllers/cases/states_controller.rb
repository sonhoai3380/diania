# frozen_string_literal: true

module Cases
  class StatesController < ApplicationController
    before_action :set_case, only: %i[reject discard request_nanny accept refuse finish complete reset]
    before_action :set_user, only: %i[request_nanny]

    def reject
      notice = if @case.may_reject? && @case.reject!
                 t('.success')
               else
                 t('.error')
               end
      redirect_to cases_path, notice: notice
    end

    def discard
      notice = if @case.may_discard? && @case.discard!
                 t('.success')
               else
                 t('.error')
               end
      redirect_to cases_path, notice: notice
    end

    def request_nanny
      notice = if @case.may_request? && @case.request!(@user)
                 t('.success')
               else
                 t('.error')
               end
      redirect_to cases_path, notice: notice
    end

    def accept
      if @case.may_run? && @case.run!
        notice = t('.success')
        redirect_to edit_case_path(@case), notice: notice
      else
        flash.now[:notice] = t('.error')
        render 'cases/show'
      end
    end

    def refuse
      notice = if @case.may_refuse? && @case.refuse!
                 t('.success')
               else
                 t('.error')
               end
      redirect_to cases_path, notice: notice
    end

    def finish
      if @case.may_finish? && @case.finish!
        notice = t('.success')
        redirect_to cases_path, notice: notice
      else
        flash.now[:notice] = t('.error')
        render 'cases/edit'
      end
    end

    def complete
      if @case.may_complete? && @case.complete!
        notice = t('.success')
        redirect_to cases_path, notice: notice
      else
        flash.now[:notice] = t('.error')
        render 'cases/show'
      end
    end

    def reset
      if @case.may_reset? && @case.reset!
        notice = t('.success')
        redirect_to cases_path, notice: notice
      else
        flash.now[:notice] = t('.error')
        render 'cases/show'
      end
    end

    private

      def set_case
        @case = Case.find(params[:case_id])
        authorize @case
      end

      def set_user
        @user = User.find(params[:user_id])
        authorize @user
      end
  end
end
