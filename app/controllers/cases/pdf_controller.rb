# frozen_string_literal: true

module Cases
  class PdfController < ApplicationController
    before_action :set_case, only: %i[pdf]

    def pdf
      pdf = CasePdf.new(@case)
      send_data pdf.render,
                filename: "Fall_#{@case.identifier}",
                type: 'application/pdf',
                disposition: 'inline'
    end

    private

      def set_case
        @case = Case.find(params[:case_id])
        authorize @case
      end
  end
end
