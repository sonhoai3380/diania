# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery

  before_action :store_user_location!, if: :storable_location?
  before_action :authenticate_user!

  def after_sign_in_path_for(resource)
    stored_location_path = stored_location_for(:user)
    return stored_location_path if stored_location_path && stored_location_path != '/'

    return cases_path if resource.admin?

    return edit_user_path(resource) if resource.sign_in_count == 1

    root_path
  end

  private

    def storable_location?
      request.get? && is_navigational_format? && !devise_controller? && !request.xhr?
    end

    def store_user_location!
      store_location_for(:user, request.fullpath)
    end
end
