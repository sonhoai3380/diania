# frozen_string_literal: true

class CasesController < ApplicationController
  before_action :current_case, only: %i[show edit update]
  skip_before_action :authenticate_user!, only: %i[new create]

  def index
    @cases_grid = initialize_grid(
      policy_scope(Case),
      include: %i[child requestor],
      name: 'cases',
      order: 'cases.id',
      order_direction: 'desc',
      enable_export_to_csv: true,
      csv_file_name: 'cases',
      csv_field_separator: ';',
      csv_encoding: 'UTF-8'
    )
    export_grid_if_requested('cases' => 'cases_grid')
    authorize Case
  end

  def new
    @form = CaseCreationForm.new
    @form.prebuild_help_service_assignments
  end

  def create
    @form = CaseCreationForm.new(create_case_params)
    if @form.save
      render 'create'
    else
      render 'new'
    end
  end

  def csv_export
    redirect_to cases_path(cases: { export: 'csv' })
  end

  def show; end

  def edit; end

  def update
    if @case.update(update_case_params)
      redirect_to edit_case_path(@case), notice: t('.success')
    else
      flash[:alert] = t('.error')
      render 'edit'
    end
  end

  private

    def create_case_params
      params.require(:case).permit(
        :address_first_name, :address_last_name, :requestor_function,
        :requestor_email, :requestor_phone, :doctor_address_name,
        :requestor_consent, :clinic_zip, :clinic_name, :requestor_confirmed,
        :child_zip, :child_gender, :child_year_of_birth,
        :child_manifested_on, :terms_of_use, :german_speaking, :comments,
        :child_language_other,
        child_language: [],
        help_service_assignments_attributes: %i[help_service_id assignee_type assignee_id]
      )
    end

    def update_case_params
      params.require(:case).permit(
        :started_on, :flyer_handed, :initiated_by, :comments, :finished_on,
        child_attributes: [
          :therapy, :id, :nationality, :nationality_other, :refugee, :phone,
          :environment, :celiac_disease, :language_other, :german_speaking,
          diseases: %i[
            mental_handicap chronic_illness chronic_illness_type
            other_general_comments
          ],
          operational_env: [], operational_reason: [], language: []
        ],
        case_deployments_attributes: %i[
          _destroy started_on finished_on duration distance id
        ]
      )
    end

    def current_case
      @case = Case.find(params[:id])
      authorize @case
    end
end
