# frozen_string_literal: true

class UsersController < ApplicationController
  after_action :verify_authorized

  before_action :set_user, except: [:index]

  def index
    @users = User.all
    authorize User
  end

  def edit
    @user.bank_account ||= @user.build_bank_account
  end

  def update
    delete_password_if_blank

    if @user.update(user_params.merge(strict_validation: true))
      redirect_to edit_user_path(@user), notice: t('.update_success')
    else
      flash.now[:alert] = t('.update_error')
      render 'edit'
    end
  end

  def destroy
    @user.destroy
    redirect_to new_user_session_path, notice: t('devise.registrations.destroyed')
  end

  private

    def set_user
      @user = User.find(params[:id])
      authorize @user
    end

    def user_params
      params.require(:user).permit(
        :email, :language_other, :password, :password_confirmation,
        address_attributes: %i[salutation first_name last_name street zip city phone_private
                               phone_mobile phone_business email_business],
        bank_account_attributes: %i[account_holder iban bic],
        language: [],
        care_age_ids: [],
        help_service_ids: [],
        user_info_attributes: [:birthday, :occupation, reference_to_diabetes: []]
      )
    end

    def delete_password_if_blank
      return if params[:user][:password].present?

      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
end
