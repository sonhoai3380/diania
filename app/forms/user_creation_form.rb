# frozen_string_literal: true

class UserCreationForm
  include ActiveModel::Model
  include Initializations
  include AttributesSettings
  include Delegations
  include Validations

  def save
    return false unless valid?

    user.help_service_assignments = filtered_help_service_assignments
    user.care_ages_users = filtered_care_ages
    user.save(validate: false)
    true
  end

  private

    def filtered_care_ages
      care_ages_users.select { |has| has.user_id.to_s.to_boolean }
    end

    def filtered_help_service_assignments
      help_service_assignments.select { |has| has.assignee_id.present? && has.assignee_id != 0 }
    end
end
