# frozen_string_literal: true

class CaseCreationForm
  include ActiveModel::Model
  include Initializations
  include AttributesSettings
  include Delegations
  include Validations

  AddressFullName = Struct.new(:first_name, :last_name)

  def save
    return false unless valid?

    # TODO: Hotfix to assign doctor name @anatoliy
    # Initializations#doctor_address is being called multiple times and name is only available in
    # the last call when there's already an empty address built
    cs.requestor.doctor_address.update(doctor_address_splited_names.to_h)

    cs.help_service_assignments = filtered_help_service_assignments

    # TODO: Why is comments not inside :case hash?
    cs.comments = comments

    cs.save(validate: false)
    true
  end

  private

    def doctor_address_splited_names
      # TODO: Make sure it works for more than two names e.g. 'Dr. John Doe'
      doctor_name = doctor_address_name.to_s.split
      doctor_last_name = doctor_name.pop
      doctor_first_name = doctor_name.join(' ')

      @doctor_address_splited_names = AddressFullName.new(
        doctor_first_name,
        doctor_last_name
      )
    end

    def filtered_help_service_assignments
      help_service_assignments.select { |has| has.assignee_id.to_s.to_boolean }
    end
end
