# frozen_string_literal: true

class CaseCreationForm
  module Validations
    def self.included(base)
      base.send :include, InstanceMethods

      base.class_eval do
        validate :validate_children
        validates :address_first_name, :address_last_name, presence: true
        validates :doctor_address_name, presence: true, if: :requestor_is_consultant?
        validates :requestor_phone, presence: true
        validates :terms_of_use, :requestor_confirmed, acceptance: true
        validate :at_least_one_help_service_assignments

        # TODO: Merge with models/requestor validation
        def requestor_is_consultant?
          requestor_function == 'consultant' ||
            requestor_function == 'assistant'
        end
      end
    end

    module InstanceMethods
      def validate_children
        setup_associations
        [child, clinic, address, doctor_address, requestor].each do |entity|
          promote_errors(entity, entity.errors) if entity.invalid?
        end
      end

      def at_least_one_help_service_assignments
        return if help_service_assignments.find { |has| has.assignee_id.to_s.to_boolean }

        errors.add(:help_service_assignment, I18n.t('errors.messages.at_least_one', restriction: 'Hilfsangebot'))
      end

      def promote_errors(entity, child_errors)
        child_errors.each do |attribute, message|
          attribute = "#{attribute}_id" if %i[care_age clinic child].include?(attribute)
          errors.add("#{entity.class.name.underscore.singularize}_#{attribute}", message)
        end
      end
    end
  end
end
