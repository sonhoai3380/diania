# frozen_string_literal: true

class CaseCreationForm
  module AttributesSettings
    def self.included(base)
      base.extend ClassMethods
      base.send :include, InstanceMethods
    end

    module ClassMethods
      def address_attributes
        Address.column_names
      end

      def cs_attributes
        Case.column_names
      end

      def child_attributes
        Child.column_names
      end

      def clinic_attributes
        Clinic.column_names
      end

      def doctor_address_attributes
        address_attributes
      end

      def requestor_attributes
        Requestor.column_names
      end

      def model_name
        ActiveModel::Name.new(self, nil, Case.name)
      end
    end

    module InstanceMethods
      def assign_attributes(params)
        %w[address doctor_address child clinic requestor].each do |entity|
          attributes = params.slice(*attributes_with_preffix(entity, self.class.public_send("#{entity}_attributes")))
          instance_eval(entity).assign_attributes(attributes_without_preffix(entity, attributes))
        end
        init_custom_fields(params)
        init_help_service_assignments_attributes(params[:help_service_assignments_attributes] || {})
        setup_associations
      end

      def init_help_service_assignments_attributes(attributes)
        @help_service_assignments = [] if attributes.present?
        attributes.each do |_i, help_service_assignment_params|
          @help_service_assignments.push(HelpServiceAssignment.find_or_initialize_by(help_service_assignment_params))
        end
      end

      private

        def init_custom_fields(params)
          self.terms_of_use = params[:terms_of_use]
          self.doctor_address_name = params[:doctor_address_name]
          self.german_speaking = params[:german_speaking]
          self.comments = params[:comments]
          set_default_language
        end

        def attributes_with_preffix(preffix, attributes)
          attributes.map { |attribute| attribute.dup.prepend("#{preffix}_") }
        end

        def attributes_without_preffix(preffix, attributes)
          attributes.transform_keys { |key| key.delete_prefix("#{preffix}_") }
        end

        def set_default_language
          self.child_language = Child.language.default_value if german_speaking.to_boolean
        end
    end
  end
end
