# frozen_string_literal: true

class CaseCreationForm
  module Initializations
    def self.included(base)
      base.send :include, InstanceMethods

      base.class_eval do
        attr_accessor :terms_of_use, :doctor_address_name,
                      :german_speaking, :help_service_assignments,
                      :help_service_assignments_attributes, :comments
      end
    end

    module InstanceMethods
      def cs
        @cs ||= Case.new
      end

      def clinic
        @clinic ||= Clinic.new
      end

      def child
        @child ||= Child.new
      end

      def requestor
        @requestor ||= Requestor.new
      end

      def address
        @address ||= Address.new
      end

      def doctor_address
        @doctor_address ||= Address.with_addressable_scope(:doctor_address).new(doctor_address_params)
      end

      def prebuild_help_service_assignments
        @help_service_assignments = HelpService.where('key != ?', :used_on_fairs).map do |help_service|
          HelpServiceAssignment.find_or_initialize_by(help_service: help_service, assignee: cs)
        end
      end

      private

        def setup_associations
          cs.clinic = clinic
          cs.child = child
          requestor.address = address
          requestor.doctor_address = doctor_address
          cs.requestor = requestor
        end

        def doctor_address_params
          doctor_address_splited_names.to_h
        end
    end
  end
end
