# frozen_string_literal: true

class CaseCreationForm
  module Delegations
    def self.included(base)
      base.class_eval do
        %w[address cs child clinic doctor_address requestor].each do |entity|
          class_eval("#{entity}_attributes", __FILE__, __LINE__).each do |attr|
            delegate attr.to_sym, "#{attr}=".to_sym, to: :"#{entity}", prefix: true
          end
        end

        delegate :id, :persisted?, to: :cs
      end
    end
  end
end
