# frozen_string_literal: true

class UserCreationForm
  module Validations
    def self.included(base)
      base.send :include, InstanceMethods

      base.class_eval do
        validate :validate_children
        validate :at_least_one_help_service_assignments
        validate :at_least_one_care_ages_users
        validates :address_first_name, :address_last_name, :address_salutation, presence: true
      end
    end

    module InstanceMethods
      def validate_children
        setup_associations
        [user, address, user_info, bank_account].each do |entity|
          promote_errors(entity, entity.errors) if entity.invalid?
        end
      end

      def at_least_one_help_service_assignments
        return unless user.strict_validation
        return if help_service_assignments.find { |has| has.assignee_id.to_s.to_boolean }

        errors.add(:help_service_assignment, I18n.t('errors.messages.at_least_one', restriction: 'Hilfsangebot'))
      end

      def at_least_one_care_ages_users
        return unless user.strict_validation
        return if care_ages_users.find { |has| has.user_id.to_s.to_boolean }

        errors.add(:care_ages_users, I18n.t('errors.messages.at_least_one', restriction: 'Betreuungsalter'))
      end

      def promote_errors(entity, child_errors)
        child_errors.each do |attribute, message|
          attribute = "#{attribute}_id" if %i[user address user_info bank_account].include?(attribute)
          errors.add("#{entity.class.name.underscore.singularize}_#{attribute}", message)
        end
      end
    end
  end
end
