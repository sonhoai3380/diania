# frozen_string_literal: true

class UserCreationForm
  module Initializations
    def self.included(base)
      base.send :include, InstanceMethods

      base.class_eval do
        attr_accessor :help_service_assignments, :help_service_assignments_attributes,
                      :care_ages_users, :care_ages_users_attributes
      end
    end

    module InstanceMethods
      def user
        @user ||= User.new
      end

      def address
        @address ||= Address.new
      end

      def bank_account
        @bank_account ||= BankAccount.new
      end

      def user_info
        @user_info ||= UserInfo.new
      end

      def prebuild_care_ages
        @care_ages_users = CareAge.all.map do |care_age|
          CareAgesUser.find_or_initialize_by(care_age: care_age, user: user)
        end
      end

      def prebuild_help_service_assignments
        @help_service_assignments = HelpService.where('key != ?', :used_on_fairs).map do |help_service|
          HelpServiceAssignment.find_or_initialize_by(help_service: help_service, assignee: user)
        end
      end

      private

        def setup_associations
          user.address = address
          user.user_info = user_info
          user.bank_account = bank_account
        end
    end
  end
end
