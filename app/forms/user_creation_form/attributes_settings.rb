# frozen_string_literal: true

class UserCreationForm
  module AttributesSettings
    def self.included(base)
      base.extend ClassMethods
      base.send :include, InstanceMethods
    end

    module ClassMethods
      def address_attributes
        Address.column_names
      end

      def user_attributes
        User.column_names | %w[password password_confirmation strict_validation]
      end

      def user_info_attributes
        UserInfo.column_names
      end

      def bank_account_attributes
        BankAccount.column_names
      end

      def model_name
        ActiveModel::Name.new(self, nil, User.name)
      end
    end

    module InstanceMethods
      def assign_attributes(params)
        %w[address user user_info bank_account].each do |entity|
          attributes = params.slice(*attributes_with_preffix(entity, self.class.public_send("#{entity}_attributes")))
          instance_eval(entity).assign_attributes(attributes_without_preffix(entity, attributes))
        end
        setup_associations
        init_help_service_assignments_attributes(params[:help_service_assignments_attributes] || {})
        init_care_ages_attributes(params[:care_ages_users_attributes] || {})
        init_custom_fields(params)
      end

      def init_help_service_assignments_attributes(attributes)
        @help_service_assignments = [] if attributes.present?
        attributes.each do |_i, help_service_assignment_params|
          @help_service_assignments.push(HelpServiceAssignment.find_or_initialize_by(help_service_assignment_params))
        end
      end

      def init_care_ages_attributes(attributes)
        @care_ages_users = [] if attributes.present?
        attributes.each do |_i, care_age_user_params|
          @care_ages_users.push(CareAgesUser.find_or_initialize_by(care_age_user_params))
        end
      end

      private

        def init_custom_fields(params)
          user_language.push(params[:user_language].to_sym) if params[:user_language].present?
          self.user_info_reference_to_diabetes = params[:user_info_reference_to_diabetes]&.keep_if(&:present?)
        end

        def attributes_with_preffix(preffix, attributes)
          attributes.map { |attribute| attribute.dup.prepend("#{preffix}_") }
        end

        def attributes_without_preffix(preffix, attributes)
          attributes.transform_keys { |key| key.delete_prefix("#{preffix}_") }
        end
    end
  end
end
