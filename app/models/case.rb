# frozen_string_literal: true

# == Schema Information
#
# Table name: cases
#
#  id                  :bigint(8)        not null, primary key
#  aasm_state          :string
#  comments            :text
#  comments_nanny      :text
#  commuting_allowance :decimal(10, 2)   default(0.0)
#  finished_on         :date
#  flyer_handed        :boolean
#  identifier          :string
#  initiated_by        :string
#  per_diem_allowance  :decimal(10, 2)   default(0.0)
#  started_on          :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  clinic_id           :bigint(8)
#  user_id             :bigint(8)
#
# Indexes
#
#  index_cases_on_clinic_id  (clinic_id)
#  index_cases_on_user_id    (user_id)
#

class Case < ApplicationRecord
  include AASM
  extend Enumerize

  # AASM START
  aasm do
    state :new, initial: true
    state :discarded, :rejected, :requested
    state :running, :refused
    state :finished
    state :completed

    event :discard, after: :notify_case_discarded do
      transitions from: :new, to: :discarded
    end

    event :reject, after: :notify_case_rejected do
      transitions from: :new, to: :rejected
    end

    event :request, after: :notify_case_accepted do
      transitions from: %i[new refused], to: :requested
    end

    event :run, after: :notify_case_running do
      transitions from: :requested, to: :running
    end

    event :refuse, after: :notify_case_refused do
      transitions from: :requested, to: :refused
    end

    event :finish, after: :notify_case_finished, if: :valid? do
      transitions from: :running, to: :finished
    end

    event :complete, after: :notify_case_completed, if: :valid? do
      transitions from: :finished, to: :completed
    end

    event :reset, after: :notify_case_reset do
      transitions from: :running, to: :new
    end
  end

  def notify_case_discarded
    CaseMailer.with(case: self, email: requestor.email).discarded.deliver
  end

  def notify_case_rejected
    CaseMailer.with(case: self, email: requestor.email).rejected.deliver
  end

  def notify_case_accepted(requested_user)
    update(user: requested_user)
    CaseMailer.with(case: self, email: user.email).requested.deliver
  end

  def notify_case_running
    CaseMailer.with(case: self).nanny_accepted.deliver
    CaseMailer.with(case: self, email: requestor.email).running.deliver
  end

  def notify_case_refused
    refused_users << user
    update(user: nil)

    CaseMailer.with(case: self).nanny_rejected.deliver
  end

  def notify_case_finished
    CaseMailer.with(case: self).finished.deliver
  end

  # rubocop:disable Metrics/AbcSize
  def notify_case_completed
    secrets = Rails.application.secrets
    update(commuting_allowance: secrets.commuting_allowance)
    update(per_diem_allowance: secrets.per_diem_allowance)

    CaseMailer.with(case: self, email: user.email).completed_nanny.deliver
    CaseMailer.with(case: self, email: requestor.email).completed_requestor.deliver
  end
  # rubocop:enable Metrics/AbcSize

  def notify_case_reset
    update(user: nil)
  end

  # AASM END

  enumerize :initiated_by, in: %i[doctor consultant assistant affected other]

  belongs_to :user, optional: true
  belongs_to :clinic, optional: true
  has_many :case_deployments, dependent: :destroy
  has_many :help_service_assignments, as: :assignee, dependent: :destroy
  has_one :child, dependent: :destroy, inverse_of: :case
  has_one :requestor, dependent: :destroy, inverse_of: :case
  has_many :refusals, dependent: :destroy
  has_many :refused_users, through: :refusals, source: :user

  validates :started_on, :initiated_by, :finished_on,
            presence: true, if: :locked?
  validates :flyer_handed, inclusion: { in: [true, false] }, if: :locked?
  validates :child, :requestor, presence: true
  validates :identifier, presence: true, uniqueness: true

  accepts_nested_attributes_for :case_deployments, allow_destroy: true
  accepts_nested_attributes_for :child, reject_if: :all_blank
  accepts_nested_attributes_for :clinic, reject_if: :all_blank
  accepts_nested_attributes_for :help_service_assignments, reject_if: :all_blank
  accepts_nested_attributes_for :requestor, reject_if: proc { |attributes| attributes['email'].blank? }

  delegate :age, to: :child, prefix: true, allow_nil: true
  delegate :age_when_disease_manifested, to: :child, allow_nil: true
  delegate :consent, to: :requestor, prefix: true, allow_nil: true
  delegate :disease_duration, to: :child
  delegate :email, to: :requestor, prefix: true, allow_nil: true
  delegate :first_name, to: :requestor, prefix: true, allow_nil: true
  delegate :doctor_address_full_name, to: :requestor, allow_nil: true
  delegate :full_name, to: :user, prefix: true, allow_nil: true
  delegate :function, to: :requestor, prefix: true, allow_nil: true
  delegate :gender, to: :child, prefix: true, allow_nil: true
  delegate :gender_text, to: :child, prefix: true, allow_nil: true
  delegate :language, to: :child, prefix: true, allow_nil: true
  delegate :languages, to: :child, prefix: true, allow_nil: true
  delegate :last_name, to: :requestor, prefix: true, allow_nil: true
  delegate :location_address, to: :child, prefix: true, allow_nil: true
  delegate :manifested_on, to: :child, prefix: true, allow_nil: true
  delegate :name, to: :clinic, prefix: true, allow_nil: true
  delegate :name, to: :user, prefix: true, allow_nil: true
  delegate :phone, to: :requestor, prefix: true, allow_nil: true
  delegate :state, to: :requestor, prefix: true, allow_nil: true
  delegate :year_of_birth, to: :child, prefix: true, allow_nil: true
  delegate :zip, to: :child, prefix: true, allow_nil: true
  delegate :zip, to: :clinic, prefix: true, allow_nil: true
  delegate :therapy, to: :child, prefix: true, allow_nil: true
  delegate :nationality, to: :child, prefix: true, allow_nil: true
  delegate :nationality_other, to: :child, prefix: true, allow_nil: true
  delegate :refugee, to: :child, prefix: true, allow_nil: true
  delegate :environment, to: :child, prefix: true, allow_nil: true
  delegate :celiac_disease, to: :child, prefix: true, allow_nil: true
  delegate :diseases, to: :child, prefix: true, allow_nil: true
  delegate :operational_env, to: :child, prefix: true, allow_nil: true
  delegate :operational_reason, to: :child, prefix: true, allow_nil: true
  delegate :id, to: :user, prefix: true, allow_nil: true
  delegate :care_age, to: :child, prefix: true, allow_nil: true
  delegate :salutation, to: :user, prefix: true, allow_nil: true
  delegate :first_name, to: :user, prefix: true, allow_nil: true
  delegate :last_name, to: :user, prefix: true, allow_nil: true
  delegate :location_address, to: :user, prefix: true, allow_nil: true
  delegate :language_other, to: :child, prefix: true, allow_nil: true
  delegate :bank_account_iban, to: :user, prefix: true, allow_nil: true
  delegate :address_street, to: :user, prefix: true, allow_nil: true
  delegate :address_zip, to: :user, prefix: true, allow_nil: true
  delegate :address_city, to: :user, prefix: true, allow_nil: true

  before_validation :finish_case,
                    if: ->(c) { c.locked? && c.finished_on.nil? }
  before_validation :generate_identifier, on: :create
  before_validation :set_language
  after_create :notify_case_created

  def notify_case_created
    CaseMailer.with(case: self).created.deliver
    # CaseMailer.with(case: self, email: requestor.email).requestor_created.deliver
  end

  def finish_case
    self.finished_on = case_deployments.max_by(&:finished_on)&.finished_on
  end

  def locked?
    %w[finished completed].include?(aasm_state)
  end

  def generate_identifier
    date = Time.zone.today
    last_case = Case.where(created_at: date.all_year).order('created_at ASC').last
    last_running_number = last_case ?  last_case.identifier.split('-').last.to_i : 0
    running_number = last_running_number + 1
    number_string = format('%04i', running_number)

    self.identifier = "#{date.year}-#{number_string}"
  end

  def set_language
    child.language = [:de] if child&.german_speaking == 'true'
  end

  def child_location_coordinates
    return [] unless child
    return child.location_coordinates if child.location_coordinates.any?

    address = child.location.address || "#{child.zip}, Deutschland"
    Geocoder.search(address).first&.coordinates
  end
end
