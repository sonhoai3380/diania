# frozen_string_literal: true

# == Schema Information
#
# Table name: locations
#
#  id                :bigint(8)        not null, primary key
#  latitude          :float
#  locationable_type :string
#  longitude         :float
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  locationable_id   :bigint(8)
#
# Indexes
#
#  index_locations_on_locationable_type_and_locationable_id  (locationable_type,locationable_id)
#

class Location < ApplicationRecord
  belongs_to :locationable, polymorphic: true, inverse_of: :location

  validates :locationable, presence: true

  geocoded_by :address
  reverse_geocoded_by :latitude, :longitude do |obj, results|
    if (_geo = results.first) && obj.locationable
      # TODO: Any better solutions?
      # obj.locationable.update_column(:state, geo.state)

    end
  end
  after_validation :geocode, :reverse_geocode

  delegate :zip, to: :locationable, allow_nil: true
  delegate :state, to: :locationable, allow_nil: true

  def address
    [zip, 'Deutschland'].compact.join(', ')
  end
end
