# frozen_string_literal: true

# == Schema Information
#
# Table name: user_infos
#
#  id                    :bigint(8)        not null, primary key
#  birthday              :date
#  occupation            :string
#  reference_to_diabetes :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  user_id               :bigint(8)
#
# Indexes
#
#  index_user_infos_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class UserInfo < ApplicationRecord
  extend Enumerize

  belongs_to :user

  validates :user, presence: true
  validates :occupation, :reference_to_diabetes,
            presence: true,
            if: ->(ua) { ua.user&.strict_validation }

  serialize :reference_to_diabetes, Array
  enumerize :reference_to_diabetes,
            in: %i[works_in_clinic also_diabetes child_with_diabetes self_help_group other_reference],
            multiple: true
end
