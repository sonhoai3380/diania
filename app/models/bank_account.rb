# frozen_string_literal: true

# == Schema Information
#
# Table name: bank_accounts
#
#  id             :bigint(8)        not null, primary key
#  account_holder :string
#  bank_name      :string
#  bic            :string
#  iban           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint(8)
#
# Indexes
#
#  index_bank_accounts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class BankAccount < ApplicationRecord
  belongs_to :user

  validates :user, presence: true
  validates :account_holder, :bic, :iban,
            presence: true,
            if: ->(ba) { ba.user&.strict_validation }

  # def any_value_present?
  #   account_holder? || bic? || iban?
  # end
end
