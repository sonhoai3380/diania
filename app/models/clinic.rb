# frozen_string_literal: true

# == Schema Information
#
# Table name: clinics
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  zip        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Clinic < ApplicationRecord
  has_many :cases, dependent: :restrict_with_error

  validates :zip, :name, presence: true
end
