# frozen_string_literal: true

# == Schema Information
#
# Table name: care_ages
#
#  id          :bigint(8)        not null, primary key
#  description :string
#  key         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class CareAge < ApplicationRecord
  has_many :users, through: :care_ages_users
  has_many :children, dependent: :restrict_with_error

  validates :key, :description, presence: true
end
