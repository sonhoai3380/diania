# frozen_string_literal: true

# == Schema Information
#
# Table name: case_deployments
#
#  id          :bigint(8)        not null, primary key
#  comment     :text
#  distance    :integer
#  duration    :integer
#  finished_on :datetime
#  started_on  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  case_id     :bigint(8)
#
# Indexes
#
#  index_case_deployments_on_case_id  (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (case_id => cases.id)
#

class CaseDeployment < ApplicationRecord
  belongs_to :case

  validates :distance, :duration, :started_on, :case, presence: true
  validates :duration, :distance, numericality: { only_integer: true, greater_than: 0 }
  validates_datetime :started_on
end
