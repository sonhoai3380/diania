# frozen_string_literal: true

# == Schema Information
#
# Table name: care_ages_users
#
#  care_age_id :bigint(8)        not null
#  user_id     :bigint(8)        not null
#
# Indexes
#
#  index_care_ages_users_on_user_id_and_care_age_id  (user_id,care_age_id)
#

class CareAgesUser < ApplicationRecord
  belongs_to :care_age
  belongs_to :user

  delegate :description, to: :care_age, prefix: true, allow_nil: true
end
