# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  confirmation_sent_at   :datetime
#  confirmation_token     :string
#  confirmed_at           :datetime
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  language               :string
#  language_other         :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  name                   :string
#  nda                    :boolean
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :string
#  sign_in_count          :integer          default(0), not null
#  unconfirmed_email      :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ApplicationRecord
  extend Enumerize
  include Languagable

  attr_accessor :strict_validation, :other_language

  has_many :care_ages_users, dependent: :delete_all
  has_many :care_ages, through: :care_ages_users
  has_one :address, as: :addressable, dependent: :destroy
  has_one :bank_account, dependent: :destroy
  has_one :user_info, dependent: :destroy
  has_many :cases, dependent: :restrict_with_error
  has_many :help_service_assignments, as: :assignee, dependent: :destroy
  has_many :help_services, through: :help_service_assignments
  has_many :refusals, dependent: :destroy
  has_many :refused_cases, through: :refusals, source: :case

  before_validation :set_name_from_address

  accepts_nested_attributes_for :address, :bank_account, :user_info, :help_service_assignments, :care_ages_users

  validates :address, :bank_account, presence: true
  validates :care_ages, :help_services,
            presence: true,
            if: ->(u) { u.strict_validation }

  enumerize :role, in: %i[nanny admin], default: :nanny,
                   predicates: true, scope: true

  devise :database_authenticatable, :confirmable,
         :recoverable, :rememberable, :validatable,
         :trackable

  reverse_geocoded_by 'locations.latitude', 'locations.longitude'

  scope :nearby_nannies_by_coordinates, lambda { |coordinates = []|
    joins(address: :location).with_role(:nanny).near(coordinates, 1000, units: :km)
  }
  scope :without_refusals, lambda { |case_id|
    where.not(id: Refusal.where(case_id: case_id).map(&:user_id))
  }

  delegate :first_name, to: :address, prefix: true, allow_nil: true
  delegate :full_name, to: :address, allow_nil: true
  delegate :last_name, to: :address, prefix: true, allow_nil: true
  delegate :street, to: :address, prefix: true, allow_nil: true
  delegate :city, to: :address, prefix: true, allow_nil: true
  delegate :zip, to: :address, prefix: true, allow_nil: true
  delegate :full_address, to: :address, allow_nil: true
  delegate :location_address, to: :address, allow_nil: true
  delegate :salutation, to: :address, allow_nil: true
  delegate :bic, to: :bank_account, prefix: true, allow_nil: true
  delegate :iban, to: :bank_account, prefix: true, allow_nil: true
  delegate :account_holder, to: :bank_account, prefix: true, allow_nil: true
  delegate :reference_to_diabetes, to: :user_info, allow_nil: true
  delegate :phone_mobile, to: :address, allow_nil: true
  delegate :phone_private, to: :address, allow_nil: true
  delegate :phone_business, to: :address, allow_nil: true
  delegate :email_business, to: :address, allow_nil: true
  delegate :birthday, to: :user_info, allow_nil: true
  delegate :occupation, to: :user_info, allow_nil: true

  protected

    def set_name_from_address
      self.name = address&.full_name
    end
end
