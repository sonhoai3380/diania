# frozen_string_literal: true

# == Schema Information
#
# Table name: help_service_assignments
#
#  id              :bigint(8)        not null, primary key
#  assignee_type   :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  assignee_id     :bigint(8)
#  help_service_id :bigint(8)
#
# Indexes
#
#  index_help_service_assignments_on_assignee_type_and_assignee_id  (assignee_type,assignee_id)
#  index_help_service_assignments_on_help_service_id                (help_service_id)
#
# Foreign Keys
#
#  fk_rails_...  (help_service_id => help_services.id)
#

class HelpServiceAssignment < ApplicationRecord
  belongs_to :help_service
  belongs_to :assignee, polymorphic: true

  delegate :description, to: :help_service, prefix: true, allow_nil: true

  validates :help_service, :assignee, presence: true
  delegate :description, to: :help_service, allow_nil: true, prefix: true
end
