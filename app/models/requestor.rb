# frozen_string_literal: true

# == Schema Information
#
# Table name: requestors
#
#  id         :bigint(8)        not null, primary key
#  confirmed  :boolean
#  consent    :boolean
#  email      :string
#  function   :string
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  case_id    :bigint(8)
#
# Indexes
#
#  index_requestors_on_case_id  (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (case_id => cases.id)
#

class Requestor < ApplicationRecord
  extend Enumerize

  belongs_to :case, inverse_of: :requestor
  has_one :address, -> { where(addressable_scope: :address) },
          as: :addressable, class_name: Address.name,
          inverse_of: :addressable, dependent: :destroy
  has_one :doctor_address, -> { where(addressable_scope: :doctor_address) },
          as: :addressable, inverse_of: :addressable,
          class_name: Address.name, dependent: :destroy

  validates :function, :email, :case, :address, presence: true
  validates :email, format: { with: Devise.email_regexp }
  validates :consent, presence: true, if: :requestor_is_consultant?

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :doctor_address

  enumerize :function, in: %i[doctor consultant assistant], predicates: true

  delegate :first_name, to: :address, allow_nil: true
  delegate :full_name, to: :address, prefix: true, allow_nil: true
  delegate :last_name, to: :address, allow_nil: true
  delegate :state, to: :address, allow_nil: true

  # TODO: Merge with case_creation_form/validations validation
  def requestor_is_consultant?
    consultant? || assistant?
  end

  def doctor_address_full_name
    doctor_address&.full_name || address_full_name
  end
end
