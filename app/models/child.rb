# frozen_string_literal: true

# == Schema Information
#
# Table name: children
#
#  id                   :bigint(8)        not null, primary key
#  celiac_disease       :boolean
#  chronic_illness_type :string
#  diseases             :json
#  environment          :string
#  gender               :string
#  language             :string
#  language_other       :string
#  manifested_on        :date
#  nationality          :string
#  nationality_other    :string
#  operational_env      :string
#  operational_reason   :string
#  phone                :string
#  refugee              :boolean
#  state                :string
#  therapy              :string
#  year_of_birth        :integer
#  zip                  :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  care_age_id          :bigint(8)
#  case_id              :bigint(8)
#
# Indexes
#
#  index_children_on_care_age_id  (care_age_id)
#  index_children_on_case_id      (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (care_age_id => care_ages.id)
#  fk_rails_...  (case_id => cases.id)
#

class Child < ApplicationRecord
  extend Enumerize

  include Languagable
  include Locatable

  attr_accessor :german_speaking

  serialize :operational_env
  serialize :operational_reason

  belongs_to :case, inverse_of: :child
  belongs_to :care_age, optional: true

  validates :gender, :manifested_on, :zip, :language, presence: true
  validates :year_of_birth, :phone, :environment, :therapy, :nationality,
            :diseases, :operational_env, :operational_reason,
            presence: true, if: -> { self.case&.locked? }
  validates :refugee, inclusion: { in: [true, false] }, if: -> { self.case&.locked? }
  validates :year_of_birth, numericality: { only_integer: true }, length: { is: 4 }
  validates :case, presence: true
  validates :language_other, presence: true, if: -> { language == 'other' }
  validates :nationality_other, presence: true, if: -> { nationality == 'other' }
  enumerize :gender, in: %i[male female], predicates: true
  enumerize :nationality, in: %i[de en ru ar other]
  enumerize :environment, in: %i[both_parents institutionalized_child single_parent]
  enumerize :operational_env, in: %i[care_center family home nursery_school other school],
                              multiple: true, skip_validations: true
  enumerize :operational_reason, in: %i[support_after_diagnosis training_of_people bridging_emergency
                                        help_with_emotional_distress support_of_independence other],
                                 multiple: true, skip_validations: true

  delegate :created_at, to: :case, prefix: true

  def age
    return unless year_of_birth

    now = Time.now.utc.to_date
    now.year - year_of_birth
  end

  def age_when_disease_manifested
    return unless year_of_birth

    manifested_on.year - year_of_birth
  end
end
