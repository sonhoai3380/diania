# frozen_string_literal: true

# == Schema Information
#
# Table name: addresses
#
#  id                :bigint(8)        not null, primary key
#  addressable_scope :string
#  addressable_type  :string
#  city              :string
#  email_business    :string
#  first_name        :string
#  last_name         :string
#  phone_business    :string
#  phone_mobile      :string
#  phone_private     :string
#  salutation        :string
#  state             :string
#  street            :string
#  zip               :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  addressable_id    :bigint(8)
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#

class Address < ApplicationRecord
  extend Enumerize
  include Locatable

  belongs_to :addressable, polymorphic: true, inverse_of: :address

  validates :email_business, format: { with: Devise.email_regexp }, allow_blank: true
  validates :salutation, :first_name, :last_name, :street, :zip, :city,
            presence: true,
            if: ->(a) { a.addressable&.strict_validation if a.addressable.respond_to?(:strict_validation) }
  validates :phone_mobile,
            presence: true,
            if: ->(a) { a.addressable&.strict_validation if a.addressable.respond_to?(:strict_validation) }

  delegate :address, to: :location, prefix: true, allow_nil: true
  enumerize :addressable_scope, in: %i[address doctor_address], default: :address, predicates: true, scope: true

  def full_name
    name_array = [first_name, last_name].reject(&:blank?)
    name_array.join(' ') if name_array.any?
  end

  def full_address
    %(
      #{street}\r
      #{zip} #{city}
    )
  end
end
