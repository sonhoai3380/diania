# frozen_string_literal: true

module Languagable
  extend ActiveSupport::Concern

  included do
    serialize :language, Array
    enumerize :language, in: %i[de en ru ar other], default: :de, multiple: true
  end

  def languages
    all_languages = language&.texts
    all_languages << language_other if language_other.present?

    all_languages
      .reject { |l| l == 'Sonstige' }
      .compact
      .join(', ')
  end
end
