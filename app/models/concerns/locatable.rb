# frozen_string_literal: true

module Locatable
  extend ActiveSupport::Concern

  included do
    has_one :location, as: :locationable, dependent: :destroy

    before_save :geo_locate

    delegate :to_coordinates, to: :location, allow_nil: true
    alias_method :location_coordinates, :to_coordinates
  end

  def geo_locate
    location ||= build_location
    location.geocode
  end
end
