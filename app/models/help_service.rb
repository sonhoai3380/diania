# frozen_string_literal: true

# == Schema Information
#
# Table name: help_services
#
#  id          :bigint(8)        not null, primary key
#  description :string
#  key         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class HelpService < ApplicationRecord
  validates :key, :description, presence: true
end
