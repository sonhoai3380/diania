# frozen_string_literal: true

# == Schema Information
#
# Table name: refusals
#
#  id         :bigint(8)        not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  case_id    :bigint(8)
#  user_id    :bigint(8)
#
# Indexes
#
#  index_refusals_on_case_id  (case_id)
#  index_refusals_on_user_id  (user_id)
#

class Refusal < ApplicationRecord
  belongs_to :case
  belongs_to :user
end
