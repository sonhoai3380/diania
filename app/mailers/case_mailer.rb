# frozen_string_literal: true

class CaseMailer < ApplicationMailer
  before_action { @case = params[:case] }
  before_action { @email = params[:email] }

  def created
    @subject = t('.subject')
    @cta = { title: 'Zum Fall', url: case_url(@case) }
    @greeting = ''
    @greeter = ''

    mail to: DEFAULT_TO, subject: @subject
  end

  def requestor_created
    @subject = t('.subject')
    @cta = { title: 'Zum Fall', url: case_url(@case) }

    mail to: @email, subject: @subject
  end

  def discarded
    @subject = t('.subject')
    @greeting = 'Vielen Dank für Ihr Verständnis'
    @greeter = 'Ihr Dianiño – Team'

    mail to: @email, subject: @subject
  end

  def rejected
    @subject = t('.subject')
    @greeting = 'Vielen Dank für Ihr Verständnis'
    @greeter = 'Ihr Dianiño – Team'

    mail to: @email, subject: @subject
  end

  def accepted
    @subject = t('.subject')
    @greeter = 'Ingrid Binder und Ihr Dianiño – Team'

    mail to: @email, subject: @subject
  end

  def requested
    @subject = t('.subject')
    @cta = { title: "Zum Fall #{@case.identifier}", url: case_url(@case) }
    @greeting = 'Vielen Dank'
    @greeter = 'Ingrid Binder und das Dianiño – Team'

    mail to: @email, subject: @subject
  end

  def nanny_accepted
    @subject = t('.subject', case_id: @case.identifier)
    @cta = { title: 'Zu den Fällen', url: cases_url }

    mail to: DEFAULT_TO, subject: @subject
  end

  def nanny_rejected
    @subject = t('.subject', case_id: @case.identifier)
    @cta = { title: 'Zum Fall', url: case_url(@case) }

    mail to: DEFAULT_TO, subject: @subject
  end

  def running
    @subject = t('.subject')
    @greeter = 'Ingrid Binder und Ihr Dianiño – Team'

    mail to: @email, subject: @subject
  end

  def finished
    @email ||= DEFAULT_TO
    @subject = t('.subject', case_id: @case.identifier)
    @cta = { title: 'Fall abschließen', url: case_url(@case) }

    mail to: @email, subject: @subject
  end

  def completed_nanny
    attachments["fall_#{@case.identifier}.pdf"] = CasePdf.new(@case).render
    @subject = t('.subject', case_id: @case.identifier)
    @greeting = 'Nochmals vielen Dank für deinen Einsatz und bis bald'
    @greeter = 'Ingrid Binder und das Dianiño – Team'

    mail to: @email, subject: @subject
  end

  def completed_requestor
    @subject = t('.subject')
    @greeting = 'Herzlichst'
    @greeter = 'Ingrid Binder und Ihr Dianiño – Team'

    mail to: @email, subject: @subject
  end
end
