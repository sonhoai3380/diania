# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  include Resque::Mailer

  default from: Rails.application.secrets.email_from
  layout 'mailer'

  DEFAULT_TO = ENV['DEFAULT_TO']
end
