# frozen_string_literal: true

class UserMailer < ApplicationMailer
  before_action { @user = params[:user] }

  def welcome_nanny
    @token = @user.send(:set_reset_password_token)
    @subject = t('.subject')
    @cta = { title: 'Passwort setzen', url: edit_user_password_url(@user, reset_password_token: @token) }

    mail to: @user.email, subject: @subject
  end
end
