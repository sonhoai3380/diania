# frozen_string_literal: true

require 'csv'

class CasesCsvGeneratorService
  attr_reader :cases

  def initialize(cases)
    @cases = cases
  end

  def call
    CSV.generate(csv_options) do |csv|
      @cases.find_each do |cs|
        @case = cs
        csv << csv_row
      end
    end
  end

  private

    def csv_options
      { headers: false, col_sep: "\t" }
    end

    def csv_row
      [
        @case.aasm_state, formatted_date(@case.started_on), @case.flyer_handed.humanize,
        @case.initiated_by, clinic_data, requestor_data, @case.user_name, child_data,
        formatted_date(@case.created_at), formatted_date(@case.updated_at)
      ]
    end

    def clinic_data
      "#{@case.clinic_name}, #{@case.clinic_zip}"
    end

    def child_data
      "#{@case.child_year_of_birth}, #{@case.child_zip}"
    end

    def requestor_data
      "#{@case.requestor_email}, #{@case.requestor_phone}"
    end

    def formatted_date(date)
      date&.date_month_year
    end
end
