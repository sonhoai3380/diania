# frozen_string_literal: true

class CreateAdminService
  def call
    User.find_or_create_by!(email: Rails.application.secrets.admin_email) do |user|
      user.password = Rails.application.secrets.admin_password
      user.password_confirmation = Rails.application.secrets.admin_password
      user.name = Rails.application.secrets.admin_name
      assign_default_values(user)
    end
  end

  def assign_default_values(user)
    user.role = :admin
    user.nda = true
    user.language << :de
    user.build_address
    user.build_bank_account
    user.confirm
  end
end
