# frozen_string_literal: true

require 'csv'

class CreateHelpServicesService
  class << self
    def call
      parse_file_and_create_help_services!
    end

    private

      def parse_file_and_create_help_services!
        CSV.foreach(file_path, csv_options) do |row|
          @row = row
          create_help_service!
        end
      end

      def file_path
        Rails.root.join('db', 'files', 'help_services.csv')
      end

      def csv_options
        { headers: true, col_sep: ';', header_converters: :symbol, skip_blanks: true }
      end

      def create_help_service!
        HelpService.find_or_create_by(key: @row[:key]) do |help_service|
          help_service.description = @row[:description]
        end
      end
  end
end
