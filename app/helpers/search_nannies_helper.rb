# frozen_string_literal: true

module SearchNanniesHelper
  def for_google_maps(users)
    # Diabled rubocop because it's always safe to render unescaped users as json
    # rubocop:disable Rails/OutputSafety
    resource = ActiveModelSerializers::SerializableResource.new(users, {})
    resource.to_json.html_safe
    # rubocop:enable Rails/OutputSafety
  end
end
