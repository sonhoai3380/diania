# frozen_string_literal: true

module CasesHelper
  def care_ages_list
    CareAge.pluck(:description, :id)
  end

  def functions_list
    Requestor.function.options
  end

  def gender_list
    Child.gender.options
  end

  def languages_list
    Child.language.options.delete_if { |_k, v| v == 'de' }
  end

  def user_languages_list
    Child.language.options.delete_if { |_k, v| %w[de other].include? v }
  end

  def nationalities_other_list
    Child.language.options.select { |_k, v| %w[de other].include? v }
  end

  def initiated_by_list
    Case.initiated_by.options
  end

  def check_yes_for_radio_button?(value)
    return value if boolean?(value)

    value.nil? || value.to_boolean
  end

  def boolean?(value)
    [true, false].include? value
  end

  def case_state_filter
    if current_user.admin?
      Hash[
        Case.aasm.states.map(&:name).collect do |item|
          [I18n.t("activerecord.attributes.case.aasm_states.#{item}"), item]
        end
      ]
    else
      Hash[
        %i[requested running finished].collect do |item|
          [I18n.t("activerecord.attributes.case.aasm_states.#{item}"), item]
        end
      ]
    end
  end

  def dashboard_case_link(the_case)
    if %w[running].include?(the_case.aasm_state) && current_user.nanny?
      edit_case_path(the_case)
    else
      case_path(the_case)
    end
  end

  def sum_for_deployments(the_case)
    allowance = if the_case.per_diem_allowance&.positive?
                  the_case.per_diem_allowance
                else
                  Rails.application.secrets.per_diem_allowance
                end
    the_case.case_deployments.count * allowance
  end

  def sum_for_distance(the_case)
    allowance = if the_case.commuting_allowance&.positive?
                  the_case.commuting_allowance
                else
                  Rails.application.secrets.commuting_allowance
                end
    the_case.case_deployments.sum(:distance) * allowance
  end

  def sum_for_all(the_case)
    sum_for_deployments(the_case) + sum_for_distance(the_case)
  end

  def show_case_completed_details?(the_case)
    the_case.locked?
  end

  def therapy_list
    [
      [I18n.t('activerecord.attributes.child.therapy.true'), 'true'],
      [I18n.t('activerecord.attributes.child.therapy.false'), 'false']
    ]
  end

  def case_language(the_case)
    [the_case.child_languages, the_case.child_language_other].reject(&:blank?).join(', ')
  end

  def case_nationality(the_case)
    [the_case.child_nationality, the_case.child_nationality_other]
      .reject { |i| i.blank? || i.casecmp('other').zero? }.map(&:humanize).join(', ')
  end

  def child_diseases_to_bool(child_diseases)
    (child_diseases.to_i == 1).humanize
  end

  def child_chronic_illness(chronic_illness, chronic_illness_type)
    return child_diseases_to_bool(chronic_illness) if chronic_illness_type.blank?

    [child_diseases_to_bool(chronic_illness), chronic_illness_type.humanize].join(', ')
  end
end
