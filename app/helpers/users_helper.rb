# frozen_string_literal: true

module UsersHelper
  def reference_to_diabetes_list
    UserInfo.reference_to_diabetes.options
  end

  def self.roles
    User.role.values.map { |r| [r&.capitalize, r] }
  end

  def all_languages
    Child.language.values.map { |r| [r&.text, r] }
  end

  def reference_to_diabetes_text(reference_to_diabetes)
    return if reference_to_diabetes.blank?

    reference_to_diabetes.texts.join(', ')
  end

  def show_complete_profile_flash?(user)
    user&.nanny? &&
      !user&.valid? &&
      controller_name == 'pages'
  end
end
