# frozen_string_literal: true

class SearchNannyPolicy < BasePolicy
  def index?
    @current_user.admin?
  end
end
