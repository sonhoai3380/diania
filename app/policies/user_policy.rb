# frozen_string_literal: true

class UserPolicy < BasePolicy
  def index?
    @current_user.admin?
  end

  def show?
    @current_user.admin? || current_user?
  end

  def edit?
    @current_user.admin? || current_user?
  end

  def update?
    @current_user.admin? || current_user?
  end

  def new?
    @current_user.admin?
  end

  def create?
    @current_user.admin?
  end

  def destroy?
    @current_user.admin? || current_user?
  end

  def destroy_all?
    @current_user.admin?
  end

  def request_nanny?
    @current_user.admin?
  end

  def resend_confirmation_instructions?
    @current_user.admin?
  end

  private

    def current_user?
      @current_user == @model
    end
end
