# frozen_string_literal: true

class CasePolicy < BasePolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(user: user).where(aasm_state: %i[requested running finished completed])
      end
    end
  end

  def index?
    true
  end

  def show?
    case_user_or_admin?
  end

  def edit?
    (case_user? && !case_locked?) ||
      (@current_user.admin? && case_finished?) ||
      (@current_user.admin? && case_new?)
  end

  def update?
    case_user_or_admin? && !case_locked?
  end

  def destroy?
    @current_user.admin?
  end

  def reject?
    @current_user.admin?
  end

  def discard?
    @current_user.admin?
  end

  def request_nanny?
    @current_user.admin?
  end

  def accept?
    case_user?
  end

  def refuse?
    case_user?
  end

  def finish?
    case_user?
  end

  def complete?
    @current_user.admin?
  end

  def reset?
    @current_user.admin?
  end

  def pdf?
    case_user_or_admin?
  end

  private

    def case_user?
      @model.user == @current_user
    end

    def case_new?
      @model.new?
    end

    def case_locked?
      @model.locked?
    end

    def case_finished?
      @model.finished?
    end

    def case_user_or_admin?
      @current_user.admin? || case_user?
    end
end
