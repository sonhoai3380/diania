# frozen_string_literal: true

class TrueClass
  def humanize
    I18n.t('core_ext.boolean.yes_title')
  end
end

class FalseClass
  def humanize
    I18n.t('core_ext.boolean.no_title')
  end
end

class NilClass
  def humanize
    I18n.t('core_ext.boolean.no_title')
  end

  def to_boolean
    ActiveRecord::Type::Boolean.new.cast(self)
  end
end
