# frozen_string_literal: true

module MethodsForExtendingDateTimeClasses
  def date_month_year
    strftime(date_formats[:dmy])
  end

  private

    def date_formats
      { dmy: '%d.%m.%Y' }
    end
end

class DateTime
  include MethodsForExtendingDateTimeClasses
end

class Date
  include MethodsForExtendingDateTimeClasses
end

class Time
  include MethodsForExtendingDateTimeClasses
end
