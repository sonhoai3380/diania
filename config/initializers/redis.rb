# frozen_string_literal: true

$redis = Redis::Namespace.new('dianino', redis: Redis.new)
