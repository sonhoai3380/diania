# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'high_voltage/pages#show', id: 'about'
  namespace :admin do
    resources :users do
      collection do
        post 'destroy_all', to: 'users#destroy_all', as: :destroy_all
      end
      member do
        get 'resend_confirmation_instructions', to: 'users#resend_confirmation_instructions',
                                                as: :resend_confirmation_instructions
      end
    end
  end
  devise_for :users, skip: [:registrations], controllers: { confirmations: 'confirmations' },
                     path: '', path_names: { sign_in: 'login', sign_out: 'logout' }
  as :user do
    get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
    put 'users' => 'devise/registrations#update', :as => 'user_registration'
  end
  resources :cases, except: %i[destroy] do
    resources :search_nannies, only: :index
    put 'reject', to: 'cases/states#reject', as: :reject
    put 'discard', to: 'cases/states#discard', as: :discard
    put 'request_nanny/:user_id', to: 'cases/states#request_nanny', as: :request_nanny
    get 'show_nanny/:id', to: 'users#show', as: :show_nanny
    put 'accept', to: 'cases/states#accept', as: :accept
    put 'refuse', to: 'cases/states#refuse', as: :refuse
    put 'finish', to: 'cases/states#finish', as: :finish
    put 'complete', to: 'cases/states#complete', as: :complete
    put 'reset', to: 'cases/states#reset', as: :reset
    get 'pdf', to: 'cases/pdf#pdf', as: :pdf
    collection do
      get 'csv', to: 'cases#csv_export', as: :csv
    end
  end

  resources :users, except: %i[new create]
end
