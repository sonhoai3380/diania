# frozen_string_literal: true

namespace :create do
  desc 'Import Data from Excel'
  task identifiers_for_cases: :environment do
    cases = Case.order(created_at: :asc).where(identifier: nil)

    cases.each do |the_case|
      the_case.child.language << the_case.child.attribute_in_database('language') if the_case.child.language.empty?

      the_case.generate_identifier
      the_case.save
    end
  end
end
