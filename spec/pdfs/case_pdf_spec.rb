# frozen_string_literal: true

require 'rails_helper'

describe CasePdf do
  describe '#new' do
    subject { described_class }
    let(:the_case) { create(:case, :finished) }
    let(:pdf) { subject.new(the_case) }
    let(:rendered_pdf) { pdf.render }
    let(:text_analysis) { PDF::Inspector::Text.analyze(rendered_pdf) }

    it 'should have case number on pdf' do
      expect(text_analysis.strings).to include("Abrechnung #{the_case.identifier}")
    end

    it 'should have full name on pdf' do
      expect(text_analysis.strings).to include(the_case.user_full_name)
    end
  end
end
