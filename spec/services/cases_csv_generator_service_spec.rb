# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CasesCsvGeneratorService, type: :service do
  describe '#initialize' do
    let(:cases) { double('cases') }
    let(:generator) { CasesCsvGeneratorService.new(cases) }

    it 'should assign cases' do
      expect(generator.cases).to eq cases
    end
  end

  describe '#call' do
    let(:cases) { double('cases') }
    let(:csv_file) { File.read(Rails.root.join('spec', 'support', 'files', 'correct_cases.csv')) }
    let(:generator) { described_class.new(cases) }

    context 'when cases exists' do
      let(:user) { build_stubbed(:user, name: 'John Doe') }
      let(:clinic) { build_stubbed(:clinic, name: 'Clinic1', zip: '20095') }
      let(:case1) { build_stubbed(:case, started_on: '2019-01-20') }
      let(:case2) { build_stubbed(:case, started_on: '2019-01-20') }

      let(:requestor1) { build_stubbed(:requestor, email: 'foobar@test.com', phone: '3200002') }
      let(:requestor2) { build_stubbed(:requestor, email: 'alexgreen@test.com', phone: '5200003') }
      let(:child1) { build_stubbed(:child, year_of_birth: 10, zip: '76234') }
      let(:child2) { build_stubbed(:child, year_of_birth: 7, zip: '61856') }

      let(:cases) { double('cases') }

      before do
        Timecop.freeze('2019-01-20 00:00')
        allow(case1).to receive(:user).and_return(user)
        allow(case1).to receive(:clinic).and_return(clinic)
        allow(case2).to receive(:user).and_return(user)
        allow(case2).to receive(:clinic).and_return(clinic)
        allow(case1).to receive(:child).and_return(child1)
        allow(case2).to receive(:child).and_return(child2)
        allow(case1).to receive(:requestor).and_return(requestor1)
        allow(case2).to receive(:requestor).and_return(requestor2)
        allow(cases).to receive(:find_each).and_yield(case1).and_yield(case2)
      end

      after { Timecop.return }

      it 'should return cases list in csv format' do
        expect(generator.call).to eq csv_file
      end
    end

    context 'when cases empty' do
      let(:cases) { Case.none }

      it 'should return empty content' do
        expect(generator.call).to be_blank
      end
    end
  end
end
