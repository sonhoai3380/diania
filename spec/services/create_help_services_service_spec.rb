# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreateHelpServicesService, type: :service do
  describe '.call' do
    it 'should create 7 help services' do
      expect { described_class.call }.to change { HelpService.count }.to(7)
    end
  end
end
