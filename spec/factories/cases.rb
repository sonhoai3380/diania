# frozen_string_literal: true

# == Schema Information
#
# Table name: cases
#
#  id                  :bigint(8)        not null, primary key
#  aasm_state          :string
#  comments            :text
#  comments_nanny      :text
#  commuting_allowance :decimal(10, 2)   default(0.0)
#  finished_on         :date
#  flyer_handed        :boolean
#  identifier          :string
#  initiated_by        :string
#  per_diem_allowance  :decimal(10, 2)   default(0.0)
#  started_on          :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  clinic_id           :bigint(8)
#  user_id             :bigint(8)
#
# Indexes
#
#  index_cases_on_clinic_id  (clinic_id)
#  index_cases_on_user_id    (user_id)
#

FactoryBot.define do
  factory :case do
    aasm_state { :new }
    started_on { '2019-01-14' }
    flyer_handed { true }
    initiated_by { :doctor }
    clinic
    comments { 'My comment' }

    after :build do |the_case|
      the_case.requestor ||= build(:requestor, case: the_case)
      the_case.child ||= build(:child, case: the_case)
    end

    trait :discarded do
      aasm_state { :discarded }
    end

    trait :rejected do
      aasm_state { :rejected }
    end

    trait :requested do
      user
      aasm_state { :requested }
    end

    trait :refused do
      user
      aasm_state { :refused }
    end

    trait :running do
      user
      aasm_state { :running }
    end

    trait :finished do
      user
      aasm_state { :finished }

      after :build do |the_case|
        the_case.case_deployments << build(:case_deployment, case: the_case)
      end
    end

    trait :completed do
      user
      aasm_state { :completed }
      commuting_allowance { Rails.application.secrets.commuting_allowance }
      per_diem_allowance { Rails.application.secrets.per_diem_allowance }

      after :build do |the_case|
        the_case.case_deployments << build(:case_deployment, case: the_case)
      end
    end
  end
end
