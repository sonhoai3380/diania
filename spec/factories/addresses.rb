# frozen_string_literal: true

# == Schema Information
#
# Table name: addresses
#
#  id                :bigint(8)        not null, primary key
#  addressable_scope :string
#  addressable_type  :string
#  city              :string
#  email_business    :string
#  first_name        :string
#  last_name         :string
#  phone_business    :string
#  phone_mobile      :string
#  phone_private     :string
#  salutation        :string
#  state             :string
#  street            :string
#  zip               :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  addressable_id    :bigint(8)
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#

FactoryBot.define do
  factory :address do
    salutation { 'Herr' }
    first_name { 'Max' }
    last_name { 'Mustermann' }
    street { 'Musterstraße 1' }
    zip { '20095' }
    city { 'Hamburg' }
    state { 'Hamburg' }
    email_business { 'text@test.de' }
    phone_business { '123456789' }
    phone_private { '123456789' }
    phone_mobile { '123456789' }
    addressable_scope { :address }

    trait :with_doctor do
      addressable_scope { :doctor_address }
    end

    trait :for_requestor do
      association :addressable, factory: :requestor
    end

    trait :for_user do
      association :addressable, factory: :user
    end
  end
end
