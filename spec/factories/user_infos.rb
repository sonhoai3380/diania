# frozen_string_literal: true

# == Schema Information
#
# Table name: user_infos
#
#  id                    :bigint(8)        not null, primary key
#  birthday              :date
#  occupation            :string
#  reference_to_diabetes :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  user_id               :bigint(8)
#
# Indexes
#
#  index_user_infos_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :user_info do
    occupation { 'Musterjob' }
    birthday { '2019-01-14' }
    reference_to_diabetes { %i[works_in_clinic] }
    user
  end
end
