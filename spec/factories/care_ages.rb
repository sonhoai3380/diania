# frozen_string_literal: true

# == Schema Information
#
# Table name: care_ages
#
#  id          :bigint(8)        not null, primary key
#  description :string
#  key         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :care_age do
    key { 'nanny' }
    description { 'Nanny (0 - 12 Jahre)' }
  end
end
