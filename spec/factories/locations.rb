# frozen_string_literal: true

# == Schema Information
#
# Table name: locations
#
#  id                :bigint(8)        not null, primary key
#  latitude          :float
#  locationable_type :string
#  longitude         :float
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  locationable_id   :bigint(8)
#
# Indexes
#
#  index_locations_on_locationable_type_and_locationable_id  (locationable_type,locationable_id)
#

FactoryBot.define do
  factory :location do
    locationable { build_stubbed(:address) }
    latitude { 0 }
    longitude { 0 }
  end
end
