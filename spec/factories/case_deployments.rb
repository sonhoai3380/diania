# frozen_string_literal: true

# == Schema Information
#
# Table name: case_deployments
#
#  id          :bigint(8)        not null, primary key
#  comment     :text
#  distance    :integer
#  duration    :integer
#  finished_on :datetime
#  started_on  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  case_id     :bigint(8)
#
# Indexes
#
#  index_case_deployments_on_case_id  (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (case_id => cases.id)
#

FactoryBot.define do
  factory :case_deployment do
    distance { 10 }
    duration { 10 }
    started_on { '2019-01-15 09:27:09' }
    finished_on { '2019-01-15 09:27:09' }
    comment { 'A comment' }

    after :build do |case_deployment|
      case_deployment.case ||= build(:case)
    end
  end
end
