# frozen_string_literal: true

# == Schema Information
#
# Table name: clinics
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  zip        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :clinic do
    name { 'Musterklinnik' }
    zip { '20095' }
  end
end
