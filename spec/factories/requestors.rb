# frozen_string_literal: true

# == Schema Information
#
# Table name: requestors
#
#  id         :bigint(8)        not null, primary key
#  confirmed  :boolean
#  consent    :boolean
#  email      :string
#  function   :string
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  case_id    :bigint(8)
#
# Indexes
#
#  index_requestors_on_case_id  (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (case_id => cases.id)
#

FactoryBot.define do
  factory :requestor do
    function { :doctor }
    consent { true }
    confirmed { true }
    email { 'max@mustermann.de' }
    phone { '04076512345' }

    after :build do |requestor|
      requestor.address = build(:address)
      requestor.doctor_address = build(:address, :with_doctor)
      requestor.case ||= build(:case, requestor: requestor)
    end
  end
end
