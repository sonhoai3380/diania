# frozen_string_literal: true

# == Schema Information
#
# Table name: children
#
#  id                   :bigint(8)        not null, primary key
#  celiac_disease       :boolean
#  chronic_illness_type :string
#  diseases             :json
#  environment          :string
#  gender               :string
#  language             :string
#  language_other       :string
#  manifested_on        :date
#  nationality          :string
#  nationality_other    :string
#  operational_env      :string
#  operational_reason   :string
#  phone                :string
#  refugee              :boolean
#  state                :string
#  therapy              :string
#  year_of_birth        :integer
#  zip                  :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  care_age_id          :bigint(8)
#  case_id              :bigint(8)
#
# Indexes
#
#  index_children_on_care_age_id  (care_age_id)
#  index_children_on_case_id      (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (care_age_id => care_ages.id)
#  fk_rails_...  (case_id => cases.id)
#

FactoryBot.define do
  factory :child do
    gender { :male }
    year_of_birth { 2010 }
    phone { '040/12345678' }
    manifested_on { '2019-01-15' }
    zip { '20095' }
    state { 'Hamburg' }
    environment { :both_parents }
    therapy { 'Therapy' }
    nationality { :de }
    refugee { false }
    diseases { { mental_handicap: 'No', chronic_illness: 'No', other_general_comments: 'No' } }
    operational_env { [:care_center] }
    operational_reason { [:support_after_diagnosis] }
    language { [:de] }
    association :case, factory: :case
    care_age
    celiac_disease { false }

    after :build do |child|
      child.case ||= build(:case, child: child)
    end
  end
end
