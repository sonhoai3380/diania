# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  confirmation_sent_at   :datetime
#  confirmation_token     :string
#  confirmed_at           :datetime
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  language               :string
#  language_other         :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  name                   :string
#  nda                    :boolean
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :string
#  sign_in_count          :integer          default(0), not null
#  unconfirmed_email      :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

FactoryBot.define do
  factory :user do
    confirmed_at { Time.current.to_s }
    name { 'Test User' }
    sequence(:email) { |n| "person#{n}@example.com" }
    password { 'please123' }
    nda { 1 }
    language { [:de] }

    transient do
      first_name { 'Max' }
      last_name { 'Mustermann' }
    end

    trait :admin do
      role { :admin }
    end

    after :build do |user, evaluator|
      user.address ||= build(:address,
                             first_name: evaluator.first_name,
                             last_name: evaluator.last_name,
                             addressable: user)
      user.bank_account ||= build(:bank_account, user: user)
      user.user_info ||= build(:user_info, user: user)
      user.care_ages << create(:care_age)
      user.help_services << create(:help_service)
    end
  end
end
