# frozen_string_literal: true

# == Schema Information
#
# Table name: help_services
#
#  id          :bigint(8)        not null, primary key
#  description :string
#  key         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :help_service do
    key { 'support_after_diagnose' }
    description { 'Unterstützung nach der Diagnose, zum Einfinden in die neue Lebenssituation.' }
  end
end
