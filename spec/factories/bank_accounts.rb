# frozen_string_literal: true

# == Schema Information
#
# Table name: bank_accounts
#
#  id             :bigint(8)        not null, primary key
#  account_holder :string
#  bank_name      :string
#  bic            :string
#  iban           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint(8)
#
# Indexes
#
#  index_bank_accounts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

FactoryBot.define do
  factory :bank_account do
    iban { 'DE21123456780012345678' }
    bic { 'BFSWDE33MUE' }
    bank_name { 'Musterbank' }
    account_holder { 'Max Mustermann' }
  end
end
