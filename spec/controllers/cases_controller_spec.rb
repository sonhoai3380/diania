# frozen_string_literal: true

require 'wice_grid'

RSpec.describe CasesController, type: :controller do
  include Wice::Controller

  let(:admin) { build_stubbed(:user, :admin) }

  before { sign_in_as(admin) }

  describe 'GET index' do
    include Wice::Controller
    let(:cases) { double('CasesAssociation') }
    let(:cases_grid) { double }

    before do
      allow(controller).to receive(:policy_scope).with(Case).and_return(cases)
      allow(controller).to receive(:export_grid_if_requested).with('cases' => 'cases_grid').and_return(false)
      allow(controller).to receive(:initialize_grid)
        .with(cases, include: %i[child requestor],
                     name: 'cases',
                     order: 'cases.id',
                     order_direction: 'desc',
                     enable_export_to_csv: true,
                     csv_file_name: 'cases',
                     csv_field_separator: ';',
                     csv_encoding: 'UTF-8')
        .and_return(cases_grid)
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render index' do
      expect(response).to render_template(:index)
    end

    it 'should assign cases_grid' do
      expect(assigns(:cases_grid)).to be == cases_grid
    end
  end

  describe 'GET new' do
    let(:form) { double('CaseCreationForm') }

    before do
      expect(CaseCreationForm).to receive(:new).and_return(form)
      expect(form).to receive(:prebuild_help_service_assignments)
      get :new
    end

    it 'returns http success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render new' do
      expect(response).to render_template(:new)
    end

    it 'should assign form' do
      expect(assigns(:form)).to be == form
    end
  end

  describe 'POST create' do
    let(:cs) { build_stubbed(:case) }
    let(:form) { double('CaseCreationForm', cs: cs) }
    let(:params) do
      ActionController::Parameters.new(
        address_first_name: 'First name', address_last_name: 'Last name',
        doctor_address_name: 'Megan Fox', requestor_phone: '32445424',
        terms_of_use: true, requestor_function: 'doctor', requestor_email: 'john@mail.com',
        requestor_consent: true, clinic_zip: '342234', clinic_name: 'Die Klinik',
        requestor_confirmed: true, child_zip: '6432', child_care_age_id: 1, child_gender: 'male',
        child_language: 'de', child_manifested_on: '2019-01-25', comments: 'Wunschnanny: Maria Mustermann'
      ).permit!
    end

    before do
      expect(CaseCreationForm).to receive(:new).and_return(form)
    end

    context 'with valid params' do
      before do
        allow(form).to receive(:save).and_return(true)
        post :create, params: { case: params }
      end

      it 'should redirect' do
        expect(response).to render_template(:create)
      end
    end

    context 'with invalid params' do
      before do
        allow(form).to receive(:save).and_return(false)
        post :create, params: { case: params }
      end

      it 'should success' do
        expect(response).to have_http_status(:successful)
      end

      it 'should render template' do
        expect(response).to render_template(:new)
      end

      it 'should assign form' do
        expect(assigns(:form)).to be == form
      end
    end
  end

  describe 'GET edit' do
    let(:cs) { build_stubbed(:case) }

    before do
      expect(Case).to receive(:find).and_return(cs)
      get :edit, params: { id: 1 }
    end

    it 'should be success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render template' do
      expect(response).to render_template(:edit)
    end

    it 'should assign case' do
      expect(assigns(:case)).to be == cs
    end
  end

  describe 'GET csv_export' do
    before do
      get :csv_export
    end
    it 'should redirect' do
      expect(response).to be_redirect
      expect(response).to redirect_to(cases_path(cases: { export: 'csv' }))
    end
  end

  describe 'GET show' do
    let(:cs) { build_stubbed(:case) }

    before do
      expect(Case).to receive(:find).and_return(cs)
      get :show, params: { id: 1 }
    end

    it 'should be success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render template' do
      expect(response).to render_template(:show)
    end

    it 'should assign case' do
      expect(assigns(:case)).to be == cs
    end
  end

  describe 'PUT update' do
    let(:cs) { build_stubbed(:case) }
    let(:params) { attributes_for(:case) }

    before do
      expect(Case).to receive(:find).and_return(cs)
    end

    context 'with valid params' do
      before do
        allow(cs).to receive(:update).and_return(true)
        put :update, params: { id: cs.to_param, case: params }
      end

      it 'should redirect' do
        expect(response).to be_redirect
        expect(response).to redirect_to(edit_case_path(cs))
      end
    end

    context 'with invalid params' do
      before do
        allow(cs).to receive(:update).and_return(false)
        put :update, params: { id: cs.to_param, case: params }
      end

      it 'should success' do
        expect(response).to have_http_status(:successful)
      end

      it 'should render template' do
        expect(response).to render_template(:edit)
      end

      it 'should assign case' do
        expect(assigns(:case)).to be == cs
      end
    end
  end
end
