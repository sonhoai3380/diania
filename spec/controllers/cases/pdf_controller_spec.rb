# frozen_string_literal: true

RSpec.describe Cases::PdfController, type: :controller do
  describe 'GET pdf' do
    let(:the_case) { build_stubbed(:case) }
    let(:user) { build_stubbed(:user) }
    let(:case_pdf) { double('CasePDF', render: 'the-pdf-content') }

    before do
      sign_in_as(user)

      allow(Case).to receive(:find).and_return(the_case)
      allow(the_case).to receive(:user).and_return(user)
      allow(CasePdf).to receive(:new).and_return(case_pdf)
    end

    context 'on success' do
      before do
        get :pdf, params: { case_id: the_case.id }
      end

      it 'redirects to cases path' do
        expect(response).to have_http_status(:successful)
      end
    end
  end
end
