# frozen_string_literal: true

RSpec.describe Cases::StatesController, type: :controller do
  describe 'PUT reject' do
    let(:admin) { build_stubbed(:user, :admin) }
    let(:the_case) { build_stubbed(:case) }
    let(:user) { build_stubbed(:user) }

    before do
      sign_in_as(admin)

      allow(Case).to receive(:find).and_return(the_case)
      allow(User).to receive(:find).and_return(user)
    end

    context 'on success' do
      before do
        allow(the_case).to receive(:may_reject?).and_return(true)
        allow(the_case).to receive(:reject!).and_return(true)

        put :reject, params: { case_id: the_case.id }
      end

      it 'should have success flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.reject.success'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end

    context 'on failure' do
      before do
        allow(the_case).to receive(:may_reject?).and_return(false)

        put :reject, params: { case_id: the_case.id }
      end

      it 'should have error flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.reject.error'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end
  end

  describe 'PUT discard' do
    let(:admin) { build_stubbed(:user, :admin) }
    let(:the_case) { build_stubbed(:case) }
    let(:user) { build_stubbed(:user) }

    before do
      sign_in_as(admin)

      allow(Case).to receive(:find).and_return(the_case)
      allow(User).to receive(:find).and_return(user)
    end

    context 'on success' do
      before do
        allow(the_case).to receive(:may_discard?).and_return(true)
        allow(the_case).to receive(:discard!).and_return(true)

        put :discard, params: { case_id: the_case.id, discard: { message: 'Ein Grund' } }
      end

      it 'should have success flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.discard.success'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end

    context 'on failure' do
      before do
        allow(the_case).to receive(:may_discard?).and_return(false)

        put :discard, params: { case_id: the_case.id, discard: { message: 'Ein Grund' } }
      end

      it 'should have error flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.discard.error'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end
  end

  describe 'PUT request_nanny' do
    let(:admin) { build_stubbed(:user, :admin) }
    let(:the_case) { build_stubbed(:case) }
    let(:user) { build_stubbed(:user) }

    before do
      sign_in_as(admin)

      allow(Case).to receive(:find).and_return(the_case)
      allow(User).to receive(:find).and_return(user)
    end

    context 'on success' do
      before do
        allow(the_case).to receive(:may_request?).and_return(true)
        allow(the_case).to receive(:request!).with(user).and_return(true)
        allow(the_case).to receive(:update).with(user: user).and_return(true)

        put :request_nanny, params: { case_id: the_case.id, user_id: user.id }
      end

      it 'should have success flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.request_nanny.success'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end

    context 'on failure' do
      before do
        allow(the_case).to receive(:may_request?).and_return(false)

        put :request_nanny, params: { case_id: the_case.id, user_id: user.id }
      end

      it 'should have error flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.request_nanny.error'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end
  end

  describe 'PUT accept' do
    let(:the_case) { build_stubbed(:case, user: user) }
    let(:user) { build_stubbed(:user) }

    before do
      sign_in_as(user)

      allow(Case).to receive(:find).and_return(the_case)
      allow(User).to receive(:find).and_return(user)
    end

    context 'on success' do
      before do
        allow(the_case).to receive(:may_run?).and_return(true)
        allow(the_case).to receive(:run!).and_return(true)

        put :accept, params: { case_id: the_case.id }
      end

      it 'should have success flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.accept.success'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(edit_case_path(the_case))
      end
    end

    context 'on failure' do
      before do
        allow(the_case).to receive(:may_run?).and_return(false)

        put :accept, params: { case_id: the_case.id }
      end

      it 'should have error flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.accept.error'))
      end

      it 'renders template show' do
        expect(response).to render_template(:show)
      end
    end
  end

  describe 'PUT refuse' do
    let(:the_case) { build_stubbed(:case, user: user) }
    let(:user) { build_stubbed(:user) }

    before do
      sign_in_as(user)

      allow(Case).to receive(:find).and_return(the_case)
      allow(User).to receive(:find).and_return(user)
    end

    context 'on success' do
      before do
        allow(the_case).to receive(:may_refuse?).and_return(true)
        allow(the_case).to receive(:refuse!).and_return(true)

        put :refuse, params: { case_id: the_case.id }
      end

      it 'should have success flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.refuse.success'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end

    context 'on failure' do
      before do
        allow(the_case).to receive(:may_refuse?).and_return(false)

        put :refuse, params: { case_id: the_case.id }
      end

      it 'should have error flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.refuse.error'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end
  end

  describe 'PUT finish' do
    let(:the_case) { build_stubbed(:case, user: user) }
    let(:user) { build_stubbed(:user) }

    before do
      sign_in_as(user)

      allow(Case).to receive(:find).and_return(the_case)
      allow(User).to receive(:find).and_return(user)
    end

    context 'on success' do
      before do
        allow(the_case).to receive(:may_finish?).and_return(true)
        allow(the_case).to receive(:finish!).and_return(true)

        put :finish, params: { case_id: the_case.id }
      end

      it 'should have success flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.finish.success'))
      end

      it 'redirects to edit case path' do
        expect(response).to redirect_to(cases_path)
      end
    end

    context 'on failure' do
      before do
        allow(the_case).to receive(:may_finish?).and_return(false)

        put :finish, params: { case_id: the_case.id }
      end

      it 'should have error flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.finish.error'))
      end

      it 'renders template edit' do
        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'PUT complete' do
    let(:the_case) { build_stubbed(:case, user: user) }
    let(:user) { build_stubbed(:user) }
    let(:admin) { build_stubbed(:user, :admin) }

    before do
      sign_in_as(admin)

      allow(Case).to receive(:find).and_return(the_case)
      allow(User).to receive(:find).and_return(admin)
    end

    context 'on success' do
      before do
        allow(the_case).to receive(:may_complete?).and_return(true)
        allow(the_case).to receive(:complete!).and_return(true)

        put :complete, params: { case_id: the_case.id }
      end

      it 'should have success flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.complete.success'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end

    context 'on failure' do
      before do
        allow(the_case).to receive(:may_complete?).and_return(false)

        put :complete, params: { case_id: the_case.id }
      end

      it 'should have error flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.complete.error'))
      end

      it 'renders template show' do
        expect(response).to render_template(:show)
      end
    end
  end

  describe 'PUT reset' do
    let(:the_case) { build_stubbed(:case, user: user) }
    let(:user) { build_stubbed(:user) }
    let(:admin) { build_stubbed(:user, :admin) }

    before do
      sign_in_as(admin)

      allow(Case).to receive(:find).and_return(the_case)
      allow(User).to receive(:find).and_return(admin)
    end

    context 'on success' do
      before do
        allow(the_case).to receive(:may_reset?).and_return(true)
        allow(the_case).to receive(:reset!).and_return(true)

        put :reset, params: { case_id: the_case.id }
      end

      it 'should have success flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.reset.success'))
      end

      it 'redirects to cases path' do
        expect(response).to redirect_to(cases_path)
      end
    end

    context 'on failure' do
      before do
        allow(the_case).to receive(:may_complete?).and_return(false)

        put :reset, params: { case_id: the_case.id }
      end

      it 'should have error flash notification' do
        expect(flash[:notice]).to eq(I18n.t('cases.states.reset.error'))
      end

      it 'renders template show' do
        expect(response).to render_template(:show)
      end
    end
  end
end
