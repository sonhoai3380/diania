# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::UsersController, type: :controller do
  let(:admin) { build_stubbed(:user, :admin) }

  before { sign_in_as(admin) }

  describe 'GET index' do
    let(:users) { double('UsersAssociation') }
    let(:users_grid) { double }

    before do
      allow(controller).to receive(:policy_scope).with(User).and_return(users)
      allow(controller).to receive(:export_grid_if_requested).with('users' => 'users_grid').and_return(false)
      allow(controller).to receive(:initialize_grid)
        .with(users,
              include: %i[address], name: 'users',
              enable_export_to_csv: true, csv_file_name: 'users', csv_field_separator: ';',
              order: 'users.id', order_direction: 'desc',
              custom_order: {
                'users.language' => 'lower(users.language)',
                'users.email' => 'lower(users.email)',
                'addresses.last_name' => 'lower(addresses.last_name)',
                'addresses.first_name' => 'lower(addresses.first_name)'
              })
        .and_return(users_grid)
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render index' do
      expect(response).to render_template(:index)
    end

    it 'should assign users' do
      expect(assigns(:users_grid)).to be == users_grid
    end
  end

  describe 'GET show' do
    before do
      expect(User).to receive(:find).and_return(admin)
      get :show, params: { id: admin.id }
    end

    it 'should be success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render template' do
      expect(response).to render_template(:show)
    end

    it 'should assign case' do
      expect(assigns(:user)).to be == admin
    end
  end

  describe 'PUT update' do
    let(:params) { attributes_for(:user, :admin).merge(email: 'new_email@example.com') }

    before do
      expect(User).to receive(:find).and_return(admin)
    end

    context 'with valid params' do
      before do
        allow(admin).to receive(:update).and_return(true)
        put :update, params: { id: admin.to_param, user: params }
      end

      it 'should redirect' do
        expect(response).to be_redirect
        expect(response).to redirect_to(edit_admin_user_path)
      end
    end

    context 'with invalid params' do
      before do
        allow(admin).to receive(:update).and_return(false)
        put :update, params: { id: admin.to_param, user: params }
      end

      it 'should success' do
        expect(response).to have_http_status(:redirect)
      end

      it 'should redirect' do
        expect(response).to be_redirect
        expect(response).to redirect_to(edit_admin_user_path)
      end
    end
  end

  describe 'GET destroy' do
    let(:user) { build_stubbed(:user) }

    context 'with success' do
      before do
        expect(User).to receive(:find).and_return(user)
        expect(user).to receive(:destroy).and_return(true)
        delete :destroy, params: { id: 1 }
      end

      it 'should be success' do
        expect(response).to have_http_status(:redirect)
      end

      it 'should redirect' do
        expect(response).to be_redirect
        expect(response).to redirect_to(admin_users_path)
      end
    end

    context 'without success' do
      before do
        expect(User).to receive(:find).and_return(user)
        expect(user).to receive(:destroy).and_return(false)
        delete :destroy, params: { id: 1 }
      end

      it 'should be success' do
        expect(response).to have_http_status(:redirect)
      end

      it 'should redirect' do
        expect(response).to be_redirect
        expect(response).to redirect_to(admin_user_path(user))
      end
    end
  end

  describe 'GET resend_confirmation_instructions' do
    let(:user) { build_stubbed(:user) }

    before do
      expect(User).to receive(:find).and_return(user)
      expect(user).to receive(:send_confirmation_instructions)
      expect(user).to receive(:update)
      get :resend_confirmation_instructions, params: { id: 1 }
    end

    it 'should be success' do
      expect(response).to have_http_status(:redirect)
    end

    it 'should redirect' do
      expect(response).to be_redirect
      expect(response).to redirect_to(admin_users_path)
    end
  end
end
