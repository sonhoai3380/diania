# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:admin) { build_stubbed(:user, :admin) }

  before { sign_in_as(admin) }

  describe 'GET index' do
    let(:users) { double('UsersAssociation') }

    before do
      expect(User).to receive(:all).and_return(users)
      get :index
    end

    it 'returns http success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render index' do
      expect(response).to render_template(:index)
    end

    it 'should assign users' do
      expect(assigns(:users)).to be == users
    end
  end

  describe 'GET show' do
    before do
      expect(User).to receive(:find).and_return(admin)
      get :show, params: { id: admin.id }
    end

    it 'should be success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render template' do
      expect(response).to render_template(:show)
    end

    it 'should assign case' do
      expect(assigns(:user)).to be == admin
    end
  end

  describe 'PUT update' do
    let(:params) { attributes_for(:user, :admin).merge(email: 'new_email@example.com') }

    before do
      expect(User).to receive(:find).and_return(admin)
    end

    context 'with valid params' do
      before do
        allow(admin).to receive(:update).and_return(true)
        put :update, params: { id: admin.to_param, user: params }
      end

      it 'should redirect' do
        expect(response).to be_redirect
        expect(response).to redirect_to(edit_user_path)
      end
    end

    context 'with invalid params' do
      before do
        allow(admin).to receive(:update).and_return(false)
        put :update, params: { id: admin.to_param, user: params }
      end

      it 'should success' do
        expect(response).to have_http_status(:ok)
      end

      it 'should render edit template' do
        expect(response).to render_template(:edit)
      end

      it 'should assign case' do
        expect(assigns(:user)).to be == admin
      end
    end
  end

  describe 'GET destroy' do
    let(:admin) { build_stubbed(:user) }

    before do
      expect(User).to receive(:find).and_return(admin)
      expect(admin).to receive(:destroy)
      delete :destroy, params: { id: 1 }
    end

    it 'should be success' do
      expect(response).to have_http_status(:redirect)
    end

    it 'should redirect' do
      expect(response).to be_redirect
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
