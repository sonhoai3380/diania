# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SearchNanniesController, type: :controller do
  include Wice::Controller

  let(:admin) { build_stubbed(:user, :admin) }

  before { sign_in_as(admin) }

  describe 'GET index' do
    let(:users) { double }
    let(:the_case) { build_stubbed(:case) }
    let(:coordinates) { double }
    let(:grid) { double }

    before do
      allow(Case).to receive(:find).with(the_case.to_param).and_return(the_case)
      expect(the_case).to receive(:child_location_coordinates).and_return(coordinates)
      allow(User).to receive_message_chain(:nearby_nannies_by_coordinates, :without_refusals) { users }
      allow(controller).to receive(:initialize_grid).with(users).and_return(grid)
      get :index, params: { case_id: the_case.to_param }
    end

    it 'returns http success' do
      expect(response).to have_http_status(:successful)
    end

    it 'should render index' do
      expect(response).to render_template(:index)
    end
  end
end
