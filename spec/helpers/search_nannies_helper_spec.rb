# frozen_string_literal: true

require 'rails_helper'

describe SearchNanniesHelper, type: :helper do
  describe '#for_google_maps' do
    let(:the_case) { build_stubbed(:case) }
    let(:user) { create(:user) }

    it 'should return array for options_for_select' do
      expect(helper.for_google_maps([user])).to eq(
        [{
          id: user.id,
          address: {
            location_coordinates: [53.5510010700068, 9.9996662352794],
            full_name: 'Max Mustermann'
          }
        }].to_json
      )
    end
  end
end
