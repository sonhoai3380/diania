# frozen_string_literal: true

require 'rails_helper'

describe CasesHelper, type: :helper do
  describe '#care_ages_list' do
    let(:list) { [['nunny', 0]] }

    it 'should return array for options_for_select' do
      allow(CareAge).to receive(:pluck).with(:description, :id).and_return(list)
      expect(helper.care_ages_list).to eq(list)
    end
  end

  describe '#functions_list' do
    let(:list) { [%w[Doctor doctor]] }

    it 'should return array for options_for_select' do
      allow(Requestor).to receive_message_chain(:function, :options).and_return(list)
      expect(helper.functions_list).to eq(list)
    end
  end

  describe '#gender_list' do
    let(:list) { [%w[Male male]] }

    it 'should return array for options_for_select' do
      allow(Child).to receive_message_chain(:gender, :options).and_return(list)
      expect(helper.gender_list).to eq(list)
    end
  end

  describe '#languages_list' do
    let(:list) { [%w[English en]] }

    it 'should return array for options_for_select' do
      allow(Child).to receive_message_chain(:language, :options).and_return(list)
      expect(helper.languages_list).to eq(list)
    end
  end

  describe '#check_yes_for_radio_button?' do
    it 'should return true' do
      expect(helper.check_yes_for_radio_button?('true')).to eq(true)
    end

    it 'should return false' do
      expect(helper.check_yes_for_radio_button?('false')).to eq(false)
    end
  end

  context '#dashboard_case_link' do
    it 'should return show path for new case' do
      the_case = build_stubbed(:case)
      expect(helper.dashboard_case_link(the_case)).to eq(case_path(the_case))
    end

    it 'should return show path for requested case' do
      the_case = build_stubbed(:case, :requested)
      expect(helper.dashboard_case_link(the_case)).to eq(case_path(the_case))
    end

    it 'should return show path for refused case' do
      the_case = build_stubbed(:case, :refused)
      expect(helper.dashboard_case_link(the_case)).to eq(case_path(the_case))
    end

    it 'should return edit path for running case for a nanny' do
      allow(controller).to receive(:current_user).and_return(build_stubbed(:user))

      the_case = build_stubbed(:case, :running)
      expect(helper.dashboard_case_link(the_case)).to eq(edit_case_path(the_case))
    end

    it 'should return show path for running case for an admin' do
      allow(controller).to receive(:current_user).and_return(build_stubbed(:user, :admin))

      the_case = build_stubbed(:case, :running)
      expect(helper.dashboard_case_link(the_case)).to eq(case_path(the_case))
    end

    it 'should return show path for completed case' do
      the_case = build_stubbed(:case, :completed)
      expect(helper.dashboard_case_link(the_case)).to eq(case_path(the_case))
    end

    it 'should return show path for finished case' do
      the_case = build_stubbed(:case, :finished)
      expect(helper.dashboard_case_link(the_case)).to eq(case_path(the_case))
    end
  end

  describe '#sum_for_deployments' do
    it 'should return sum of deployments' do
      the_case = build_stubbed(:case, :finished)
      allow(the_case).to receive_message_chain(:case_deployments, :count).and_return(3)

      expect(helper.sum_for_deployments(the_case)).to eq(120)
    end
    it 'should return sum of deployments when completed' do
      the_case = build_stubbed(:case, :completed)
      allow(the_case).to receive_message_chain(:case_deployments, :count).and_return(3)

      expect(helper.sum_for_deployments(the_case)).to eq(120)
    end
  end

  describe '#sum_for_distance' do
    it 'should return sum of distance' do
      the_case = build_stubbed(:case, :finished)
      allow(the_case).to receive_message_chain(:case_deployments, :sum).and_return(22)

      expect(helper.sum_for_distance(the_case)).to eq(6.6)
    end

    it 'should return sum of distance when completed' do
      the_case = build_stubbed(:case, :completed)
      allow(the_case).to receive_message_chain(:case_deployments, :sum).and_return(22)

      expect(helper.sum_for_distance(the_case)).to eq(6.6)
    end
  end

  describe '#sum_for_all' do
    it 'should return sum of deployments and distance' do
      the_case = build_stubbed(:case, :finished)
      allow(helper).to receive(:sum_for_deployments).and_return(60)
      allow(helper).to receive(:sum_for_distance).and_return(440)

      expect(helper.sum_for_all(the_case)).to eq(500)
    end
  end

  describe '#show_case_completed_details?' do
    it 'should false for new case' do
      the_case = build_stubbed(:case)

      expect(helper.show_case_completed_details?(the_case)).to eq(false)
    end

    it 'should true for finished case' do
      the_case = build_stubbed(:case, :finished)

      expect(helper.show_case_completed_details?(the_case)).to eq(true)
    end
  end

  describe '#therapy_list' do
    let(:list) { [%w[ICT true], %w[Pumpe false]] }

    it 'should return array for options_for_select' do
      expect(helper.therapy_list).to eq(list)
    end
  end

  describe '#child_chronic_illness' do
    let(:list) { %w[1 illness] }
    let(:list_negative) { [0, ''] }
    it 'should return string with Ja and the name of disease' do
      expect(helper.child_chronic_illness(*list)).to eq('Ja, Illness')
    end

    it 'should return string with only Ja' do
      expect(helper.child_chronic_illness(*list_negative)).to eq('Nein')
    end
  end
end
