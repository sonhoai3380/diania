# frozen_string_literal: true

require 'rails_helper'

describe UsersHelper, type: :helper do
  describe '#reference_to_diabetes_list' do
    let(:list) do
      [[I18n.t('enumerize.user_info.reference_to_diabetes.works_in_clinic'), :works_in_clinic]]
    end

    it 'should return array for options_for_select' do
      allow(UserInfo).to receive_message_chain(:reference_to_diabetes, :options).and_return(list)
      expect(helper.reference_to_diabetes_list).to eq(list)
    end
  end

  describe '#roles' do
    let(:roles) do
      %w[nanny admin].map { |r| [r&.capitalize, r] }
    end

    it 'should return array for all roles' do
      expect(UsersHelper.roles).to eq(roles)
    end
  end

  describe '#all_languages' do
    let(:all_languages) { [%w[Deutsch de], %w[Englisch en], %w[Russisch ru], %w[Arabisch ar], %w[Sonstige other]] }

    it 'should return array for all languages' do
      expect(helper.all_languages).to eq(all_languages)
    end
  end

  describe '#reference_to_diabetes_text' do
    context 'when exists' do
      let(:reference_to_diabetes) { double(texts: %w[text1 text2]) }

      it 'should return joined texts' do
        expect(helper.reference_to_diabetes_text(reference_to_diabetes)).to eq('text1, text2')
      end
    end

    context 'when blank' do
      let(:reference_to_diabetes) { double(texts: []) }

      it 'should return empty value' do
        expect(helper.reference_to_diabetes_text(reference_to_diabetes)).to be_blank
      end
    end
  end
end
