# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserCreationForm, type: :form do
  let(:help_service_1) { create(:help_service) }
  let(:help_service_2) { create(:help_service) }
  let(:help_service_assignments_attributes) do
    {
      '0' => { help_service_id: help_service_1.to_param, assignee_type: 'User', assignee_id: 0 },
      '1' => { help_service_id: help_service_2.to_param, assignee_type: 'User', assignee_id: 1 }
    }
  end

  let(:care_age_1) { create(:care_age) }
  let(:care_age_2) { create(:care_age) }
  let(:care_ages_users_attributes) do
    {
      '0' => { care_age_id: care_age_1.to_param, user_id: 0 },
      '1' => { care_age_id: care_age_2.to_param, user_id: 1 }
    }
  end
  let(:valid_params) do
    ActionController::Parameters.new(
      address_salutation: 'test', address_first_name: 'Jana', address_last_name: 'Fruehauf',
      address_street: 'Schulstrasse 4', address_zip: '20095', address_city: 'Bad Oeynhausen',
      user_email: 'test@example.com',
      user_role: 'nanny', user_password: 'test1234', user_password_confirmation: 'test1234',
      user_language: 'de', user_language_other: 'italienisch', user_info_birthday: '2019-01-25',
      user_info_occupation: 'test', address_phone_private: '0113232320', address_phone_mobile: '0113232320',
      address_phone_business: '0113232320', address_email_business: 'test@example.com',
      bank_account_account_holder: 'test', bank_account_iban: 'DE89 3704 0044 0532 0130 00',
      bank_account_bic: 'AABSDE31', user_info_reference_to_diabetes: %i[works_in_clinic],
      help_service_assignments_attributes: help_service_assignments_attributes,
      care_ages_users_attributes: care_ages_users_attributes
    ).permit!
  end

  let(:form) { UserCreationForm.new(valid_params) }

  context 'validations' do
    before do
      form.user.password = valid_params[:user_password]
      form.user.password_confirmation = valid_params[:user_password_confirmation]
    end

    it 'is valid form' do
      expect(form.valid?).to be_truthy
    end

    it 'is not valid without address_salutation' do
      form.address_salutation = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without address_first_name' do
      form.address_first_name = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without address_last_name' do
      form.address_last_name = nil
      expect(form).to_not be_valid
    end

    it 'is valid without at least one help_service_assignments' do
      form.help_service_assignments = []
      expect(form).to be_valid
    end

    it 'is valid without at least one care_age' do
      form.care_ages_users = []
      expect(form).to be_valid
    end
  end

  context 'with strict validation' do
    before do
      form.user.strict_validation = true
    end

    it 'is not a valid form' do
      expect(form.valid?).to be_falsy
    end

    context 'without care ages or help services' do
      let(:help_service_assignments_attributes) do
        {
          '0' => { help_service_id: help_service_1.to_param, assignee_type: 'User', assignee_id: nil },
          '1' => { help_service_id: help_service_2.to_param, assignee_type: 'User', assignee_id: nil }
        }
      end
      let(:care_ages_users_attributes) do
        {
          '0' => { care_age_id: care_age_1.to_param, user_id: nil },
          '1' => { care_age_id: care_age_2.to_param, user_id: nil }
        }
      end

      it 'is not a valid form' do
        expect(form.valid?).to be_falsy
      end
    end
  end

  context 'with valid params' do
    before do
      form.user.password = valid_params[:user_password]
      form.user.password_confirmation = valid_params[:user_password_confirmation]
    end
    it 'should create user' do
      expect { form.save }.to change { User.count }.by 1
    end

    it 'should create address' do
      expect { form.save }.to change { Address.count }.by 1
    end

    it 'should create user_info' do
      expect { form.save }.to change { UserInfo.count }.by 1
    end

    it 'should create bank_account' do
      expect { form.save }.to change { BankAccount.count }.by 1
    end

    it { expect(form.save).to be_truthy }

    it { expect(form.prebuild_help_service_assignments.count).to eq 2 }
    it { expect(form.prebuild_care_ages.count).to eq 2 }
  end

  context 'with invalid params' do
    let(:valid_params) do
      ActionController::Parameters.new(
        user_email: 'test@example.com',
        help_service_assignments_attributes: {},
        care_ages_users_attributes: {}
      ).permit!
    end

    it { expect(form.save).not_to be_truthy }
  end
end
