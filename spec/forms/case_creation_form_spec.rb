# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CaseCreationForm, type: :form do
  let(:help_service_1) { create(:help_service) }
  let(:help_service_2) { create(:help_service) }
  let(:help_service_assignments_attributes) do
    {
      '0' => { help_service_id: help_service_1.to_param, assignee_type: 'Case', assignee_id: 0 },
      '1' => { help_service_id: help_service_2.to_param, assignee_type: 'Case', assignee_id: 1 }
    }
  end
  let(:valid_params) do
    ActionController::Parameters.new(
      address_first_name: 'First name', address_last_name: 'Last name',
      doctor_address_name: 'Megan Fox', requestor_phone: '32445424',
      terms_of_use: true, requestor_function: 'doctor', requestor_email: 'john@mail.com',
      requestor_consent: true, clinic_zip: '342234', clinic_name: 'Die Klinik',
      requestor_confirmed: true, child_zip: '20095', child_gender: 'male',
      child_language: ['de'], child_manifested_on: '2019-01-25', child_year_of_birth: 2010,
      help_service_assignments_attributes: help_service_assignments_attributes,
      comments: 'Wunschnanny: Maria Mustermann'
    ).permit!
  end
  let(:form) { CaseCreationForm.new(valid_params) }

  context 'validations' do
    it 'is valid form' do
      expect(form.valid?).to be_truthy
    end

    it 'is not valid without address_first_name' do
      form.address_first_name = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without address_last_name' do
      form.address_last_name = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without requestor_phone' do
      form.requestor_phone = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without accepted terms_of_use' do
      form.terms_of_use = false
      expect(form).to_not be_valid
    end

    it 'is not valid without at least one help_service_assignments' do
      form.help_service_assignments = []
      expect(form).to_not be_valid
    end

    it 'is not valid without requestor_function' do
      form.requestor_function = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without requestor_email' do
      form.requestor_email = nil
      expect(form).to_not be_valid
    end

    context 'as doctor' do
      it 'is valid without doctor_address_name' do
        form.doctor_address_name = nil
        expect(form).to be_valid
      end

      it 'is valid without selected requestor_consent' do
        form.requestor_consent = nil
        expect(form).to be_valid
      end
    end

    context 'as consultant' do
      let(:form) { CaseCreationForm.new(valid_params.merge(requestor_function: 'consultant')) }

      it 'is not valid without doctor_address_name' do
        form.doctor_address_name = nil
        expect(form).to_not be_valid
      end

      it 'is not valid without selected requestor_consent' do
        form.requestor_consent = nil
        expect(form).to_not be_valid
      end
    end

    it 'is not valid without clinic_name' do
      form.clinic_zip = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without clinic_zip' do
      form.clinic_zip = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without accepated requestor_confirmed' do
      form.requestor_confirmed = false
      expect(form).to_not be_valid
    end

    it 'is not valid without child_zip' do
      form.child_zip = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without child_age' do
      form.child_year_of_birth = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without child_gender' do
      form.child_gender = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without child_language' do
      form.child_language = nil
      expect(form).to_not be_valid
    end

    it 'is not valid without child_manifested_on' do
      form.child_manifested_on = nil
      expect(form).to_not be_valid
    end

    it do
      form.child_language = nil
      expect(form.save).to be_falsey
    end
  end

  context 'with valid params' do
    it 'should create case' do
      expect { form.save }.to change { Case.count }.by 1
    end

    it 'should create child' do
      expect { form.save }.to change { Child.count }.by 1
    end

    it 'should create clinic' do
      expect { form.save }.to change { Clinic.count }.by 1
    end

    it 'should create requstor' do
      expect { form.save }.to change { Requestor.count }.by 1
    end

    it 'should create doctor address' do
      expect { form.save }.to change { Address.with_addressable_scope(:doctor_address).count }.by 1
    end

    it 'should create address' do
      expect { form.save }.to change { Address.with_addressable_scope(:address).count }.by 1
    end

    it 'should create doctor and own addresseses' do
      expect { form.save }.to change { HelpServiceAssignment.count }.by 1
    end

    it 'should create locations' do
      expect { form.save }.to change { Location.count }.by 3
    end

    it { expect(form.save).to be_truthy }

    it { expect(form.prebuild_help_service_assignments.count).to eq 2 }
  end
end
