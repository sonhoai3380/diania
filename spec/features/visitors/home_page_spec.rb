# frozen_string_literal: true

# Feature: Home page
#   As a logged in user
#   I want to visit a home page
#   So I can learn more about the website
feature 'Home page' do
  # Scenario: Visit the home page
  #   Given I am a visitor
  #   When I visit the home page
  #   Then I see "Welcome"
  scenario 'visit the home page' do
    user = create(:user)
    signin(user.email, user.password)

    visit root_path
    expect(page).to have_content 'Willkommen'
  end
end
