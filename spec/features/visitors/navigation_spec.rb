# frozen_string_literal: true

feature 'Navigation links', :devise do
  scenario 'view navigation links as administrator' do
    user = create(:user, :admin)
    signin(user.email, user.password)

    visit root_path
    expect(page).to have_content I18n.t('layouts.navigation_links.dashboard')
    expect(page).to have_content I18n.t('layouts.navigation_links.nanny_search')
    expect(page).to have_content I18n.t('layouts.navigation_links.admin')
    expect(page).to have_content I18n.t('layouts.navigation.sign_out')
  end

  scenario 'view navigation links as nanny' do
    user = create(:user)
    signin(user.email, user.password)

    visit root_path
    expect(page).to have_content I18n.t('layouts.navigation_links.cases')
    expect(page).to have_content I18n.t('layouts.navigation_links.documentation')
    expect(page).to have_content I18n.t('layouts.navigation.sign_out')
  end
end
