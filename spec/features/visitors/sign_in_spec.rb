# frozen_string_literal: true

feature 'About sign in' do
  include Warden::Test::Helpers
  Warden.test_mode!
  # Scenario: Signout
  #   Given I am a visitor
  #   When Signout
  #   Then I am send to signin page
  scenario 'Visit the about page' do
    user = create(:user, sign_in_count: -1)
    signin(user.email, user.password)
    expect(current_path).to eq(root_path)
  end
end
