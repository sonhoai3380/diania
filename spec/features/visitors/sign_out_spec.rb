# frozen_string_literal: true

feature 'About signout' do
  include Warden::Test::Helpers
  Warden.test_mode!
  # Scenario: Signout
  #   Given I am a visitor
  #   When Signout
  #   Then I am send to signin page
  scenario 'Visit the about page' do
    user = create(:user)
    signin(user.email, user.password)
    visit root_path
    signout
    expect(current_path).to eq('/login')
  end
end
