# frozen_string_literal: true

# Feature: Case index page
#   As an admin or nanny
#   I want to visit the case's index page
#   So I can see all cases
feature 'Case index page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  after(:each) do
    Warden.test_reset!
  end

  let!(:the_case) { create(:case, :running, user: user) }
  let(:admin) { create(:user, :admin) }
  let(:user) { create(:user) }

  describe 'a requested case' do
    # Scenario: User is an admin
    #   Given I am signed in
    #   When I visit the case index page
    #   Then I can see a nanny column
    scenario 'user is an admin' do
      signin(admin.email, admin.password)
      visit cases_path

      expect(page).to have_selector('th a', text: 'Nanny')
      expect(page).to have_link('Anfragedetails', href: case_path(the_case))
    end

    # Scenario: User is a nanny
    #   Given I am signed in
    #   When I visit the case index page
    #   Then I cannot see a nanny column
    scenario 'user is a nanny' do
      signin(user.email, user.password)
      visit cases_path

      expect(page).not_to have_selector('th a', text: 'Nanny')
      expect(page).not_to have_link('Anfragedetails', href: case_path(the_case))
    end
  end

  describe 'a running case' do
    # Scenario: User is an admin
    #   Given I am signed in
    #   When I visit the case index page
    #   Then I can see a nanny column
    scenario 'user is an admin' do
      signin(admin.email, admin.password)
      visit cases_path

      expect(page).to have_selector('th a', text: 'Nanny')
      expect(page).to have_link('Anfragedetails', href: case_path(the_case))
    end

    # Scenario: User is a nanny
    #   Given I am signed in
    #   When I visit the case index page
    #   Then I cannot see a nanny column
    scenario 'user is a nanny' do
      signin(user.email, user.password)
      visit cases_path

      expect(page).not_to have_selector('th a', text: 'Nanny')
      expect(page).to have_link('Anfragedetails', href: edit_case_path(the_case))
    end
  end
end
