# frozen_string_literal: true

# Feature: Case State request page
#   As an nanny
#   I want to refuse the case
#   So the nanny can respond
feature 'Case state refuse page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:user) { create(:user) }

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User is an nanny
  #   Given I am signed in
  #   When I visit the case page and refuse case
  #   Then I am redirect to case path with success message
  scenario 'nanny refuses case' do
    the_case = create(:case, :requested, user: user)

    signin(user.email, user.password)
    visit case_path(the_case)
    click_link I18n.t('cases.show.refuse')
    expect(page).to have_content I18n.t('cases.states.refuse.success')
  end
end
