# frozen_string_literal: true

# Feature: Case State request page
#   As an admin
#   I want to complete the case
#   So the case is completed and paid
feature 'Case state complete page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:user) { create(:user, :admin) }

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User is an admin
  #   Given I am signed in
  #   When I visit the case page and finish case
  #   Then I am redirect to case path with success message
  scenario 'admin completes case' do
    the_case = create(:case, :finished, user: user)

    signin(user.email, user.password)
    visit case_path(the_case)
    click_link I18n.t('cases.show.complete_case')
    expect(page).to have_content I18n.t('cases.states.finish.success')

    the_case.reload
    expect(the_case.aasm_state).to_not eq(:finished)
    expect(the_case.finished_on).to eq(Date.parse('15 Jan 2019'))
  end

  # Scenario: User is an admin
  #   Given I am signed in
  #   When I visit the case page
  #   Then I cannot see the complete case link
  scenario 'admin completes case with error' do
    the_case = create(:case, user: user)

    signin(user.email, user.password)
    visit case_path(the_case)
    expect(page).to_not have_content I18n.t('cases.show.complete_case')
  end
end
