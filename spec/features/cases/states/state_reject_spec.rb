# frozen_string_literal: true

# Feature: Case State request page
#   As an nanny
#   I want to reject the case
#   So the nanny can respond
feature 'Case state reject page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:admin) { create(:user, :admin) }

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User is an nanny
  #   Given I am signed in
  #   When I visit the case page and reject case
  #   Then I am redirect to case path with success message
  scenario 'admin rejects case' do
    the_case = create(:case)

    signin(admin.email, admin.password)
    visit case_path(the_case)
    click_link I18n.t('cases.show.reject')
    expect(page).to have_content I18n.t('cases.states.reject.success')
  end
end
