# frozen_string_literal: true

# Feature: Case State request page
#   As an admin
#   I want to request the case to a nanny
#   So the nanny can respond
feature 'Case state request page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:user) { create(:user) }
  let(:admin) { create(:user, :admin) }

  before(:all) do
    create(:user)
  end

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User is an admin
  #   Given I am signed in
  #   When I visit the request case state page
  #   Then I am redirect to cases path with success message
  scenario 'admin requests nanny for case' do
    the_case = create(:case)

    signin(admin.email, admin.password)
    visit case_search_nannies_path(case_id: the_case.id)
    expect(page).to have_content I18n.t('cases.index.show_nanny')
    click_link 'Nanny anzeigen', match: :first
    click_link 'Anfragen'
    expect(page).to have_content I18n.t('cases.states.request_nanny.success')

    the_case.reload
    expect(the_case.user).to_not be_nil
  end

  # Scenario: User is an user
  #   Given I am signed in
  #   When I visit the request case state page
  #   Then I am redirect to cases path with error message
  scenario 'user requests nanny for case' do
    the_case = create(:case, :requested, user: nil)

    signin(user.email, user.password)
    visit case_search_nannies_path(case_id: the_case.id)
    expect(page).to have_content I18n.t('pundit.access_denied')

    the_case.reload
    expect(the_case.user).to be_nil
  end
end
