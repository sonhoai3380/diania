# frozen_string_literal: true

# Feature: Case State request page
#   As an admin
#   I want to discard the case
feature 'Case state discard page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:admin) { create(:user, :admin) }

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User is an admin
  #   Given I am signed in
  #   When I visit the case page and discard case
  #   Then I am redirect to case path with success message
  scenario 'admin discards case' do
    the_case = create(:case)

    signin(admin.email, admin.password)
    visit case_path(the_case)
    click_link I18n.t('cases.show.discard')
    expect(page).to have_content I18n.t('cases.states.discard.success')
  end
end
