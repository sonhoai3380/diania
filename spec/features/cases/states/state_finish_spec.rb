# frozen_string_literal: true

# Feature: Case State request page
#   As an nanny
#   I want to finish the case
#   So the nanny can respond
feature 'Case state finish page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:user) { create(:user) }

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User is an nanny
  #   Given I am signed in
  #   When I visit the case page and finish case
  #   Then I am redirect to case path with success message
  scenario 'nanny finishes case' do
    the_case = create(:case, :running, user: user)
    the_case.case_deployments << create(:case_deployment)

    signin(user.email, user.password)
    visit edit_case_path(the_case)
    click_button I18n.t('cases.edit_form.end_and_send_case')
    expect(page).to have_content I18n.t('cases.states.finish.success')

    the_case.reload
    expect(the_case.aasm_state).to_not eq(:finished)
  end

  # Scenario: User is an nanny
  #   Given I am signed in
  #   When I visit the case page and finish case
  #   Then I am redirect to case path with error message
  scenario 'nanny finishes case with error' do
    the_case = create(:case, user: user)

    signin(user.email, user.password)
    visit edit_case_path(the_case)
    click_button I18n.t('cases.edit_form.end_and_send_case')
    expect(page).to have_content I18n.t('cases.states.finish.error')

    the_case.reload
    expect(the_case.aasm_state).to_not eq(:running)
  end
end
