# frozen_string_literal: true

# Feature: Case detail page
#   As an admin or nanny
#   I want to visit a case's detail page
#   So I can see all case related infos
feature 'Case detail page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:user) { create(:user) }
  let(:admin) { create(:user, :admin) }

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User is an admin
  #   Given I am signed in
  #   When I visit a case's detail page
  #   Then I see the current status, can search for a nanny or reject the case
  scenario 'user is an admin' do
    the_case = create(:case, user: user)

    signin(admin.email, admin.password)
    visit case_path(the_case)

    expect(page).to have_content the_case.user_full_name
    expect(page).to have_content the_case.requestor_phone
    expect(page).to have_content the_case.clinic_zip
    expect(page).to have_content the_case.age_when_disease_manifested
    expect(page).to_not have_content the_case.initiated_by
    expect(page).to_not have_content the_case.user.bank_account_iban
    expect(page).to have_selector(:link_or_button, 'Nanny suchen')
    expect(page).to have_selector(:link_or_button, 'Fall ablehnen')
    expect(page).to have_selector(:link_or_button, 'Falscher Anfragender')
  end

  # Scenario: User is a nanny
  #   Given I am signed in
  #   When I visit a case's detail page
  #   Then I can accept or reject the case
  scenario 'user is a nanny' do
    the_case = create(:case, :requested, user: user)

    signin(user.email, user.password)
    visit case_path(the_case)

    expect(page).to have_content the_case.user_full_name
    expect(page).to have_content the_case.requestor_phone
    expect(page).to have_content the_case.clinic_zip
    expect(page).to have_content the_case.age_when_disease_manifested
    expect(page).to_not have_content the_case.initiated_by
    expect(page).to_not have_content the_case.user.bank_account_iban
    expect(page).to have_selector(:link_or_button, 'Fall annehmen')
    expect(page).to have_selector(:link_or_button, 'Fall ablehnen')
  end

  scenario 'user is an admin' do
    the_case = create(:case, :requested, user: user)

    signin(admin.email, admin.password)
    visit case_path(the_case)

    expect(page).to_not have_selector(:link_or_button, 'Fall annehmen')
    expect(page).to_not have_selector(:link_or_button, 'Fall ablehnen')
    expect(page).to have_content I18n.t('cases.show.nanny_being_searched')
  end

  # Scenario: User is a admin
  #   Given I am signed in
  #   When I visit a case's detail page
  #   Then I cannot see any buttons
  scenario 'user is a admin' do
    the_case = create(:case, :finished, user: user)

    signin(admin.email, admin.password)
    visit case_path(the_case)

    expect(page).to have_content the_case.user_full_name
    expect(page).to have_content the_case.requestor_phone
    expect(page).to have_content the_case.clinic_zip
    expect(page).to have_content the_case.age_when_disease_manifested
    expect(page).to have_content the_case.initiated_by.text
    expect(page).to have_content the_case.user.bank_account_iban
    expect(page).to have_selector(:link_or_button, 'Fall abschließen')
  end

  # Scenario: User is a nanny
  #   Given I am signed in
  #   When I visit a case's detail page
  #   Then I cannot see any buttons
  scenario 'user is a nanny' do
    the_case = create(:case, :finished, user: user)

    signin(user.email, user.password)
    visit case_path(the_case)

    expect(page).to have_content the_case.user_full_name
    expect(page).to have_content the_case.requestor_phone
    expect(page).to have_content the_case.clinic_zip
    expect(page).to have_content the_case.age_when_disease_manifested
    expect(page).to have_content the_case.user.bank_account_iban
    expect(page).to_not have_content the_case.initiated_by
    expect(page).to_not have_selector(:link_or_button, 'Fall abschließen')
  end
end
