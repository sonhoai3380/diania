# frozen_string_literal: true

feature 'Case detail page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  given(:admin) { create(:user, :admin) }
  given!(:care_age) { create(:care_age) }
  given!(:help_service) { create(:help_service) }

  after(:each) do
    Warden.test_reset!
  end

  context 'requestor is consultant' do
    background { fill_out_consultant_info }

    scenario 'shows thank you note' do
      txts = [I18n.t('cases.create.case_created'), I18n.t('cases.create.thank_you')]
      expect(page).to have_content(/.*#{txts[0]}.*|.*#{txts[1]}.*/)
    end

    context 'created case' do
      given(:the_case) { Case.last }

      scenario 'shows requestor full name' do
        expect(the_case.requestor.address_full_name).to eq('Max Mustermann')
      end

      scenario 'shows doctor full address' do
        expect(the_case.doctor_address_full_name).to eq('Dr. Arnold Arzt')
      end

      scenario 'shows comment' do
        expect(the_case.comments).to eq('Wunschnanny: Nina Nanny')
      end

      scenario 'shows child language' do
        expect(the_case.child_language.texts).to eq(['Deutsch'])
      end

      scenario 'shows other child language' do
        expect(the_case.child_language_other).to be_blank
      end

      context 'nanny view' do
        background do
          signin(admin.email, admin.password)
          visit case_path(the_case)
        end

        scenario 'shows child language' do
          expect(page).to have_content the_case.child_languages
        end
      end
    end
  end

  context 'requestor is doctor' do
    background { fill_out_doctor_info }

    scenario 'shows thank you note' do
      txts = [I18n.t('cases.create.case_created'), I18n.t('cases.create.thank_you')]
      expect(page).to have_content(/.*#{txts[0]}.*|.*#{txts[1]}.*/)
    end

    context 'created case' do
      given(:the_case) { Case.last }

      scenario 'shows requestor full name' do
        expect(the_case.requestor.address_full_name).to eq('Dr. Max Mustermann')
      end

      scenario 'shows doctor full address' do
        expect(the_case.doctor_address_full_name).to eq('Dr. Max Mustermann')
      end

      scenario 'shows comment' do
        expect(the_case.comments).to eq('Wunschnanny: Maria Mustermann')
      end

      scenario 'shows child language' do
        expect(the_case.child_language.texts).to eq(['Sonstige'])
      end

      scenario 'shows other child language' do
        expect(the_case.child_language_other).to eq('Rukiga')
      end

      context 'nanny view' do
        background do
          signin(admin.email, admin.password)
          visit case_path(the_case)
        end

        scenario 'shows child language' do
          expect(page).to have_content the_case.child_languages
        end
      end
    end
  end

  def fill_out_consultant_info
    fill_out_general_info
    fill_in 'Vorname', with: 'Max'
    select 'Diabetesberater', from: 'Funktion Anfragender (Arzt/Ärztin, Diabetesberater/in, Diabetesassistent/in)'
    fill_in 'Name des Arztes/der Ärztin', with: 'Dr. Arnold Arzt'
    check 'Einverständnis des Arztes/der Ärztin mit der Anfrage'
    choose 'case_german_speaking_true'
    fill_in 'Bemerkungen', with: 'Wunschnanny: Nina Nanny'
    click_button 'Abschicken'
  end

  def fill_out_doctor_info
    fill_out_general_info
    fill_in 'Vorname', with: 'Dr. Max'
    select 'Arzt/Ärztin', from: 'Funktion Anfragender (Arzt/Ärztin, Diabetesberater/in, Diabetesassistent/in)'
    choose 'case_german_speaking_false'
    find(:css, "#case_child_language_other[type='checkbox']").set(true)
    fill_in 'Andere Sprache', with: 'Rukiga'
    fill_in 'Bemerkungen', with: 'Wunschnanny: Maria Mustermann'
    click_button 'Abschicken'
  end

  def fill_out_general_info
    visit new_case_path
    fill_out_requestor_info
    fill_out_clinic_info
    fill_out_child_info
  end

  def fill_out_requestor_info
    fill_in 'Nachname', with: 'Mustermann'

    fill_in 'E-Mail', with: 'max.mustermann@example.com'
    fill_in 'Telefon', with: '040/1234567'

    check 'Ich bestätige Arzt/Ärztin, Diabetesberater/in oder Diabetesassistent/in zu sein'

    check 'Unterstützung nach der Diagnose, zum Einfinden in die neue Lebenssituation.'
    check 'Die Eltern/Vormund sind damit einverstanden, dass Dianino angefragt wird.'
  end

  def fill_out_clinic_info
    fill_in 'case_clinic_name', with: 'Die Klinik'
    fill_in 'case_clinic_zip', with: '20095'
  end

  def fill_out_child_info
    fill_in 'case_child_zip', with: '20095'
    fill_in 'Geburtsjahr', with: '2010'
    select 'Männlich', from: 'Geschlecht'
    fill_in 'Datum Erstmanifestation', with: '01.01.2019'
  end
end
