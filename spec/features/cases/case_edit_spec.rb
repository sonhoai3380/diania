# frozen_string_literal: true

# Feature: Case edit page
#   As an admin or nanny
#   I want to visit a case's edit page
#   So I can edit all case related infos
feature 'Case edit page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:user) { create(:user) }
  let(:admin) { create(:user, :admin) }

  after(:each) do
    Warden.test_reset!
  end

  context 'user is an nanny visiting a new case' do
    given(:the_case) { create(:case, user: user) }

    background do
      signin(user.email, user.password)
      visit edit_case_path(the_case)
    end

    scenario 'sees save button' do
      expect(page).to have_selector(:link_or_button, 'Speichern')
    end

    scenario 'sees language' do
      expect(the_case.child_languages).to eq 'Deutsch'
    end

    scenario 'does not see the chronic disease type' do
      expect(page).not_to have_selector(:css, '#case_child_diseases_chronic_illness_type')
    end

    scenario 'appear chronic disease type input if chronic_illness' do
      expect(page).not_to have_selector(:css, '#case_child_diseases_chronic_illness_type')
      check 'case_child_attributes_diseases_chronic_illness'
      expect(page).to have_selector('#case_child_attributes_diseases_chronic_illness_type')
    end

    scenario 'appear chronic disease type input if chronic_illness' do
      check 'case_child_attributes_diseases_chronic_illness'
      fill_in 'case_child_attributes_diseases_chronic_illness_type', with: 'Test'
      click_button 'Speichern'
      the_case.reload
      expect(page).to have_selector('#case_child_attributes_diseases_chronic_illness_type')
      expect(the_case.child.diseases['chronic_illness_type']).to eq 'Test'
    end

    scenario 'remove chronic disease type input if no chronic_illness' do
      check 'case_child_attributes_diseases_chronic_illness'
      fill_in 'case_child_attributes_diseases_chronic_illness_type', with: 'Test'
      uncheck 'case_child_attributes_diseases_chronic_illness'
      expect(page).to have_selector('#case_child_attributes_diseases_chronic_illness_type', visible: false)
      expect(find('#case_child_attributes_diseases_chronic_illness_type').text).to eq ''
    end

    scenario 'changes language' do
      find('#case_child_attributes_german_speaking_false').set(true)
      find("#case_child_attributes_language_other[type='checkbox']").set(true)
      find(:css, "#case_child_attributes_language_other[type='text']").set('Rukiga')
      click_button 'Speichern'

      the_case.reload
      # byebug
      expect(the_case.child_languages).to eq 'Rukiga'
    end
  end

  # Scenario: User is an admin
  #   Given I am signed in
  #   When I visit a case's edit page
  #   Then I see the form to make change
  scenario 'user is an admin visiting a new case' do
    the_case = create(:case, user: user)

    signin(admin.email, admin.password)
    visit edit_case_path(the_case)
    expect(page).to have_selector(:link_or_button, 'Speichern')
  end

  # Scenario: User is an nanny
  #   Given I am signed in
  #   When I visit a case's edit page
  #   Then I see the form to make change
  scenario 'user is a nanny visiting a locked case' do
    the_case = create(:case, :finished, user: user)

    signin(user.email, user.password)
    visit edit_case_path(the_case)
    expect(page).to have_content I18n.t('pundit.access_denied')
  end

  # Scenario: User is an admin
  #   Given I am signed in
  #   When I visit a finishedd case's edit page
  #   Then I see the form to make change
  scenario 'user is an admin visiting a finished case' do
    the_case = create(:case, :finished, user: user)

    signin(admin.email, admin.password)
    visit edit_case_path(the_case)
    expect(page).to have_selector(:link_or_button, 'Speichern')
  end

  # Scenario: User is an admin
  #   Given I am signed in
  #   When I visit a case's edit page
  #   Then I see the form to make change
  scenario 'user is an admin visiting a running case' do
    the_case = create(:case, :running, user: user)

    signin(admin.email, admin.password)
    visit edit_case_path(the_case)
    expect(page).to have_content I18n.t('pundit.access_denied')
  end

  # Scenario: User is an nanny
  #   Given I am signed in
  #   When I visit a case's edit page
  #   When child has diseases
  #   Then I see the form to make change
  scenario 'user is an nanny visiting a new case with diseases' do
    child    = create(:child, diseases: { mental_handicap: '0', chronic_illness: '1', chronic_illness_type: 'test' })
    the_case = create(:case, user: user, child: child)

    signin(user.email, user.password)
    visit edit_case_path(the_case)
    expect(page).to have_checked_field('case_child_attributes_diseases_chronic_illness')
    expect(page).to have_field('case_child_attributes_diseases_chronic_illness_type')
    expect(find_field('case_child_attributes_diseases_chronic_illness_type').value).to eq 'test'
  end

  # Scenario: User is an nanny
  #   Given I am signed in
  #   When I visit a case's edit page
  #   When child has no diseases
  #   Then I see the form to make change
  scenario 'user is an nanny visiting a new case without diseases' do
    the_case = create(:case, user: user)

    signin(user.email, user.password)
    visit edit_case_path(the_case)
    expect(page).not_to have_checked_field('case_child_attributes_diseases_chronic_illness')
    expect(page).to have_field('case_child_attributes_diseases_chronic_illness_type')
    expect(find_field('case_child_attributes_diseases_chronic_illness_type').value).to eq nil
  end
end
