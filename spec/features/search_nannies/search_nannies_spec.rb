# frozen_string_literal: true

feature 'Search nannies', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:admin) { create(:user, :admin) }
  let(:the_case) { create(:case, :requested, user: refusing_nanny) }
  let(:refusing_nanny) { create(:user, first_name: 'Günther', last_name: 'Gabriel') }
  let!(:other_nanny) { create(:user, first_name: 'John', last_name: 'Doe') }

  after(:each) do
    Warden.test_reset!
  end

  scenario 'admin searches another nanny after rejected' do
    the_case.refuse!

    signin(admin.email, admin.password)
    visit case_search_nannies_path(case_id: the_case.id)
    expect(page).to have_content I18n.t('cases.index.show_nanny')
    expect(page).to have_content other_nanny.address_first_name
    expect(page).to have_content other_nanny.address_last_name
    expect(page).to_not have_content refusing_nanny.address_first_name
    expect(page).to_not have_content refusing_nanny.address_last_name
  end
end
