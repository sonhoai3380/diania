# frozen_string_literal: true

# Feature: User index page
#   As a user
#   I want to see a list of users
#   So I can see who has registered
feature 'User index page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User listed on index page
  #   Given I am signed in
  #   When I visit the user index page
  #   Then I see my own email address
  scenario 'user sees welcome' do
    user = create(:user, :admin)
    login_as(user, scope: :user)
    visit root_path
    expect(page).to have_content 'Willkommen'
  end
end
