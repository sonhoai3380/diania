# frozen_string_literal: true

# Feature: User profile page
#   As a user
#   I want to visit my user profile page
#   So I can see my personal account data
feature 'User profile page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User sees own profile
  #   Given I am signed in
  #   When I visit the user profile page
  #   Then I see my own email address
  scenario 'user sees own profile' do
    user = create(:user)
    login_as(user, scope: :user)
    visit user_path(user)
    expect(page).to have_content 'E-Mail'
    expect(page).to have_content user.email
  end

  # Scenario: User cannot see another user's profile
  #   Given I am signed in
  #   When I visit another user's profile
  #   Then I see an 'access denied' message
  scenario "user cannot see another user's profile" do
    me = create(:user)
    other = create(:user, email: 'other@example.com')
    login_as(me, scope: :user)
    Capybara.current_session.driver.header 'Referer', root_path
    # TODO: Changing behaviour: Sometimes raising error, sometimes redirecting
    # 1. error: Pundit::NotAuthorizedError: not allowed to show?
    # 2. error:
    # expect { visit user_path(other) }.to raise_error(Pundit::NotAuthorizedError)
    visit user_path(other)
    expect(page).to have_content I18n.t('pundit.access_denied')
  end
end
