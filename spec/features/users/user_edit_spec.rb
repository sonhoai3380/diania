# frozen_string_literal: true

# Feature: User edit
#   As a user
#   I want to edit my user profile
#   So I can change my email address
feature 'User edit', :devise, :js do
  include Warden::Test::Helpers
  Warden.test_mode!

  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User changes email address
  #   Given I am signed in
  #   When I change my email address
  #   Then I see an account updated message
  scenario 'user changes email address' do
    user = create(:user)
    login_as(user, scope: :user)
    visit edit_user_path(user)
    fill_in 'E-Mail', with: 'newemail@example.com'
    click_button 'Speichern'
    expect(page).to have_content I18n.t('users.update.update_success')
  end

  # Scenario: User cannot edit another user's profile
  #   Given I am signed in
  #   When I try to edit another user's profile
  #   Then I see access denied
  scenario "user cannot edit another user's profile", :me do
    me = create(:user)
    other = create(:user, email: 'other@example.com')
    login_as(me, scope: :user)
    visit edit_user_path(other)
    expect(page).to have_content I18n.t('pundit.access_denied')
  end
end
