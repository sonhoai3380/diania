# frozen_string_literal: true

# Feature: User profile page
#   As a user
#   I want to visit my user profile page
#   So I can see my personal account data
feature 'Admin show user profile page', :devise do
  include UsersHelper
  include Warden::Test::Helpers
  Warden.test_mode!

  after(:each) do
    Warden.test_reset!
  end

  before(:each) do
    login_as(user, scope: :user)
  end

  let(:user) { create(:user, :admin, reset_password_sent_at: Time.current, confirmation_sent_at: Time.current) }

  let(:headers) { %w[Nachname Vorname PLZ Tel. mobil E-Mail Zusätzliche Sprache Rolle Anzeigen Bearbeiten Löschen] }

  scenario 'admin sees user profile' do
    visit admin_user_path(user)
    [user.address.salutation, user.full_name, user.address_street, user.address_zip, user.address_city,
     user.location_address, I18n.l(user.user_info&.birthday), user.user_info&.occupation, user.phone_private,
     user.phone_business, user.email_business, user.email, user.languages,
     user.role&.capitalize, user.bank_account_account_holder, user.bank_account_bic, user.bank_account_iban,
     reference_to_diabetes_text(user.reference_to_diabetes), user.language_other,
     I18n.l(user.reset_password_sent_at, format: :short),
     I18n.l(user.created_at, format: :short),
     I18n.l(user.updated_at, format: :short),
     I18n.l(user.confirmed_at, format: :short),
     I18n.l(user.confirmation_sent_at, format: :short)].each do |value|
      next if value.nil?

      expect(page).to have_content value
    end
    (user.care_ages | user.help_services).each do |u|
      expect(page).to have_content u.description
    end
  end
end
