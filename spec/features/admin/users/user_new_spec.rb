# frozen_string_literal: true

feature 'Admin::User new page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  given(:admin) { create(:user, :admin) }
  given!(:care_age) { create(:care_age) }
  given!(:help_service) { create(:help_service) }

  after(:each) do
    Warden.test_reset!
  end

  context 'create user not valid' do
    scenario 'create user' do
      user = create(:user, role: 'admin')
      login_as(user, scope: :user)
      visit new_admin_user_path
      click_button 'Abschicken'
      expect(page).to have_current_path(admin_users_path)
      expect(page).to have_content 'Anrede muss ausgefüllt werden'
    end
  end

  context 'create user' do
    scenario 'create user' do
      user = create(:user, role: 'admin')
      login_as(user, scope: :user)
      visit new_admin_user_path
      fill_in 'user_address_salutation', with: 'salutation'
      fill_in 'user_address_first_name', with: 'firstName'
      fill_in 'user_address_last_name', with: 'lastName'
      fill_in 'user_address_street', with: 'street'
      fill_in 'user_address_zip', with: '20095'
      fill_in 'user_user_email', with: 'email@example.com'
      fill_in 'user_user_language_other', with: 'other'
      fill_in 'user_user_info_birthday', with: 'birthday'
      fill_in 'user_user_info_occupation', with: 'test'
      fill_in 'user_address_phone_private', with: '0111111110'
      fill_in 'user_address_phone_mobile', with: '0111111110'
      fill_in 'user_address_phone_business', with: '0111111110'
      fill_in 'user_address_email_business', with: 'email@example.com'
      fill_in 'user_bank_account_account_holder', with: 'test'
      fill_in 'user_bank_account_iban', with: 'DE89 3704 0044 0532 0130 00'
      fill_in 'user_bank_account_bic', with: 'AABSDE31'
      find(:css, '#user_user_info_reference_to_diabetes_works_in_clinic').set(true)
      find(:css, '#user_help_service_assignments_attributes_0_assignee_id').set(true)
      find(:css, '#user_care_ages_users_attributes_0_user_id').set(true)
      find('#user_user_role').find(:xpath, 'option[1]').select_option
      find('#user_user_language').find(:xpath, 'option[1]').select_option
      click_button 'Abschicken'
      expect(page).to have_current_path(admin_users_path)
      expect(page).to have_content 'Benutzer wurde erfolgreich erstellt'
    end
  end
end
