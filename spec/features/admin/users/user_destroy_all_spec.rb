# frozen_string_literal: true

feature 'Admin::User index page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:user) { create(:user, :admin) }
  let(:user2) { create(:user) }
  let(:user3) { create(:user) }

  before(:each) do
    login_as(user, scope: :user)
  end

  after(:each) do
    Warden.test_reset!
  end

  scenario 'admin delete multiple selected records' do
    visit admin_users_path
    within 'table.table.wice-grid' do
      all("input[name='users[selected][]']").each { |box| box.set(true) }
    end
    expect { click_button 'Ausgewählte Benutzer löschen' }.to change { User.count }.by(-1)
  end
end
