# frozen_string_literal: true

# Feature: User edit
#   As a user
#   I want to edit my user profile
#   So I can change my email address
feature 'Admin::User edit', :devise, :js do
  include Warden::Test::Helpers
  Warden.test_mode!

  after(:each) do
    Warden.test_reset!
  end

  scenario 'admin changes email address' do
    user = create(:user, role: 'admin')
    login_as(user, scope: :user)
    visit admin_users_path
    click_link 'Bearbeiten'
    expect(page).to have_current_path(edit_admin_user_path(user))
    fill_in 'user_email', with: 'newemail@example.com'
    click_button 'Abschicken'
    expect(page).to have_current_path(edit_admin_user_path(user))
    expect(page).to have_content I18n.t('users.update.update_success')
    user.reload
    expect(user.email).to eq 'newemail@example.com'
  end

  scenario 'admin cannot update' do
    user = create(:user, role: 'admin')
    allow_any_instance_of(User).to receive(:update).and_return(false)
    login_as(user, scope: :user)
    visit edit_admin_user_path(user)
    click_button 'Abschicken'
    expect(page).to have_current_path(edit_admin_user_path(user))
    expect(page).to have_content I18n.t('users.update.update_error')
  end
end
