# frozen_string_literal: true

# Feature: Admin delete
#   As an admin
#   I want to delete a user profile
feature 'Admin::User delete', :devise, :js do
  include Warden::Test::Helpers
  Warden.test_mode!

  # after(:each) do
  #   Warden.test_reset!
  # end

  # Scenario: Admin can delete other account
  #   Given I am signed in as admin
  #   When I delete my account
  #   Then I should have one less user
  # TODO: Temporarily disabled because of a 'Net::ReadTimeout' in CodeShip
  # scenario 'admin can delete other accounts' do
  #   user = create(:user, role: 'admin')
  #   login_as(user, scope: :user)
  #   visit '/admin/users'
  #   expect { click_link 'Löschen' }.to change { User.count }.by(-1)
  # end

  # scenario 'admin cannot delete other accounts' do
  #   user = create(:user, role: 'admin')
  #   login_as(user, scope: :user)
  #   allow(User).to receive(:find).and_return(user)
  #   allow(user).to receive(:destroy).and_return(false)
  #   visit '/admin/users'
  #   expect { click_link 'Löschen' }.to change { User.count }.by(0)
  # end
end
