# frozen_string_literal: true

# Feature: User index page
#   As a user
#   I want to see a list of users
#   So I can see who has registered
feature 'Admin::User index page', :devise do
  include Warden::Test::Helpers
  Warden.test_mode!

  let(:user) { create(:user, :admin) }
  let(:headers) { %w[Nachname Vorname PLZ Tel. mobil E-Mail Sprache Rolle Anzeigen Bearbeiten Löschen] }
  let(:columns) do
    [user.address_first_name, user.address_last_name, user.address_zip,
     user.address&.phone_mobile, user.email, user.languages, user.role&.capitalize]
  end

  before(:each) do
    login_as(user, scope: :user)
  end

  after(:each) do
    Warden.test_reset!
  end

  scenario 'admin sees welcome page after login' do
    visit root_path
    expect(page).to have_content 'Willkommen'
  end

  scenario 'admin send to index page after clicking Benutzerverwaltung' do
    visit root_path
    click_link 'Benutzerverwaltung'
    expect(page).to have_current_path(admin_users_path)
  end

  scenario 'admin sees index table at index page' do
    visit admin_users_path

    ['Neuer Benutzer', 'Ausgewählte Benutzer löschen', 'Benutzerverwaltung'].each { |v| expect(page).to have_content v }

    expect(page).to have_css 'table.table.wice-grid'

    within 'table.table.wice-grid' do
      (headers | columns).each do |value|
        next if value.nil?

        expect(page).to have_content value
      end
    end
  end
end
