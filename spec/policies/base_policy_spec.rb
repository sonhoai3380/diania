# frozen_string_literal: true

require 'rails_helper'

describe BasePolicy do
  subject { described_class }

  let(:admin) { build :user, :admin }

  def resolve_for(user)
    subject::Scope.new(user, Case).resolve
  end

  permissions '.scope' do
    it 'resolves scope' do
      create :case
      expect(resolve_for(admin).count).to eq(1)
    end
  end
end
