# frozen_string_literal: true

require 'rails_helper'

describe CasePolicy do
  subject { described_class }

  let(:current_user) { build :user }
  let(:the_case) { build :case, user: current_user }
  let(:finished_case) { build :case, :finished, user: current_user }
  let(:running_case) { build :case, :running, user: current_user }
  let(:other_user) { build :user }
  let(:admin) { build :user, :admin }

  def resolve_for(user)
    subject::Scope.new(user, Case).resolve
  end

  permissions :index? do
    it 'allows access for an user' do
      expect(subject).to permit(current_user, the_case)
    end
    it 'allows access for an admin' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions :show? do
    it 'prevents other users from seeing your case' do
      expect(subject).not_to permit(other_user, the_case)
    end
    it 'allows you to see your own case' do
      expect(subject).to permit(current_user, the_case)
    end
    it 'allows an admin to see any case' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions :edit? do
    it 'prevents edits if not own case' do
      expect(subject).not_to permit(other_user, the_case)
    end
    it 'allows user to edit your own case' do
      expect(subject).to permit(current_user, the_case)
    end
    it 'allows an admin to edit a new case' do
      expect(subject).to permit(admin, the_case)
    end
    it 'prevents user to edit your own finished case' do
      expect(subject).not_to permit(current_user, finished_case)
    end
    it 'allows an admin to edit your own finished case' do
      expect(subject).to permit(admin, finished_case)
    end
    it 'prevents an admin to edit your own running case' do
      expect(subject).not_to permit(admin, running_case)
    end
  end

  permissions :update? do
    it 'prevents updates if not own case' do
      expect(subject).not_to permit(other_user, the_case)
    end
    it 'allows user to update your own case' do
      expect(subject).to permit(current_user, the_case)
    end
    it 'allows an admin to update a case' do
      expect(subject).to permit(admin, the_case)
    end
    it 'prevents user to update your own finished case' do
      expect(subject).not_to permit(current_user, finished_case)
    end
    it 'prevents an admin to update your own finished case' do
      expect(subject).not_to permit(admin, finished_case)
    end
  end

  permissions :destroy? do
    it 'prevents deleting if not an admin' do
      expect(subject).not_to permit(current_user, the_case)
    end
    it 'allows an admin to delete any case' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions :reject? do
    it 'prevents rejecting if not an admin' do
      expect(subject).not_to permit(current_user, the_case)
    end
    it 'allows an admin to reject any case' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions :discard? do
    it 'prevents discarding if not an admin' do
      expect(subject).not_to permit(current_user, the_case)
    end
    it 'allows an admin to discard any case' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions :request_nanny? do
    it 'prevents requesting a nanny if not an admin' do
      expect(subject).not_to permit(current_user, the_case)
    end
    it 'allows an admin to requesting a nanny for any case' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions :accept? do
    it 'prevents accepting if not an admin' do
      expect(subject).to_not permit(other_user, the_case)
    end
    it 'allows an requested user to accept any case' do
      expect(subject).to permit(current_user, the_case)
    end
    it 'prevents an admin to accept any case' do
      expect(subject).to_not permit(admin, the_case)
    end
  end

  permissions :refuse? do
    it 'prevents refusing if not an admin' do
      expect(subject).to_not permit(other_user, the_case)
    end
    it 'allows an requested user to refuse any case' do
      expect(subject).to permit(current_user, the_case)
    end
    it 'prevents an admin to refuse any case' do
      expect(subject).to_not permit(admin, the_case)
    end
  end

  permissions :finish? do
    it 'prevents finishing if not an admin' do
      expect(subject).to_not permit(other_user, the_case)
    end
    it 'allows an requested user to finish any case' do
      expect(subject).to permit(current_user, the_case)
    end
    it 'prevents an admin to finish any case' do
      expect(subject).to_not permit(admin, the_case)
    end
  end

  permissions :complete? do
    it 'prevents completing if not an admin' do
      expect(subject).to_not permit(other_user, the_case)
    end
    it 'prevents an requested user to complete any case' do
      expect(subject).to_not permit(current_user, the_case)
    end
    it 'allows an admin to complete any case' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions :reset? do
    it 'prevents reseting if not an admin' do
      expect(subject).to_not permit(other_user, the_case)
    end
    it 'prevents an requested user to reset any case' do
      expect(subject).to_not permit(current_user, the_case)
    end
    it 'allows an admin to complete any case' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions :pdf? do
    it 'prevents downloading the pdf if not an admin' do
      expect(subject).to_not permit(other_user, the_case)
    end
    it 'allows an downloading the pdf to for case user' do
      expect(subject).to permit(current_user, the_case)
    end
    it 'allows an admin to download the pdf of any case' do
      expect(subject).to permit(admin, the_case)
    end
  end

  permissions '.scope' do
    it 'allows admins to see all cases' do
      create :case
      create :case
      expect(resolve_for(admin).count).to eq(2)
    end
    it 'allows the user to see his cases' do
      create :case
      create :case, :requested, user: current_user
      expect(resolve_for(current_user).count).to eq(1)
    end
  end
end
