# frozen_string_literal: true

require 'rails_helper'

describe SearchNannyPolicy do
  subject { described_class }

  let(:current_user) { build :user }
  let(:the_case) { build :case, user: current_user }
  let(:other_user) { build :user }
  let(:admin) { build :user, :admin }

  permissions :index? do
    it 'prevents other uses seeing nannies for case' do
      expect(subject).not_to permit(current_user, the_case)
    end
    it 'allows access for an admin' do
      expect(subject).to permit(admin, the_case)
    end
  end
end
