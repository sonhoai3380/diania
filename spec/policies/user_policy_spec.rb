# frozen_string_literal: true

describe UserPolicy do
  subject { described_class }

  let(:current_user) { build :user }
  let(:other_user) { build :user }
  let(:admin) { build :user, :admin }

  permissions :index? do
    it 'denies access if not an admin' do
      expect(subject).not_to permit(current_user)
    end
    it 'allows access for an admin' do
      expect(subject).to permit(admin)
    end
  end

  permissions :show? do
    it 'prevents other users from seeing your profile' do
      expect(subject).not_to permit(current_user, other_user)
    end
    it 'allows you to see your own profile' do
      expect(subject).to permit(current_user, current_user)
    end
    it 'allows an admin to see any profile' do
      expect(subject).to permit(admin)
    end
  end

  permissions :update? do
    it 'prevents updates if not an admin' do
      expect(subject).not_to permit(current_user)
    end
    it 'allows an admin to make updates' do
      expect(subject).to permit(admin)
    end
  end

  permissions :create? do
    it 'prevents creates if not an admin' do
      expect(subject).not_to permit(current_user)
    end
    it 'allows an admin to make creates' do
      expect(subject).to permit(admin)
    end
  end

  permissions :new? do
    it 'prevents new if not an admin' do
      expect(subject).not_to permit(current_user)
    end
    it 'allows an admin to make new' do
      expect(subject).to permit(admin)
    end
  end

  permissions :destroy? do
    it 'prevents deleting yourself' do
      expect(subject).to permit(current_user, current_user)
    end
    it 'allows an admin to delete any user' do
      expect(subject).to permit(admin, other_user)
    end
  end

  permissions :destroy_all? do
    it 'prevents deleting yourself' do
      expect(subject).not_to permit(current_user)
    end
    it 'allows an admin to delete any user' do
      expect(subject).to permit(admin)
    end
  end

  permissions :resend_confirmation_instructions? do
    it 'prevents deleting yourself' do
      expect(subject).not_to permit(current_user)
    end
    it 'allows an admin to delete any user' do
      expect(subject).to permit(admin)
    end
  end
end
