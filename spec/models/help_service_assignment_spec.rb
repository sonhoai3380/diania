# frozen_string_literal: true

# == Schema Information
#
# Table name: help_service_assignments
#
#  id              :bigint(8)        not null, primary key
#  assignee_type   :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  assignee_id     :bigint(8)
#  help_service_id :bigint(8)
#
# Indexes
#
#  index_help_service_assignments_on_assignee_type_and_assignee_id  (assignee_type,assignee_id)
#  index_help_service_assignments_on_help_service_id                (help_service_id)
#
# Foreign Keys
#
#  fk_rails_...  (help_service_id => help_services.id)
#

require 'rails_helper'

RSpec.describe HelpServiceAssignment, type: :model do
  let(:assignment) { create(:help_service_assignment) }

  it 'has a valid factory' do
    expect(assignment).to be_valid
  end

  it 'is not valid without a help service' do
    assignment.help_service = nil
    expect(assignment).to_not be_valid
  end
end
