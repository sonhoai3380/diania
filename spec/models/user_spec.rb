# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  confirmation_sent_at   :datetime
#  confirmation_token     :string
#  confirmed_at           :datetime
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  language               :string
#  language_other         :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  name                   :string
#  nda                    :boolean
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :string
#  sign_in_count          :integer          default(0), not null
#  unconfirmed_email      :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

describe User do
  let(:email) { 'user@example.com' }
  let(:user) { create(:user, email: email) }

  it 'responds to email' do
    expect(user).to respond_to(:email)
  end

  it '#email returns a string' do
    expect(user.email).to match email
  end

  it 'has a valid factory' do
    expect(user).to be_valid
  end

  it 'is valid without nda accepted' do
    user.nda = nil
    expect(user).to be_valid
  end

  context 'scopes' do
    describe '.nearby_nannies_by_coordinates' do
      let(:address1) { build(:address, zip: '20095') }
      let(:address2) { build(:address, zip: 'another zip') }

      let(:user1) { create(:user, address: address1) }
      let(:user2) { create(:user, address: address2) }
      let(:nearby_nannies) { described_class.nearby_nannies_by_coordinates(coordinates) }

      context 'when coordinates present' do
        let(:coordinates) { [53.5510010700068, 9.9996662352794] }

        it 'should return nearby nannies by coordinates' do
          expect(nearby_nannies).to include(user1)
          expect(nearby_nannies).not_to include(user2)
        end
      end

      context 'when coordinates empty' do
        let(:coordinates) { [] }

        it 'should return nearby nannies by coordinates' do
          expect(nearby_nannies).not_to include(user1)
          expect(nearby_nannies).not_to include(user2)
        end
      end
    end
  end
end
