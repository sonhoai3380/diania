# frozen_string_literal: true

# == Schema Information
#
# Table name: addresses
#
#  id                :bigint(8)        not null, primary key
#  addressable_scope :string
#  addressable_type  :string
#  city              :string
#  email_business    :string
#  first_name        :string
#  last_name         :string
#  phone_business    :string
#  phone_mobile      :string
#  phone_private     :string
#  salutation        :string
#  state             :string
#  street            :string
#  zip               :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  addressable_id    :bigint(8)
#
# Indexes
#
#  index_addresses_on_addressable_type_and_addressable_id  (addressable_type,addressable_id)
#

require 'rails_helper'

RSpec.describe Address, type: :model do
  let(:address) { create(:address, :for_user, first_name: 'Max', last_name: 'Mustermann') }

  it 'has a valid factory' do
    expect(address).to be_valid
  end

  it 'has valid coordinates' do
    expect(address.to_coordinates).to eq([53.5510010700068, 9.9996662352794])
  end

  it 'updates coordinates on save' do
    address.update(zip: '20457')
    expect(address.to_coordinates).to eq([53.5435696985051, 9.99301361584148])
  end

  context 'on create' do
    let(:address) { build(:address, :for_user) }

    it 'is valid without a first name' do
      address.first_name = nil
      expect(address).to be_valid
    end

    it 'is valid without a last name' do
      address.last_name = nil
      expect(address).to be_valid
    end

    it 'is valid without a street' do
      address.street = nil
      expect(address).to be_valid
    end

    it 'is valid without a zip' do
      address.zip = nil
      expect(address).to be_valid
    end

    it 'is valid without a city' do
      address.city = nil
      expect(address).to be_valid
    end

    it 'is valid without a state' do
      address.state = nil
      expect(address).to be_valid
    end

    it 'is valid without a email_business' do
      address.email_business = nil
      expect(address).to be_valid
    end

    it 'is valid without a phone_business' do
      address.phone_business = nil
      expect(address).to be_valid
    end
  end

  context 'on update' do
    it 'is not valid with an invalid email address' do
      address.email_business = 'max%mustermann.de'
      expect(address).to_not be_valid
    end

    it 'is valid with any phone number' do
      address.phone_business = '0171-12345678:'
      expect(address).to be_valid
    end
  end

  it 'returns the full name' do
    expect(address.full_name).to eq('Max Mustermann')
  end
end
