# frozen_string_literal: true

# == Schema Information
#
# Table name: requestors
#
#  id         :bigint(8)        not null, primary key
#  confirmed  :boolean
#  consent    :boolean
#  email      :string
#  function   :string
#  phone      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  case_id    :bigint(8)
#
# Indexes
#
#  index_requestors_on_case_id  (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (case_id => cases.id)
#

require 'rails_helper'

RSpec.describe Requestor, type: :model do
  let(:requestor) { create(:requestor, function: requestor_function) }

  context 'as a doctor' do
    let(:requestor_function) { :doctor }

    it 'has a valid factory' do
      expect(requestor).to be_valid
    end

    it 'is not valid without a function' do
      requestor.function = nil
      expect(requestor).to_not be_valid
    end

    it 'is valid without a consent' do
      requestor.consent = nil
      expect(requestor).to be_valid
    end

    it 'is valid without a consent' do
      requestor.consent = nil
      expect(requestor).to be_valid
    end

    it 'is not valid without an email' do
      requestor.email = nil
      expect(requestor).to_not be_valid
    end

    it 'is not valid without an associated case' do
      requestor.case = nil
      expect(requestor).to_not be_valid
    end

    it 'is not valid with an invalid email' do
      requestor.email = 'bla&blub.de'
      expect(requestor).to_not be_valid
    end
  end

  context 'as a consultant' do
    let(:requestor_function) { :consultant }

    it 'has a valid factory' do
      expect(requestor).to be_valid
    end

    it 'is not valid without a consent' do
      requestor.consent = nil
      expect(requestor).to_not be_valid
    end

    it 'is not valid without a consent' do
      requestor.consent = nil
      expect(requestor).to_not be_valid
    end
  end

  context 'as an assistant' do
    let(:requestor_function) { :assistant }

    it 'has a valid factory' do
      expect(requestor).to be_valid
    end

    it 'is not valid without a consent' do
      requestor.consent = nil
      expect(requestor).to_not be_valid
    end

    it 'is not valid without a consent' do
      requestor.consent = nil
      expect(requestor).to_not be_valid
    end
  end
end
