# frozen_string_literal: true

# == Schema Information
#
# Table name: refusals
#
#  id         :bigint(8)        not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  case_id    :bigint(8)
#  user_id    :bigint(8)
#
# Indexes
#
#  index_refusals_on_case_id  (case_id)
#  index_refusals_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe Refusal, type: :model do
  it { is_expected.to belong_to(:case) }
  it { is_expected.to belong_to(:user) }
end
