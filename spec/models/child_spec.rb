# frozen_string_literal: true

# == Schema Information
#
# Table name: children
#
#  id                   :bigint(8)        not null, primary key
#  celiac_disease       :boolean
#  chronic_illness_type :string
#  diseases             :json
#  environment          :string
#  gender               :string
#  language             :string
#  language_other       :string
#  manifested_on        :date
#  nationality          :string
#  nationality_other    :string
#  operational_env      :string
#  operational_reason   :string
#  phone                :string
#  refugee              :boolean
#  state                :string
#  therapy              :string
#  year_of_birth        :integer
#  zip                  :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  care_age_id          :bigint(8)
#  case_id              :bigint(8)
#
# Indexes
#
#  index_children_on_care_age_id  (care_age_id)
#  index_children_on_case_id      (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (care_age_id => care_ages.id)
#  fk_rails_...  (case_id => cases.id)
#

require 'rails_helper'

RSpec.describe Child, type: :model do
  let(:child) { create(:child, year_of_birth: 2010) }

  it 'has a valid factory' do
    expect(child).to be_valid
  end

  context 'on create' do
    let(:child) { build(:child) }

    it 'is not valid without a gender' do
      child.gender = nil
      expect(child).to_not be_valid
    end

    it 'is not valid without a manifestation date' do
      child.manifested_on = nil
      expect(child).to_not be_valid
    end

    it 'is not valid without a zip' do
      child.zip = nil
      expect(child).to_not be_valid
    end

    it 'is not valid without a language' do
      child.language = nil
      expect(child).to_not be_valid
    end

    it 'is not valid without an associated case' do
      child.case = nil
      expect(child).not_to be_valid
    end

    it 'is not valid without a year of birth' do
      child.year_of_birth = nil
      expect(child).not_to be_valid
    end

    it 'is valid without an associated care age' do
      child.care_age = nil
      expect(child).to be_valid
    end

    it 'is valid without environment infos' do
      child.environment = nil
      expect(child).to be_valid
    end

    it 'is valid without therapy infos' do
      child.therapy = nil
      expect(child).to be_valid
    end

    it 'is valid without a nationality' do
      child.nationality = nil
      expect(child).to be_valid
    end

    it 'is valid without a refugee flag' do
      child.refugee = nil
      expect(child).to be_valid
    end

    it 'is valid without a state' do
      child.state = nil
      expect(child).to be_valid
    end
  end

  context 'on update' do
    context 'when case has finished state' do
      let(:the_case) { create(:case, :finished) }
      let(:child) { build(:child, case: the_case) }

      it 'is not valid without a year of birth' do
        child.year_of_birth = nil
        expect(child).to_not be_valid
      end

      it 'is not valid without environment infos' do
        child.environment = nil
        expect(child).to_not be_valid
      end

      it 'is not valid without therapy infos' do
        child.therapy = nil
        expect(child).to_not be_valid
      end

      it 'is not valid without a nationality' do
        child.nationality = nil
        expect(child).to_not be_valid
      end

      it 'is not valid without a refugee flag' do
        child.refugee = nil
        expect(child).to_not be_valid
      end
    end
  end

  it 'returns the user\'s age' do
    now = Time.now.utc.to_date
    expect(child.age).to eq(now.year - 2010)
  end

  describe '#age_when_disease_manifested' do
    it 'should return nil when year_of_birth is missed' do
      child.year_of_birth = nil
      expect(child.age_when_disease_manifested.to_i).to eq 0
    end

    it 'should return age when disease was manifested' do
      child.manifested_on = '2013-01-01'
      expect(child.age_when_disease_manifested.to_i).to eq 3
    end
  end
end
