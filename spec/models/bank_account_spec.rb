# frozen_string_literal: true

# == Schema Information
#
# Table name: bank_accounts
#
#  id             :bigint(8)        not null, primary key
#  account_holder :string
#  bank_name      :string
#  bic            :string
#  iban           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  user_id        :bigint(8)
#
# Indexes
#
#  index_bank_accounts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe BankAccount, type: :model do
  let(:user) { create(:user) }
  let(:account) { create(:bank_account, user: user) }

  it 'has a valid factory' do
    expect(account).to be_valid
  end

  it 'is valid without an iban' do
    account.iban = nil
    expect(account).to be_valid
  end

  it 'is not valid without an associated user ' do
    account.user = nil
    expect(account).to_not be_valid
  end
end
