# frozen_string_literal: true

# == Schema Information
#
# Table name: case_deployments
#
#  id          :bigint(8)        not null, primary key
#  comment     :text
#  distance    :integer
#  duration    :integer
#  finished_on :datetime
#  started_on  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  case_id     :bigint(8)
#
# Indexes
#
#  index_case_deployments_on_case_id  (case_id)
#
# Foreign Keys
#
#  fk_rails_...  (case_id => cases.id)
#

require 'rails_helper'

RSpec.describe CaseDeployment, type: :model do
  let(:deployment) { create(:case_deployment) }

  it 'has a valid factory' do
    expect(deployment).to be_valid
  end

  it 'is not valid without a distance' do
    deployment.distance = nil
    expect(deployment).to_not be_valid
  end

  it 'is not valid without a duration' do
    deployment.duration = nil
    expect(deployment).to_not be_valid
  end

  it 'is not valid without a start date' do
    deployment.started_on = nil
    expect(deployment).to_not be_valid
  end

  it 'is valid without a end date' do
    deployment.finished_on = nil
    expect(deployment).to be_valid
  end

  it 'is not valid without an associated case' do
    deployment.case = nil
    expect(deployment).to_not be_valid
  end
end
