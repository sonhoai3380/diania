# frozen_string_literal: true

# == Schema Information
#
# Table name: user_infos
#
#  id                    :bigint(8)        not null, primary key
#  birthday              :date
#  occupation            :string
#  reference_to_diabetes :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  user_id               :bigint(8)
#
# Indexes
#
#  index_user_infos_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe UserInfo, type: :model do
  let(:info) { create(:user_info) }

  it 'has a valid factory' do
    expect(info).to be_valid
  end

  it 'is not valid without an associated user' do
    info.user = nil
    expect(info).to_not be_valid
  end
end
