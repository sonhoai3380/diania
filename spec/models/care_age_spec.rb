# frozen_string_literal: true

# == Schema Information
#
# Table name: care_ages
#
#  id          :bigint(8)        not null, primary key
#  description :string
#  key         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe CareAge, type: :model do
  let(:age) { create(:care_age) }

  it 'has a valid factory' do
    expect(age).to be_valid
  end

  it 'is not valid without a key' do
    age.key = nil
    expect(age).to_not be_valid
  end

  it 'is not valid without a description' do
    age.description = nil
    expect(age).to_not be_valid
  end
end
