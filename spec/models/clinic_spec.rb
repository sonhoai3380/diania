# frozen_string_literal: true

# == Schema Information
#
# Table name: clinics
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  zip        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Clinic, type: :model do
  let(:clinic) { create(:clinic) }

  it 'has a valid factory' do
    expect(clinic).to be_valid
  end

  context 'on create' do
    let(:clinic) { build(:clinic) }

    it 'is not valid without a zip' do
      clinic.zip = nil
      expect(clinic).to_not be_valid
    end

    it 'valid without a name' do
      clinic.name = nil
      expect(clinic).to_not be_valid
    end
  end
end
