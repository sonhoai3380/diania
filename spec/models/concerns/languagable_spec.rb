# frozen_string_literal: true

require 'rails_helper'

describe Languagable do
  let(:child) { create(:child) }

  describe '.language' do
    it 'should return Enumerize::Set class' do
      expect(child.language.class).to eq Enumerize::Set
    end
  end

  describe '.languages' do
    it 'should a string of all languages' do
      expect(child.languages).to eq 'Deutsch'
    end
  end
end
