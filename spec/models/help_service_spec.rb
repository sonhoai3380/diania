# frozen_string_literal: true

# == Schema Information
#
# Table name: help_services
#
#  id          :bigint(8)        not null, primary key
#  description :string
#  key         :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe HelpService, type: :model do
  let(:service) { create(:help_service) }

  it 'has a valid factory' do
    expect(service).to be_valid
  end

  it 'is not valid without a key' do
    service.key = nil
    expect(service).to_not be_valid
  end

  it 'is not valid without a description' do
    service.description = nil
    expect(service).to_not be_valid
  end
end
