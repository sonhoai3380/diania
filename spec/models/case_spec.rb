# frozen_string_literal: true

# == Schema Information
#
# Table name: cases
#
#  id                  :bigint(8)        not null, primary key
#  aasm_state          :string
#  comments            :text
#  comments_nanny      :text
#  commuting_allowance :decimal(10, 2)   default(0.0)
#  finished_on         :date
#  flyer_handed        :boolean
#  identifier          :string
#  initiated_by        :string
#  per_diem_allowance  :decimal(10, 2)   default(0.0)
#  started_on          :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  clinic_id           :bigint(8)
#  user_id             :bigint(8)
#
# Indexes
#
#  index_cases_on_clinic_id  (clinic_id)
#  index_cases_on_user_id    (user_id)
#

require 'rails_helper'

RSpec.describe Case, type: :model do
  let(:the_case) { create(:case) }
  let(:requested_case) { create(:case, :requested) }
  let(:refused_case) { create(:case, :refused) }
  let(:running_case) { create(:case, :running) }
  let(:finished_case) { create(:case, :finished) }
  let(:date) { Time.zone.today }

  it 'has a valid factory' do
    expect(the_case).to be_valid
  end

  it 'has a valid identifier' do
    expect(the_case.identifier).to eq "#{date.year}-0001"
  end

  it 'increases the identifier for a second case' do
    the_case
    second_case = create(:case)
    expect(second_case.identifier).to eq "#{date.year}-0002"
  end

  it 'resets identifier count in new year' do
    the_case
    Timecop.freeze(Time.zone.today + 1.year) do
      second_case = create(:case)
      expect(second_case.identifier).to eq "#{date.year}-0001"
    end
  end

  it 'is not valid without an associated child' do
    the_case.child = nil
    expect(the_case).to_not be_valid
  end

  it 'is not valid without an associated requestor' do
    the_case.requestor = nil
    expect(the_case).to_not be_valid
  end

  it 'is valid without finished_on' do
    the_case.finished_on = nil
    expect(the_case).to be_valid
    expect(the_case.finished_on).to be_nil
  end

  it 'has no allowances set' do
    expect(the_case.commuting_allowance).to eq 0.0
    expect(the_case.per_diem_allowance).to eq 0.0
  end

  context 'case with finished aasm state' do
    it 'is not valid without started_on' do
      finished_case.started_on = nil
      expect(finished_case).to_not be_valid
    end

    it 'is not valid without flyer_handed' do
      finished_case.flyer_handed = nil
      expect(finished_case).to_not be_valid
    end

    it 'is not valid without initiated_by' do
      finished_case.initiated_by = nil
      expect(finished_case).to_not be_valid
    end

    it 'is not valid without initiated_by' do
      finished_case.finished_on = nil
      expect(finished_case).to be_valid
      expect(finished_case.finished_on).to eq(Date.parse('15 Jan 2019'))
    end
  end

  context 'assigns addresses correctly' do
    let(:requestor_address) { build(:address, first_name: 'Riko', last_name: 'Requestor') }
    let(:doctor_address) { build(:address, :with_doctor, first_name: 'Dirk', last_name: 'Doktor') }
    let(:requestor) { build(:requestor, address: requestor_address, doctor_address: doctor_address) }
    let(:the_case) { create(:case, requestor: requestor) }

    it { expect(the_case.doctor_address_full_name).to eq(requestor.doctor_address_full_name) }
    it { expect(the_case.requestor.address_full_name).to eq(requestor.address_full_name) }
  end

  describe '#child_location_coordinates' do
    it 'returns child location coordiantes' do
      expect(the_case.child_location_coordinates).to eq [53.5510010700068, 9.9996662352794]
    end
  end

  context 'initial state' do
    subject { the_case }

    let(:nanny) { create(:user) }

    it { is_expected.to have_state(:new) }

    it { is_expected.to transition_from(:new).to(:discarded).on_event(:discard, 'Ein Grund') }
    it { is_expected.to transition_from(:new).to(:rejected).on_event(:reject) }
    it { is_expected.to transition_from(:new).to(:requested).on_event(:request, nanny) }

    it { is_expected.to allow_event :discard }
    it { is_expected.to allow_event :reject }
    it { is_expected.to allow_event :request }
    it { is_expected.to_not allow_event :run }
    it { is_expected.to_not allow_event :finish }
    it { is_expected.to_not allow_event :complete }
    it { is_expected.to_not allow_event :reset }

    describe 'gets discarded' do
      before { the_case.discard(discard: { message: 'Ein Grund' }) }
      subject { the_case }

      it { is_expected.to have_state(:discarded) }
      it { expect(ActionMailer::Base.deliveries.last.to).to eq([the_case.requestor.email]) }
      it { expect(ActionMailer::Base.deliveries.last.from).to eq([ENV['EMAIL_FROM']]) }
      it { expect(ActionMailer::Base.deliveries.last.subject).to eq(I18n.t('case_mailer.discarded.subject')) }
    end

    describe 'gets rejected' do
      before { the_case.reject }
      subject { the_case }

      it { is_expected.to have_state(:rejected) }
      it { expect(ActionMailer::Base.deliveries.last.to).to eq([the_case.requestor.email]) }
      it { expect(ActionMailer::Base.deliveries.last.from).to eq([ENV['EMAIL_FROM']]) }
      it { expect(ActionMailer::Base.deliveries.last.subject).to eq(I18n.t('case_mailer.rejected.subject')) }
    end

    describe 'gets requested' do
      before { the_case.request(nanny) }
      subject { the_case }

      it { is_expected.to have_state(:requested) }
      it { expect(ActionMailer::Base.deliveries.last.to).to eq([nanny.email]) }
      it { expect(ActionMailer::Base.deliveries.last.from).to eq([ENV['EMAIL_FROM']]) }
      it { expect(ActionMailer::Base.deliveries.last.subject).to eq(I18n.t('case_mailer.requested.subject')) }
    end
  end

  context 'requested state' do
    subject { requested_case }

    it { is_expected.to have_state(:requested) }

    it { is_expected.to transition_from(:requested).to(:running).on_event(:run) }
    it { is_expected.to transition_from(:requested).to(:refused).on_event(:refuse) }

    it { is_expected.to allow_event :run }
    it { is_expected.to allow_event :refuse }
    it { is_expected.to_not allow_event :discard }
    it { is_expected.to_not allow_event :finish }
    it { is_expected.to_not allow_event :complete }
    it { is_expected.to_not allow_event :reset }

    describe 'gets accepted' do
      before { requested_case.run }
      subject { requested_case }

      it { is_expected.to have_state(:running) }
      it { expect(ActionMailer::Base.deliveries.last.to).to eq([requested_case.requestor.email]) }
      it { expect(ActionMailer::Base.deliveries.last.from).to eq([ENV['EMAIL_FROM']]) }
      it { expect(ActionMailer::Base.deliveries.last.subject).to eq(I18n.t('case_mailer.accepted.subject')) }
    end

    describe 'gets refused' do
      before { requested_case.refuse }
      subject { requested_case }

      it { is_expected.to have_state(:refused) }
      it { expect(ActionMailer::Base.deliveries.last.to).to eq(ApplicationMailer::DEFAULT_TO.split(',')) }
      it { expect(ActionMailer::Base.deliveries.last.from).to eq([ENV['EMAIL_FROM']]) }
      it {
        expect(ActionMailer::Base.deliveries.last.subject).to eq(I18n.t('case_mailer.nanny_rejected.subject',
                                                                        case_id: requested_case.identifier))
      }
    end
  end

  context 'refused state' do
    subject { refused_case }

    let(:nanny) { create(:user) }

    it { is_expected.to have_state(:refused) }

    it { is_expected.to transition_from(:refused).to(:requested).on_event(:request, nanny) }

    it { is_expected.to allow_event :request }
    it { is_expected.to_not allow_event :discard }
    it { is_expected.to_not allow_event :finish }
    it { is_expected.to_not allow_event :complete }
    it { is_expected.to_not allow_event :reset }

    describe 'gets requested' do
      before { refused_case.request(nanny) }
      subject { refused_case }

      it { is_expected.to have_state(:requested) }
      it { expect(ActionMailer::Base.deliveries.last.to).to eq([nanny.email]) }
      it { expect(ActionMailer::Base.deliveries.last.from).to eq([ENV['EMAIL_FROM']]) }
      it { expect(ActionMailer::Base.deliveries.last.subject).to eq(I18n.t('case_mailer.requested.subject')) }
    end
  end

  context 'running state' do
    subject { running_case }

    it { is_expected.to have_state(:running) }

    it { is_expected.to transition_from(:running).to(:finished).on_event(:finish) }
    it { is_expected.to transition_from(:running).to(:new).on_event(:reset) }

    it { is_expected.to allow_event :finish }
    it { is_expected.to allow_event :reset }
    it { is_expected.to_not allow_event :discard }
    it { is_expected.to_not allow_event :request }
    it { is_expected.to_not allow_event :complete }

    describe 'gets finished' do
      before { running_case.finish }
      subject { running_case }

      it { is_expected.to have_state(:finished) }
      it { expect(ActionMailer::Base.deliveries.last.to).to eq(ENV['DEFAULT_TO'].split(',')) }
      it { expect(ActionMailer::Base.deliveries.last.from).to eq([ENV['EMAIL_FROM']]) }
      it {
        expect(ActionMailer::Base.deliveries.last.subject).to eq(I18n.t('case_mailer.finished.subject',
                                                                        case_id: running_case.identifier))
      }
    end

    describe 'gets reset' do
      before { running_case.reset }
      subject { running_case }

      it { is_expected.to have_state(:new) }
      it { expect(subject.user).to be_nil }
    end
  end

  context 'finished state' do
    subject { finished_case }

    it { is_expected.to have_state(:finished) }

    it { is_expected.to transition_from(:finished).to(:completed).on_event(:complete) }

    it { is_expected.to allow_event :complete }
    it { is_expected.to_not allow_event :discard }
    it { is_expected.to_not allow_event :request }
    it { is_expected.to_not allow_event :finish }
    it { is_expected.to_not allow_event :reset }

    describe 'gets completed' do
      before { finished_case.complete }
      subject { finished_case }

      it { is_expected.to have_state(:completed) }
      it { expect(ActionMailer::Base.deliveries.last.to).to eq([finished_case.requestor.email]) }
      it { expect(ActionMailer::Base.deliveries.last.from).to eq([ENV['EMAIL_FROM']]) }
      it { expect(ActionMailer::Base.deliveries.last.subject).to eq(I18n.t('case_mailer.completed_requestor.subject')) }
      it { expect(finished_case.finish_case).to eq(Time.zone.parse('2019-01-15 09:27:09 CET')) }

      it 'has allowances set' do
        expect(finished_case.commuting_allowance).to eq(Rails.application.secrets.commuting_allowance)
        expect(finished_case.per_diem_allowance).to eq(Rails.application.secrets.per_diem_allowance)
      end
    end
  end

  context 'notifications for a new case' do
    subject { the_case }

    let(:nanny) { create(:user) }

    it { expect { subject.notify_case_discarded }.to change { ActionMailer::Base.deliveries.count }.by(2) }
    it { expect { subject.notify_case_rejected }.to change { ActionMailer::Base.deliveries.count }.by(2) }
    it { expect { subject.notify_case_accepted(nanny) }.to change { ActionMailer::Base.deliveries.count }.by(2) }
  end

  context 'notifications for a requested case' do
    subject { requested_case }

    it { expect { subject.notify_case_running }.to change { ActionMailer::Base.deliveries.count }.by(3) }
    it { expect { subject.notify_case_refused }.to change { ActionMailer::Base.deliveries.count }.by(2) }
  end

  context 'notifications for a running case' do
    subject { running_case }

    it { expect { subject.notify_case_finished }.to change { ActionMailer::Base.deliveries.count }.by(2) }
  end

  context 'notifications for a finished case' do
    subject { finished_case }

    it { expect { subject.notify_case_completed }.to change { ActionMailer::Base.deliveries.count }.by(3) }
  end
end
