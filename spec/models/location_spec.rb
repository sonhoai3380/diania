# frozen_string_literal: true

# == Schema Information
#
# Table name: locations
#
#  id                :bigint(8)        not null, primary key
#  latitude          :float
#  locationable_type :string
#  longitude         :float
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  locationable_id   :bigint(8)
#
# Indexes
#
#  index_locations_on_locationable_type_and_locationable_id  (locationable_type,locationable_id)
#

require 'rails_helper'

RSpec.describe Location, type: :model do
  let(:location) { build(:location) }

  it 'has a valid factory' do
    allow(location.locationable).to receive(:update_column).and_return(true)

    expect(location).to be_valid
  end

  it 'is not valid without locationable object' do
    location.locationable = nil
    expect(location).to_not be_valid
  end
end
