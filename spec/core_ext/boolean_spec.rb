# frozen_string_literal: true

require 'rails_helper'

describe TrueClass, type: :core_ext do
  describe '#humanize' do
    it 'should return localized yes value' do
      expect(true.humanize).to eq(I18n.t('core_ext.boolean.yes_title'))
    end
  end
end

describe FalseClass, type: :core_ext do
  describe '#humanize' do
    it 'should return localized no value' do
      expect(false.humanize).to eq(I18n.t('core_ext.boolean.no_title'))
    end
  end
end

describe NilClass, type: :core_ext do
  describe '#humanize' do
    it 'should return localized no value' do
      expect(nil.humanize).to eq(I18n.t('core_ext.boolean.no_title'))
    end
  end

  describe '#to_boolean' do
    context 'when nil value' do
      it { expect(nil.to_boolean).to be_falsey }
    end
  end
end
