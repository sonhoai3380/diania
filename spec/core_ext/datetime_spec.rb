# frozen_string_literal: true

require 'rails_helper'

describe DateTime, type: :core_ext do
  describe '#date_month_year' do
    before { Timecop.freeze('2019-01-23 00:00') }
    after { Timecop.return }

    it 'should return date in format DD.MM.YYYY' do
      expect(DateTime.current.date_month_year).to eq('23.01.2019')
    end
  end
end

describe Date, type: :core_ext do
  describe '#date_month_year' do
    before { Timecop.freeze('2019-01-23') }
    after { Timecop.return }

    it 'should return date in format DD.MM.YYYY' do
      expect(Time.zone.today.date_month_year).to eq('23.01.2019')
    end
  end
end

describe Time, type: :core_ext do
  describe '#date_month_year' do
    before { Timecop.freeze('2019-01-23 00:00') }
    after { Timecop.return }

    it 'should return date in format DD.MM.YYYY' do
      expect(Time.current.date_month_year).to eq('23.01.2019')
    end
  end
end
