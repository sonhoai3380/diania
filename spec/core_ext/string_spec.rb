# frozen_string_literal: true

require 'rails_helper'

describe String, type: :core_ext do
  describe '#to_boolean' do
    context 'when TRUE values' do
      it { expect('1'.to_boolean).to be_truthy }
    end

    context 'when FALSE values' do
      it { expect('0'.to_boolean).to be_falsey }
    end
  end
end
