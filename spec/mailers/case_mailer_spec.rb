# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CaseMailer, type: :mailer do
  describe 'new' do
    let(:the_case) { create(:case) }
    let(:mail) { CaseMailer.with(case: the_case).created }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.created.subject'))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Fallanfrage')
    end
  end

  describe 'requestor_new' do
    let(:the_case) { create(:case) }
    let(:mail) { CaseMailer.with(case: the_case, email: 'to@example.com').requestor_created }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.requestor_created.subject'))
      expect(mail.to).to eq(['to@example.com'])
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Fallanfrage')
    end
  end

  describe 'discarded' do
    let(:the_case) { create(:case, :discarded) }
    let(:mail) { CaseMailer.with(case: the_case, email: ApplicationMailer::DEFAULT_TO, message: 'Ein Grund').discarded }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.discarded.subject'))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Website oder Fotodokument')
      expect(mail.body.encoded).to include('Fotodokument')
    end
  end

  describe 'rejected' do
    let(:the_case) { create(:case, :rejected) }
    let(:mail) { CaseMailer.with(case: the_case, email: ApplicationMailer::DEFAULT_TO).rejected }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.rejected.subject'))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.html_part.body.decoded).to include('unsere Hilfsmöglichkeiten abgedeckt')
    end
  end

  describe 'accepted' do
    let(:the_case) { create(:case, :running) }
    let(:mail) { CaseMailer.with(case: the_case, email: ApplicationMailer::DEFAULT_TO).accepted }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.accepted.subject'))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Wir suchen eine passende')
    end
  end

  describe 'requested' do
    let(:the_case) { create(:case, :requested) }
    let(:mail) { CaseMailer.with(case: the_case, email: ApplicationMailer::DEFAULT_TO).requested }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.requested.subject'))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Wenn ja freuen wir uns, wenn du den Fall')
      expect(mail.body.encoded).to include('neue Hilfsanfrage bei der')
    end
  end

  describe 'nanny_accepted' do
    let(:the_case) { create(:case, :running) }
    let(:mail) { CaseMailer.with(case: the_case).nanny_accepted }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.nanny_accepted.subject', case_id: the_case.identifier))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('angenommen')
    end
  end

  describe 'nanny_rejected' do
    let(:the_case) { create(:case, :refused) }
    let(:mail) { CaseMailer.with(case: the_case).nanny_rejected }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.nanny_rejected.subject', case_id: the_case.identifier))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('abgelehnt')
    end
  end

  describe 'running' do
    let(:the_case) { create(:case, :running) }
    let(:mail) { CaseMailer.with(case: the_case, email: ApplicationMailer::DEFAULT_TO).running }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.running.subject'))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Verbindung setzen und alle weiteren Schritte besprechen')
    end
  end

  describe 'finished without param' do
    let(:the_case) { create(:case, :finished) }
    let(:mail) { CaseMailer.with(case: the_case).finished }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.finished.subject', case_id: the_case.identifier))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('beendet')
    end
  end

  describe 'finished with param' do
    let(:the_case) { create(:case, :finished) }
    let(:mail) { CaseMailer.with(case: the_case, email: 'to@example.com').finished }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.finished.subject', case_id: the_case.identifier))
      expect(mail.to).to eq(['to@example.com'])
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('beendet')
    end
  end

  describe 'completed_nanny' do
    let(:the_case) { create(:case, :completed) }
    let(:mail) { CaseMailer.with(case: the_case, email: ApplicationMailer::DEFAULT_TO).completed_nanny }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.completed_nanny.subject', case_id: the_case.identifier))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('Tagen auf das Konto mit der IBAN')
      expect(mail.body.encoded).to include(the_case.user_full_name)
      expect(mail.body.encoded).to include('abgeschlossen')
    end

    it 'has an pdf attachment' do
      expect(mail.attachments.count).to eq(1)
      expect(mail.attachments.first).to be_a_kind_of(Mail::Part)
      expect(mail.attachments.first.filename).to eq("fall_#{the_case.identifier}.pdf")
    end
  end

  describe 'completed_requestor' do
    let(:the_case) { create(:case, :completed) }
    let(:mail) { CaseMailer.with(case: the_case, email: ApplicationMailer::DEFAULT_TO).completed_requestor }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('case_mailer.completed_requestor.subject'))
      expect(mail.to).to eq(ApplicationMailer::DEFAULT_TO.split(','))
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to include('abgeschlossen')
    end
  end
end
