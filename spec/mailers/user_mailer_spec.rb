# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  describe 'welcome_nanny' do
    let(:user) { create(:user) }
    let(:mail) { UserMailer.with(user: user).welcome_nanny }

    it 'renders the headers' do
      expect(mail.subject).to eq(I18n.t('user_mailer.welcome_nanny.subject'))
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq([ENV['EMAIL_FROM']])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match("Sehr geehrte/r #{user.full_name},")
      expect(mail.body.encoded).to match('reset_password_token')
    end
  end
end
