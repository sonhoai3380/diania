# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/case_mailer
class CaseMailerPreview < ActionMailer::Preview
  def created
    the_case = Case.last
    CaseMailer.with(case: the_case).created
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/discarded
  def discarded
    the_case = Case.last
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').discarded
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/rejected
  def rejected
    the_case = Case.last
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').rejected
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/accepted
  def accepted
    the_case = Case.last
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').accepted
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/requested
  def requested
    the_case = Case.last
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').requested
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/nanny_accepted
  def nanny_accepted
    the_case = Case.last
    CaseMailer.with(case: the_case).nanny_accepted
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/nanny_rejected
  def nanny_rejected
    the_case = Case.last
    CaseMailer.with(case: the_case).nanny_rejected
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/running
  def running
    the_case = Case.last
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').running
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/finished
  def finished
    the_case = Case.last
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').finished
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/completed_nanny
  def completed_nanny
    the_case = Case.where(aasm_state: :finished).first
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').completed_nanny
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/completed_requestor
  def completed_requestor
    the_case = Case.where(aasm_state: :finished).first
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').completed_requestor
  end

  # Preview this email at http://localhost:3000/rails/mailers/case_mailer/requestor_created
  def requestor_created
    the_case = Case.last
    CaseMailer.with(case: the_case, email: 'dev@hochhaus-digital.de').requestor_created
  end
end
