# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/welcome_nanny
  def welcome_nanny
    user = User.last
    UserMailer.with(user: user).welcome_nanny
  end
end
