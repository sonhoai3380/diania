# frozen_string_literal: true

module Features
  module SessionHelpers
    def signin(email, password)
      visit new_user_session_path
      fill_in 'E-Mail', with: email
      fill_in 'Passwort', with: password
      click_button 'Anmelden'
    end

    def signout
      visit root_path
      click_link 'Abmelden'
    end
  end
end
