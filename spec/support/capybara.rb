# frozen_string_literal: true

require 'selenium-webdriver'
require 'capybara/rspec'
require 'webdrivers'

Capybara.asset_host = 'http://localhost:3000'

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups
end

options = {}
options[:args] = ['headless', 'disable-gpu', 'window-size=1280,1024']
Capybara.register_driver :headless_chrome do |app|
  Capybara::Selenium::Driver.new(
    app,
    browser: :chrome,
    options: Selenium::WebDriver::Chrome::Options.new(options)
  )
end

Capybara.javascript_driver = :headless_chrome
