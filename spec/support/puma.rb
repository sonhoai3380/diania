# frozen_string_literal: true

# Silence puma logging in rspec progress bar
Capybara.server = :puma, { Silent: true }
