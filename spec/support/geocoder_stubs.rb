# frozen_string_literal: true

addresses = {
  '20095, Deutschland' => {
    'latitude' => 53.5510010700068,
    'longitude' => 9.9996662352794,
    'address' => 'Hamburg',
    'city' => 'Hamburg',
    'state' => 'Hamburg',
    'state_code' => 'HH',
    'country' => 'Germany',
    'country_code' => 'DE'
  },
  '20457, Deutschland' => {
    'latitude' => 53.5435696985051,
    'longitude' => 9.99301361584148,
    'address' => 'Hamburg',
    'city' => 'Hamburg',
    'state' => 'Hamburg',
    'state_code' => 'HH',
    'country' => 'Germany',
    'country_code' => 'DE'
  },
  'Deutschland' => {
    'latitude' => 51.0834196,
    'longitude' => 10.4234469,
    'address' => 'Berlin',
    'city' => 'Berlin',
    'state' => 'Berlin',
    'state_code' => 'Be',
    'country' => 'Germany',
    'country_code' => 'DE'
  },
  'another zip, Deutschland' => {},
  [53.5510010700068, 9.9996662352794] => {
    'address' => 'Hamburg',
    'city' => 'Hamburg',
    'state' => 'Hamburg',
    'state_code' => 'HH',
    'country' => 'Germany',
    'country_code' => 'DE'
  },
  [51.0834196, 10.4234469] => {
    'address' => 'Hamburg',
    'city' => 'Hamburg',
    'state' => 'Hamburg',
    'state_code' => 'HH',
    'country' => 'Germany',
    'country_code' => 'DE'
  },
  [53.5435696985051, 9.99301361584148] => {
    'address' => 'Hamburg',
    'city' => 'Hamburg',
    'state' => 'Hamburg',
    'state_code' => 'HH',
    'country' => 'Germany',
    'country_code' => 'DE'
  }
}

Geocoder.configure(
  lookup: :test,
  ip_lookup: :test,
  language: :de,
  units: :km,
  distances: :linear
)

addresses.each { |lookup, results| Geocoder::Lookup::Test.add_stub(lookup, [results]) }
