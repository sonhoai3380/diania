Dianino
================

This application was generated with the [rails_apps_composer](https://github.com/RailsApps/rails_apps_composer) gem
provided by the [RailsApps Project](http://railsapps.github.io/).

Rails Composer is supported by developers who purchase our RailsApps tutorials.

Problems? Issues?
-----------

Need help? Ask on Stack Overflow with the tag 'railsapps.'

Your application contains diagnostics in the README file. Please provide a copy of the README file when reporting any issues.

If the application doesn't work as expected, please [report an issue](https://github.com/RailsApps/rails_apps_composer/issues)
and include the diagnostics.

Ruby on Rails
-------------

This application requires:

- Ruby 2.5.3
- Rails 5.2.2
- Redis server
- Node 11.9
- Yarn 1.9.4

Learn more about [Installing Rails](http://railsapps.github.io/installing-rails.html).

Getting Started
---------------

1.  `git flow init`
2.  `overcommit init`
3.  `yarn`
4.  `bundle`
5.  `rake db:setup`
6.  `foreman start`

Documentation and Support
-------------------------

Issues
-------------

none

Code Analysis
-------------

Overcommit runs pre-commit callbacks, e.g.:

1. `rubocop`
2. `rails_best_practices`

Deployment to Heroku
------------

1. `heroku create`
2. `heroku buildpacks:set https://github.com/bundler/heroku-buildpack-bundler2`
3. `git push heroku master`

Credits
-------

License
-------
