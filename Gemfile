# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }
ruby '2.5.3'

gem 'aasm', '~> 5.0.1'
gem 'active_model_serializers', '~> 0.10.2'
gem 'binding_of_caller', '~> 0.8.0'
gem 'bootsnap', '~> 1.3.2', require: false
gem 'bootstrap', '~> 4.0.0'
gem 'bootstrap-datepicker-rails', '~> 1.8.0.1'
gem 'cocoon', '~> 1.2', '>= 1.2.12'
gem 'coffee-rails', '~> 4.2.2'
gem 'devise', '~> 4.5.0'
gem 'devise-i18n', '~> 1.7.1'
gem 'enumerize', '~> 2.1', '>= 2.1.2'
gem 'geocoder', '~> 1.5.1'
gem 'high_voltage', '~> 3.1.0'
gem 'jbuilder', '~> 2.8.0'
gem 'jquery-rails', '~> 4.3.3'
gem 'pg', '~> 1.1.4'
gem 'phonelib', '~> 0.6.28'
gem 'prawn', '~> 2.2.2'
gem 'puma', '~> 3.12.0'
gem 'pundit', '~> 2.0.0'
gem 'rails', '~> 5.2.2'
gem 'redis', '~> 4.1.0'
gem 'redis-namespace', '~> 1.6.0'
gem 'redis-rack-cache', '~> 2.0.2'
gem 'redis-rails', '~> 5.0.2'
gem 'responders', '~> 2.4.0'
gem 'resque', '~> 2.0.0'
gem 'resque_mailer', '~> 2.4.3'
gem 'rollbar', '~> 2.22.1'
gem 'sass-rails', '~> 5.0.7'
gem 'simple_form', '~> 4.1.0'
gem 'slim-rails', '~> 3.2.0'
gem 'turbolinks', '~> 5.2.0'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'uglifier', '>= 4.1.20'
gem 'validates_timeliness', '~> 4.0', '>= 4.0.2'
gem 'wice_grid', '~> 4.1.0'

group :development, :test do
  gem 'annotate', '~> 2.7.4'
  gem 'byebug', '~> 10.0.2', platforms: %i[mri mingw x64_mingw]
  gem 'dotenv-rails', '~> 2.6.0'
  gem 'factory_bot_rails', '~> 4.11.1'
  gem 'faker', '~> 1.9.1'
  gem 'rspec-rails', '~> 3.8.1'
  gem 'rubocop', '~> 0.62.0'
  gem 'slim_lint', '~> 0.16.1'
end

group :development do
  gem 'better_errors', '~> 2.5.0'
  gem 'brakeman', '~> 4.4.0', require: false
  gem 'bundler-audit', '~> 0.6.1'
  gem 'capistrano', '~> 3.0.1'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano-rails', '~> 1.1.0'
  gem 'capistrano-rails-console', '~> 0.5.2'
  gem 'capistrano-rvm', '~> 0.1.2'
  gem 'fasterer', '~> 0.4.1'
  gem 'foreman', '~> 0.85.0'
  gem 'guard-bundler', '~> 2.2.0'
  gem 'guard-rails', '~> 0.8.1'
  gem 'guard-rspec', '~> 4.7.3'
  gem 'letter_opener', '~> 1.7.0'
  gem 'listen', '~> 3.1.5'
  gem 'overcommit', '~> 0.47.0'
  gem 'rails_best_practices', '~> 1.19.4'
  gem 'rails_layout', '~> 1.0.42'
  gem 'rb-fchange', '~> 0.0.6', require: false
  gem 'rb-fsevent', '~> 0.10.3', require: false
  gem 'rb-inotify', '~> 0.10.0', require: false
  gem 'spring', '~> 2.0.2'
  gem 'spring-commands-rspec', '~> 1.0.4'
  gem 'spring-watcher-listen', '~> 2.0.1'
  gem 'web-console', '~> 3.7.0'
end

group :test do
  gem 'capybara', '~> 3.31.0'
  gem 'codecov', '~> 0.1.14', require: false
  gem 'database_cleaner', '~> 1.7.0'
  gem 'launchy', '~> 2.4.3'
  gem 'pdf-inspector', '~> 1.3.0', require: 'pdf/inspector'
  gem 'rails-controller-testing', '~> 1.0.4'
  gem 'selenium-webdriver', '~> 3.142.3'
  gem 'shoulda-matchers', '~> 4.1.2'
  gem 'simplecov', '~> 0.16.1', require: false
  gem 'timecop', '~> 0.9.1'
  gem 'webdrivers'
end
