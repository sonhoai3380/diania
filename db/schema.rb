# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_01_15_102924) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "salutation"
    t.string "first_name"
    t.string "last_name"
    t.string "street"
    t.string "zip"
    t.string "city"
    t.string "state"
    t.string "email_business"
    t.string "phone_business"
    t.string "phone_private"
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "addressable_scope"
    t.string "phone_mobile"
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string "iban"
    t.string "bic"
    t.string "bank_name"
    t.string "account_holder"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_bank_accounts_on_user_id"
  end

  create_table "care_ages", force: :cascade do |t|
    t.string "key"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "care_ages_users", id: false, force: :cascade do |t|
    t.bigint "care_age_id", null: false
    t.bigint "user_id", null: false
    t.index ["user_id", "care_age_id"], name: "index_care_ages_users_on_user_id_and_care_age_id"
  end

  create_table "case_deployments", force: :cascade do |t|
    t.integer "distance"
    t.integer "duration"
    t.datetime "started_on"
    t.datetime "finished_on"
    t.text "comment"
    t.bigint "case_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["case_id"], name: "index_case_deployments_on_case_id"
  end

  create_table "cases", force: :cascade do |t|
    t.string "aasm_state"
    t.date "started_on"
    t.boolean "flyer_handed"
    t.string "initiated_by"
    t.bigint "clinic_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "finished_on"
    t.text "comments"
    t.string "identifier"
    t.decimal "commuting_allowance", precision: 10, scale: 2, default: "0.0"
    t.decimal "per_diem_allowance", precision: 10, scale: 2, default: "0.0"
    t.text "comments_nanny"
    t.index ["clinic_id"], name: "index_cases_on_clinic_id"
    t.index ["user_id"], name: "index_cases_on_user_id"
  end

  create_table "children", force: :cascade do |t|
    t.string "gender"
    t.integer "year_of_birth"
    t.string "phone"
    t.date "manifested_on"
    t.string "zip"
    t.string "environment"
    t.string "therapy"
    t.string "nationality"
    t.boolean "refugee"
    t.json "diseases"
    t.string "operational_env"
    t.string "operational_reason"
    t.bigint "case_id"
    t.bigint "care_age_id"
    t.string "language"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "celiac_disease"
    t.string "language_other"
    t.string "state"
    t.string "nationality_other"
    t.index ["care_age_id"], name: "index_children_on_care_age_id"
    t.index ["case_id"], name: "index_children_on_case_id"
  end

  create_table "clinics", force: :cascade do |t|
    t.string "name"
    t.string "zip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "help_service_assignments", force: :cascade do |t|
    t.bigint "help_service_id"
    t.string "assignee_type"
    t.bigint "assignee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assignee_type", "assignee_id"], name: "index_help_service_assignments_on_assignee_type_and_assignee_id"
    t.index ["help_service_id"], name: "index_help_service_assignments_on_help_service_id"
  end

  create_table "help_services", force: :cascade do |t|
    t.string "key"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string "locationable_type"
    t.bigint "locationable_id"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["locationable_type", "locationable_id"], name: "index_locations_on_locationable_type_and_locationable_id"
  end

  create_table "refusals", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "case_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["case_id"], name: "index_refusals_on_case_id"
    t.index ["user_id"], name: "index_refusals_on_user_id"
  end

  create_table "requestors", force: :cascade do |t|
    t.string "function"
    t.boolean "consent"
    t.boolean "confirmed"
    t.string "email"
    t.string "phone"
    t.bigint "case_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["case_id"], name: "index_requestors_on_case_id"
  end

  create_table "user_infos", force: :cascade do |t|
    t.string "occupation"
    t.date "birthday"
    t.string "reference_to_diabetes"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_infos_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.boolean "nda"
    t.string "language"
    t.string "role"
    t.string "language_other"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "bank_accounts", "users"
  add_foreign_key "case_deployments", "cases"
  add_foreign_key "children", "care_ages"
  add_foreign_key "children", "cases"
  add_foreign_key "help_service_assignments", "help_services"
  add_foreign_key "requestors", "cases"
  add_foreign_key "user_infos", "users"
end
