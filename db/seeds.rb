# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = CreateAdminService.new.call
Rails.logger.info "CREATED ADMIN USER: #{user.email}"

CreateHelpServicesService.call
Rails.logger.info 'CREATED HELP SERVICES'

care_ages_data = [['nanny', 'Nanny (0 - 12 Jahre)'], ['coach', 'Coach (13 - 18 Jahre)']]
care_ages_data.each do |care_age|
  CareAge.find_or_create_by(key: care_age[0]) { |ca| ca.description = care_age[1] }
end
Rails.logger.info 'CREATED CARE AGES'

if ENV['SEED_DATA'].present?
  Rails.logger.info 'SEEDING DATA...'

  users = [
    # Email                Name                    Zip
    ['user_1@example.com', 'Jana', 'Fruehauf',     '21075'],
    ['user_2@example.com', 'Anke', 'Nagel',        '20251'],
    ['user_3@example.com', 'Manuela', 'Rothstein', '20457'],
    ['user_4@example.com', 'Lisa', 'Maurer',       '22117'],
    ['user_5@example.com', 'Angelika', 'Waechter', '22605'],
    ['user_6@example.com', 'Andrea', 'Bergmann',   '20099'],
    ['user_7@example.com', 'Mandy', 'Frankfurter', '21077'],
    ['user_8@example.com', 'Antje', 'Wulf',        '21129']
  ]

  users.each do |user_array|
    user = User.find_or_initialize_by(email: user_array[0])
    next unless user.new_record?

    user.nda = true
    user.language = :de
    user.password = 'change-me-please'
    user.password_confirmation = 'change-me-please'
    user.build_address(first_name: user_array[1], last_name: user_array[2])
    user.address.zip = user_array[3]
    user.confirm
  end

  cases = [
    # Initiated by          Requestor name             Child zip
    ['Renate Henkel',       'Gabriele', 'Schiffmann',  '20097'],
    ['Reinhold Neubert',    'Mareike',  'Schnelle',    '20148'],
    ['Patricia Foadi',      'Roland',   'Heyenga',     '20253'],
    ['Wilhelm Ottenbacher', 'Hanna',    'Beermann',    '20354'],
    ['Gerd Startmann',      'Mareike',  'Ridder',      '20457'],
    ['Nils Streitz',        'Udo',      'Weber',       '20539'],
    ['Wiebke Schwanthaler', 'Lina',     'Wächter',     '21035'],
    ['Franz Xaver Duwe',    'Jule',     'Krull',       '21075'],
    ['Alicia Flohr',        'Uwe',      'Hopp',        '21109']
  ]

  cases.each do |case_array|
    the_case = Case.find_or_initialize_by(initiated_by: :doctor)
    next unless the_case.new_record?

    care_age = CareAge.order('random()').first
    the_case.build_child(care_age: care_age, gender: :male, manifested_on: Time.zone.today, zip: case_array[3],
                         year_of_birth: Time.current.year - rand(18))
    the_case.build_requestor(function: :consultant, email: "user_#{SecureRandom.hex(10)}@example.com", consent: true)
    the_case.requestor.build_address(first_name: case_array[1], last_name: case_array[2], zip: case_array[3])
    the_case.requestor.build_doctor_address(first_name: case_array[1], last_name: case_array[2], zip: case_array[3])
    the_case.build_clinic(zip: case_array[3], name: "Klinik #{SecureRandom.hex(10)}")
    the_case.save!
  end

  Rails.logger.info 'SEEDED DATA...'
end
# Environment variables (ENV['...']) can be set in the file .env file.
