class CreateHelpServiceAssignments < ActiveRecord::Migration[5.2]
  def change
    create_table :help_service_assignments do |t|
      t.references :help_service, foreign_key: true
      t.references :assignee, polymorphic: true, index: true

      t.timestamps
    end
  end
end
