class AddStateToChildren < ActiveRecord::Migration[5.2]
  def change
    add_column :children, :state, :string
  end
end
