class CreateCareAges < ActiveRecord::Migration[5.2]
  def change
    create_table :care_ages do |t|
      t.string :key
      t.string :description

      t.timestamps
    end
  end
end
