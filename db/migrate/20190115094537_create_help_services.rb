class CreateHelpServices < ActiveRecord::Migration[5.2]
  def change
    create_table :help_services do |t|
      t.string :key
      t.string :description

      t.timestamps
    end
  end
end
