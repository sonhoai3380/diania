class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :salutation
      t.string :first_name
      t.string :last_name
      t.string :street
      t.string :zip
      t.string :city
      t.string :state
      t.string :email_business
      t.string :phone_business
      t.string :phone_private
      t.references :addressable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
