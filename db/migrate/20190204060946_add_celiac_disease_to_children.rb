class AddCeliacDiseaseToChildren < ActiveRecord::Migration[5.2]
  def change
    add_column :children, :celiac_disease, :boolean
  end
end
