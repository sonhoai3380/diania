class AddCommutingAllowanceAndPerDiemAllowanceToCases < ActiveRecord::Migration[5.2]
  def change
    add_column :cases, :commuting_allowance, :integer
    add_column :cases, :per_diem_allowance, :integer
  end
end
