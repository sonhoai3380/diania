class ChangeCommutingAllowanceFromIntegerToDecimal < ActiveRecord::Migration[5.2]
  def change
    change_column :cases, :commuting_allowance, :decimal, precision: 10, scale: 2, default: 0.0
  end
end
