class CreateCases < ActiveRecord::Migration[5.2]
  def change
    create_table :cases do |t|
      t.string :aasm_state
      t.date :started_on
      t.boolean :flyer_handed
      t.string :initiated_by
      t.references :clinic
      t.references :user

      t.timestamps
    end
  end
end
