class AddCommentsToCases < ActiveRecord::Migration[5.2]
  def change
    add_column :cases, :comments, :text
  end
end
