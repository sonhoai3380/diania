class AddFinishedOnToCases < ActiveRecord::Migration[5.2]
  def change
    add_column :cases, :finished_on, :date
  end
end
