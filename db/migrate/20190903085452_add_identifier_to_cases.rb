class AddIdentifierToCases < ActiveRecord::Migration[5.2]
  def change
    add_column :cases, :identifier, :string
  end
end
