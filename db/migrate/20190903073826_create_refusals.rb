class CreateRefusals < ActiveRecord::Migration[5.2]
  def change
    create_table :refusals do |t|
      t.references :user
      t.references :case

      t.timestamps
    end
  end
end
