class AddPhoneMobileToAddresses < ActiveRecord::Migration[5.2]
  def change
    add_column :addresses, :phone_mobile, :string
  end
end
