class CreateUserInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :user_infos do |t|
      t.string :occupation
      t.date :birthday
      t.string :reference_to_diabetes
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
