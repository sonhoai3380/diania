class CreateCaseDeployments < ActiveRecord::Migration[5.2]
  def change
    create_table :case_deployments do |t|
      t.integer :distance
      t.integer :duration
      t.datetime :started_on
      t.datetime :finished_on
      t.text :comment
      t.references :case, foreign_key: true

      t.timestamps
    end
  end
end
