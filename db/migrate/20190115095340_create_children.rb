class CreateChildren < ActiveRecord::Migration[5.2]
  def change
    create_table :children do |t|
      t.string :gender
      t.integer :year_of_birth
      t.string :phone
      t.date :manifested_on
      t.string :zip
      t.string :environment
      t.string :therapy
      t.string :nationality
      t.boolean :refugee
      t.json  :diseases
      t.string :operational_env
      t.string :operational_reason
      t.references :case, foreign_key: true
      t.references :care_age, foreign_key: true
      t.string :language

      t.timestamps
    end
  end
end
