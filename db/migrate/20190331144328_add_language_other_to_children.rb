class AddLanguageOtherToChildren < ActiveRecord::Migration[5.2]
  def change
    add_column :children, :language_other, :string
  end
end
