class AddAddressableScopeToAddresses < ActiveRecord::Migration[5.2]
  def change
    add_column :addresses, :addressable_scope, :string, index: true
  end
end
