class CreateJoinTableCareAgesUsers < ActiveRecord::Migration[5.2]
  def change
    create_join_table :care_ages, :users do |t|
      t.index [:user_id, :care_age_id]
    end
  end
end
