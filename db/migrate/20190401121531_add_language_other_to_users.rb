class AddLanguageOtherToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :language_other, :string
  end
end
