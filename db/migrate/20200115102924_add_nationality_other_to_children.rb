class AddNationalityOtherToChildren < ActiveRecord::Migration[5.2]
  def change
    add_column :children, :nationality_other, :string
  end
end
