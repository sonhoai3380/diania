class CreateBankAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :bank_accounts do |t|
      t.string :iban
      t.string :bic
      t.string :bank_name
      t.string :account_holder
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
