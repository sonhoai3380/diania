class CreateRequestors < ActiveRecord::Migration[5.2]
  def change
    create_table :requestors do |t|
      t.string :function
      t.boolean :consent
      t.boolean :confirmed
      t.string :email
      t.string :phone
      t.references :case, foreign_key: true

      t.timestamps
    end
  end
end
